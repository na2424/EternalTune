import React, { Component } from 'react';

class BackTop extends React.PureComponent
{

  constructor()
  {
    super();
    this.start = null;
    this.state = {
      isRotate : "none"
    };

    this.onMouseClick = this.onMouseClick.bind(this);
    this.step = this.step.bind(this);
  }

  step(timestamp)
  {
    if (!this.start)
    {
      this.start = timestamp;
      this.setState({isRotate : "isAnimation"});
    }
    let progress = timestamp - this.start;
    if (progress < 250)
    {
      window.requestAnimationFrame(this.step);
    }
    else
    {
      this.setState({isRotate: "none"});
      this.start = null;
    }
  }

  onMouseClick()
  {
    window.requestAnimationFrame(this.step);
  }

  render()
  {
    return (

        <div className={this.state.isRotate} style={{cursor:'pointer' , zIndex:5}} onClick={this.onMouseClick}>
          <img src="/images/ui/up-arrow.png" />
        </div>

    );
  }
}

export default BackTop;

import React from "react";
import { connect } from "react-redux";
import Store from "../../responsive/store";
import Particles from "react-particles-js";

const VALUE_WHEN_LARGE = 150;
const VALUE_WHEN_SMALL = 42;

const onClick = (e, isRight) => {
  e.preventDefault();
  if (isRight) {
    e.preventDefault();
  }
};

const browserSelector = ({ browser }) => {
  return { browser };
};

@connect(browserSelector)
class ParticleCanvas extends React.PureComponent {
  render() {
    const state = Store.getState();
    let isSmall = state.browser.lessThan.small;
    console.log(isSmall);

    const particles = isSmall ? <SmallParticles /> : <LargeParticles />;

    return (
      <div id="particles" onContextMenu={e => onClick(e, true)}>
        {particles}
      </div>
    );
  }
}

const SmallParticles = () => (
  <React.Fragment>
    <Particles
      params={{
        particles: {
          number: {
            value: VALUE_WHEN_SMALL,
            density: {
              enable: false
            }
          },
          size: {
            value: 3,
            random: true,
            anim: {
              speed: 4,
              size_min: 0.3
            }
          },
          line_linked: {
            enable: false
          },
          move: {
            random: true,
            speed: 1,
            direction: "bottom",
            out_mode: "out"
          }
        },
        interactivity: {
          events: {
            onhover: {
              enable: true,
              mode: "bubble"
            },
            onclick: {
              enable: true,
              mode: "repulse"
            }
          },
          modes: {
            bubble: {
              distance: 250,
              duration: 2,
              size: 0,
              opacity: 0
            },
            repulse: {
              distance: 400,
              duration: 4
            }
          }
        }
      }}
    />
  </React.Fragment>
)

const LargeParticles = () => (
  <Particles
    params={{
      particles: {
        number: {
          value: VALUE_WHEN_LARGE,
          density: {
            enable: false
          }
        },
        size: {
          value: 3,
          random: true,
          anim: {
            speed: 4,
            size_min: 0.3
          }
        },
        line_linked: {
          enable: false
        },
        move: {
          random: true,
          speed: 1,
          direction: "bottom",
          out_mode: "out"
        }
      },
      interactivity: {
        events: {
          onhover: {
            enable: true,
            mode: "bubble"
          },
          onclick: {
            enable: true,
            mode: "repulse"
          }
        },
        modes: {
          bubble: {
            distance: 250,
            duration: 2,
            size: 0,
            opacity: 0
          },
          repulse: {
            distance: 400,
            duration: 4
          }
        }
      }
    }}
  />
)

export default ParticleCanvas;

import React from 'react';
import { Helmet } from 'react-helmet';

const HeadHelmet = React.memo( ({text}) => {
  const title = "Eternal∞Tune(StepMania用DWI配布サイト)" + ((text!="") ? ("/" + text) : "");
  return (
    <Helmet
      title={title}
      meta={[
        { name: 'twitter:card', content: 'summary' },
        { name: 'twitter:title', content: title },
        { name: 'twitter:description', content: 'StepManiaの足譜面(DWI)を公開しています。' },
        { name: 'twitter:image', content: 'http://na24ddr.html.xdomain.jp/images/pic/infinity.png' },
        { name: 'twitter:creator', content: '@NAOKI_StepMania' },
        { property: 'og:title', content: title },
        { property: 'og:type', content: 'website' },
        { property: 'og:url', content: 'http://na24ddr.html.xdomain.jp/' },
        { property: 'og:image', content: 'http://na24ddr.html.xdomain.jp/images/pic/infinity.png' },
        { property: 'og:description', content: 'StepManiaの足譜面(DWI)を公開しています。' },
      ]}
    />
  );
})

export default HeadHelmet;

import React, { Component } from 'react';
import { connect } from "react-redux";
import PropTypes from 'prop-types';

const NavDrawer = ({content}) => (
  <div id="nav-drawer">
    <input id="nav-input" type="checkbox" className="nav-unshown" />
    <label id="nav-open" for="nav-input" ><img src="/images/ui/side_open_button.png" /></label>
    <label className="nav-unshown" id="nav-close" for="nav-input"></label>
    <div id="nav-content">
      {content}
    </div>
  </div>
)

NavDrawer.propTypes = {
  content: PropTypes.node
};

export default NavDrawer;

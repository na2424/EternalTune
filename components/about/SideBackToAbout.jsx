import React, { Component } from "react";
import {
  BrowserRouter as Router,
  Route,
  Link,
  Switch,
  Redirect,
  NavLink
} from 'react-router-dom'

import { connect } from "react-redux";
import Store from '../../responsive/store';

import NavDrawer from '../../components/common/NavDrawer';
import { scrollToTop } from '../../components/common/Helpers';

const browserSelector = ({browser}) =>{
  return {browser}
}

@connect(browserSelector)
class SideBackToAbout extends React.PureComponent
{
  render()
  {
    const state = Store.getState();
    let isLessThanMedium = state.browser.lessThan.large;
    let content = <SideMenu/>;

    let sideDom = isLessThanMedium ?
      <NavDrawer content={content} /> :
      <div id="side">
        <div className="column">
          <SideMenu />
        </div>
      </div>;

    return( sideDom );
  }
}

const SideMenu = () => (
  <React.Fragment>
    <h3>ABOUT MENU</h3>
    <ul id="sidemenu">
        <li onClick={scrollToTop}><NavLink to="/about">{"AboutTopに戻る"}</NavLink></li>
    </ul>
  </React.Fragment>
)

export default SideBackToAbout;



export const packageModel = {
    "name":"package",
    "version": "1.0.0",
    "pack":[
        {
            "sims": 1,
            "link": "https://dl.dropboxusercontent.com/s/8hxhcnz37uw2p4i/Eternal%20Tune%20-Simfile%281%29-.zip?dl=1",
            "date": "2013-12-20",
            "size": "15MB"
        },
        {
            "sims": 2,
            "link": "https://dl.dropboxusercontent.com/s/usqgzbdvoxec5lu/Eternal%20Tune%20-Simfile%282%29-.zip?dl=1",
            "date": "2017-04-20",
            "size": "21MB"
        },
        {
            "sims": 3,
            "link": "https://dl.dropboxusercontent.com/s/afhrqql4wdogurd/Eternal%20Tune%20-Simfile%283%29-.zip?dl=1",
            "date": "2013-12-23",
            "size": "17MB"
        },
        {
            "sims": 4,
            "link": "https://dl.dropboxusercontent.com/s/lbwoh5dn684bv4n/Eternal%20Tune%20-Simfile%284%29-.zip?dl=1",
            "date": "2013-12-22",
            "size": "27MB"
        },
        {
            "sims": 5,
            "link": "https://dl.dropboxusercontent.com/s/91woltpairv4lj1/Eternal%20Tune%20-Simfile%285%29-.zip?dl=1",
            "date": "2013-12-14",
            "size": "27MB"
        },
        {
            "sims": 6,
            "link": "https://dl.dropboxusercontent.com/s/tfzy40u9i2dbocp/Eternal%20Tune%20-Simfile%286%29-.zip?dl=1",
            "date": "2013-12-22",
            "size": "12MB"
        },
        {
            "sims": 7,
            "link": "https://dl.dropboxusercontent.com/s/v8hn90kr0qmxfun/Eternal%20Tune%20-Simfile%287%29-.zip?dl=1",
            "date": "2014-03-21",
            "size": "18MB"
        },
        {
            "sims": 8,
            "link": "https://www.dropbox.com/s/7apfcxqhlszgbvu/Eternal%20Tune%20-Simfile%288%29-.zip?dl=1",
            "date": "2014-08-06",
            "size": "22MB"
        },
        {
            "sims": 9,
            "link": "https://www.dropbox.com/s/0yqee9ubzlv3qnb/Eternal%20Tune%20-Simfile%289%29-.zip?dl=1",
            "date": "2015-01-01",
            "size": "18MB"
        },
        {
            "sims": 10,
            "link": "https://www.dropbox.com/s/iynrfef1vgmmz22/Eternal%20Tune%20-Simfile%2810%29-.zip?dl=1",
            "date": "2015-03-30",
            "size": "23MB"
        },
        {
            "sims": 11,
            "link": "https://www.dropbox.com/s/z9sxu0qmyc8rtf1/Eternal%20Tune%20-Simfile%2811%29-.zip?dl=1",
            "date": "2015-11-13",
            "size": "15MB"
        },
        {
            "sims": 12,
            "link": "https://www.dropbox.com/s/1d1ozkw5cip9dh9/Eternal%20Tune%20-Simfile%2812%29-.zip?dl=1",
            "date": "2016-11-13",
            "size": "20MB"
        },
        {
            "sims": 13,
            "link": "https://www.dropbox.com/s/0f9fwqu0dy7b0ib/Eternal%20Tune%20-Simfile%2813%29-.zip?dl=1",
            "date": "2016-03-30",
            "size": "20MB"
        },
        {
            "sims": 14,
            "link": "#",
            "date": "2017-3-30",
            "size": "20MB"
        }
    ]
};

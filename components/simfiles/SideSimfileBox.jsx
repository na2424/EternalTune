import React, { Component } from 'react';
import {
  BrowserRouter as Router,
  Route,
  Link,
  Switch,
  Redirect,
  NavLink
} from 'react-router-dom'

import { connect } from "react-redux";
import Store from '../../responsive/store';

import FA from 'react-fontawesome';
import NavDrawer from '../../components/common/NavDrawer';

const simfilesNum = 14;

const titleList = [
  'ABC',
  'DEF',
  'GHI',
  'JKL',
  'MNO',
  'PQR',
  'STU',
  'VWXYZ',
  'Others'
]

const titleQuerys = [
  'a-c',
  'd-f',
  'g-i',
  'j-l',
  'm-o',
  'p-r',
  's-u',
  'v-z',
  'others'
]

//-----------------------------------
// サイドメニュー (クリックイベント)
//-----------------------------------
const browserSelector = ({browser}) =>{
  return {browser}
}

@connect(browserSelector)
class SideSimfileBox extends React.PureComponent
{
  render()
  {
    const state = Store.getState();
    let isLessThanMedium = state.browser.lessThan.large;
    let content = <SideMenu/>;

    let sideDom = isLessThanMedium ?
      <NavDrawer content={content} /> :
      <div id="side">
        <div className="column">
          <SideMenu />
        </div>
      </div>;

    return( sideDom );
  }
}

const SideMenu = () => (
  <React.Fragment>
    <h3><FA name="arrow-up"/> Simfiles Menu</h3>
    <ul id="sidemenu">
        <SideList />
    </ul>
  </React.Fragment>
)

class SideList extends React.PureComponent
{
  constructor()
  {
      super();
      this.changeList = this.changeList.bind(this);
      this.state = {
          verType: 'simfiles'
      }
  }

  changeList(event)
  {
      let type = event.target.value;
      this.setState({verType: type});
  }

  render()
  {

    let linkList = [];
    switch(this.state.verType)
    {
      case 'simfiles':
        for (let i = 1; i <= simfilesNum; i++)
        {
          linkList.push(<li data-index={i} className="sims"><NavLink to = {'/simfiles/server?simfiles='+ i } key={i} >{"Simfiles(" + i + ")"}</NavLink></li>);
        }
        break;

      case 'title':
        for (let i = 1; i <= titleList.length; i++)
        {
          linkList.push(<li data-index={i} className="sims"><NavLink to = {'/simfiles/server?title='+ titleQuerys[i-1] } key={i} >{titleList[i-1]}</NavLink></li>);
        }
        break;
    }

    return(
        <div>
          <div className="selectSimfileListRadio" onChange={event => this.changeList(event)}>
            <input type="radio" name="list" id="select1" value="simfiles" checked = { this.state.verType === 'simfiles' } />
            <label htmlFor="select1">Simfiles</label>
            <input type="radio" name="list" id="select2" value="title" checked = { this.state.verType === 'title' } />
            <label htmlFor="select2">Title</label>
          </div>
          {/*<div className="clear"></div>*/}
          <ul>
            {linkList}
          </ul>
        </div>
    );
  }
};

export default SideSimfileBox;

//---------------------------------
// Root.
//---------------------------------
import React, { Component, PureComponent } from 'react';
//import PropTypes from 'prop-types';

import { connect } from "react-redux";
import Store from '../../responsive/store';
import request from 'superagent';
import queryString from 'query-string';
import { ModalContainer, ModalDialog } from 'react-modal-dialog';
import { packageModel } from './PackageModel';
import VideoPlayer from '../../components/simfiles/VideoPlayer';

const JSON_URL = "http://na24ddr.php.xdomain.jp/simfile_json.php";

class View extends Component
{
  // static propTypes = {
  //   isLoading: PropTypes.bool
  // }

  constructor(props)
  {
    super(props);
    this.state = {
      isShowingModal: false,
      isBGLoaded: false
    };

    this.handleClick = this.handleClick.bind(this);
    this.handleClose = this.handleClose.bind(this);
  }

  handleClick() {
    this.setState({isShowingModal: true});
  }

  handleClose() {
    this.setState({isShowingModal: false});
  }

  render() {
    const {
      props: {
        isLoading = false
      }
    } = this;

    let onLoadStart = () =>{
      this.setState({isBGLoaded: false});
    }

    let onLoaded = () =>{
      this.setState({isBGLoaded: true});
    }

    return <div onClick={this.handleClick}>
    {
      this.state.isShowingModal &&
      <ModalContainer onClose={this.handleClose}>
        {
          isLoading ?
          <div>Loading...</div> :
          <ModalDialog onClose={this.handleClose}>
            <SimfilePopupView
              bg_url={this.props.bg_url}
              title={this.props.title}
              download_url={this.props.download_url}
              bg_width={this.props.bg_width}
              bg_height={this.props.bg_height}
              isBGLoaded={this.state.isBGLoaded}
              onLoadStart={onLoadStart}
              onLoaded={onLoaded}
              has_video={this.props.has_video}
              video_url={this.props.video_url}
            />
          </ModalDialog>
        }
      </ModalContainer>
    }
    <img className="simimg" src={"/images/" + this.props.bn_url} width={256} height={80} />
    </div>
  }
}

const SimfilePopupView = (props) =>
{
  let bgElement = "準備中...";

  let bgStyle = {
    width: props.bg_width.toString() +'px',
    height: props.bg_height.toString() +'px',
    objectFit: 'contain'
  };

  if(props.has_video == 1)
  {
    bgElement = <VideoPlayer video_url={props.video_url} />
  }
  else if(props.bg_url !== "" && props.bg_url !== "#")
  {
    bgElement =
      <img
      style={bgStyle}
      src={"/images/" + props.bg_url}
      onLoadStart = {props.onLoadStart}
      onLoad = {props.onLoaded}
      />;
  }

  return(
    <div className="simfilePopupView" >
      <h2>{props.title}</h2>
      <a href={props.download_url}>
        <div className="simfilePopupView__download_btn"><i class="fas fa-download"></i> Download</div>
      </a>
      {
        ((!props.isBGLoaded) && (props.has_video == 0)) &&
        <p>
          <i class="fas fa-spinner fa-spin"></i> Loading...
        </p>
      }
      <p>{bgElement}</p>
    </div>
  );

}

class SimfilesServer extends PureComponent
{
  constructor(props)
  {
    super(props);
    this.state = {
      query : ''
    };
  }

  componentWillMount()
  {
    this.setState({ query: this.props.location.search });
  }

  componentWillReceiveProps(nextProps)
  {
    this.setState({ query: nextProps.location.search });
  }

  render()
  {
    return (
      <MemberBox url={JSON_URL} query={this.state.query}/>
    );
  }
}

class MemberBox extends PureComponent
{

  constructor(props)
  {
    super(props);
    this.req = null;
    this.state = {
      data: []
    };
    this.loadSimfilesFromServer = this.loadSimfilesFromServer.bind(this);
  }

  //MySQLからSimfileJson取得(Ajaxクロスドメイン)
  loadSimfilesFromServer(query)
  {
    if(this.req !== undefined && this.req !== null)
    {
      this.req.abort();
    }

    this.req = request
      .get(this.props.url+query)
      .end((err, res) =>
      {
        this.req = null;
        if (err)
        {
          this.setState({data: []});
          console.error(this.props.url, status, err.toString());
        }
        else
        {
          const json = JSON.parse(res.text);
          if(json.length === 0)
            this.setState({data: []});
          else
            this.setState({data: json});
        }
      });
  }

  componentWillMount()
  {
    this.loadSimfilesFromServer(this.props.query);
  }

  componentWillReceiveProps(nextProps)
  {
    if(nextProps.query === '' ) return;

    this.loadSimfilesFromServer(nextProps.query);
  }

  componentWillUnmount()
  {
    if(this.req !== undefined && this.req !== null)
      this.req.abort();
  }

  render()
  {
    const parsedHash = queryString.parse(this.props.query);
    let type = '';
    let num = 1;

    for(let t in parsedHash)
    {
      type=t.toString();
      num=parsedHash[t];
    }

    const index = num-1;
    const header = (type==='simfiles') ? "Simfiles(" + num + ")" : 'Title('+ num +')';
    const hasPack = index < packageModel.pack.length;

    return (
      <div id="main" className="column">
        <h2>{header}</h2>
        {(type==='simfiles' && hasPack) ? <Package package={packageModel.pack[index]} /> : <br/>}
        <img className="picdet" src="/images/detail.png" />
        <center>
          <MemberList data={this.state.data} />
        </center>
        <div className="clear"></div>
      </div>
    );
  }
}

//--------------------------------------
// Packageダウンロードボタン(ポップアップ)
//--------------------------------------

const Package = React.memo((props) => (
  <div className="package" data-aos="zoom-in">
    <div className="package__tooltip">
      <p>
          {"Simfiles("+ props.package.sims + ") Download"}<br/>
          (Dropbox Shere link)
      </p>
      <a href={props.package.link}>
      <div className="rum">
        <img src="/images/download_button.png" />
        <span>
        <table className="package__tooltipContent">
          <tbody>
          <tr>
            <th>File:</th>
            <td>Eternal Tune -Simfile({props.package.sims})-.zip</td>
          </tr>
          <tr>
            <th>Date:</th>
            <td>{props.package.date}</td>
          </tr>
          <tr>
            <th>Size:</th>
            <td>{props.package.size}</td>
          </tr>
          <tr>
            <th>App:</th>
            <td>StepMania3.9-5.0/OpenITG</td>
          </tr>
          </tbody>
        </table>
        </span>
      </div>
      </a>
    </div>
  </div>
))

//----------------------------------
// メインコンテナ
//----------------------------------

const browserSelector = ({browser}) =>{
  return {browser}
}

@connect(browserSelector)
class MemberList extends PureComponent
{
  render()
  {
    const state = Store.getState();
    let isLessThanMedium = state.browser.lessThan.large;

    let count = 0;
    const memberNodes = this.props.data.map((member) =>
    {
      count++;
      return (
          <Member count={count} title={member.title} artist={member.artist} bpm={member.bpm}
          dl_url={member.dl_url} bn_url={member.bn_url} bg_url={member.bg_url}
          dif_gsp_url={member.dif_gsp_url} dif_bsp_url={member.dif_bsp_url} dif_dsp_url={member.dif_dsp_url} dif_esp_url={member.dif_esp_url} dif_csp_url={member.dif_csp_url}
          dif_gsp={member.dif_gsp} dif_bsp={member.dif_bsp} dif_dsp={member.dif_dsp} dif_esp={member.dif_esp} dif_csp={member.dif_csp}
          has_dp={member.has_dp} dif_bdp_url={member.dif_bdp_url} dif_ddp_url={member.dif_ddp_url} dif_edp_url={member.dif_edp_url} dif_cdp_url={member.dif_cdp_url}
          dif_bdp={member.dif_bdp} dif_ddp={member.dif_ddp} dif_edp={member.dif_edp} dif_cdp={member.dif_cdp}
          time={member.time} size={member.size} update_at={member.update_at} is_small_title={member.is_small_title} is_small_artist={member.is_small_artist}
          bg_width={member.bg_width} bg_height={member.bg_height} isLessThanMedium={isLessThanMedium} has_video={member.has_video} video_url={member.video_url}></Member>
      );
    });
    return(
      <div className="memberList">
        {memberNodes}
      </div>
    );
  }
};

const Member = ( props ) =>
{
  const singleStepChart =
  <ul className="simdif">
      <Difficulty simdif={"simdif__g"} dif_url={props.dif_gsp_url} foot_image={"dif1.png"} dif_name={"GSP"} dif={props.dif_gsp} />
      <Difficulty simdif={"simdif__b"} dif_url={props.dif_bsp_url} foot_image={"dif2.png"} dif_name={"BSP"} dif={props.dif_bsp} />
      <Difficulty simdif={"simdif__d"} dif_url={props.dif_dsp_url} foot_image={"dif3.png"} dif_name={"DSP"} dif={props.dif_dsp} />
      <Difficulty simdif={"simdif__e"} dif_url={props.dif_esp_url} foot_image={"dif4.png"} dif_name={"ESP"} dif={props.dif_esp} />
      <Difficulty simdif={"simdif__c"} dif_url={props.dif_csp_url} foot_image={"dif5.png"} dif_name={"CSP"} dif={props.dif_csp} />
  </ul>;

  const hasDP = (props.has_dp === "1");
  const doubleStepChart = hasDP ?
    <ul className="simdif">
      <Difficulty simdif={"simdif__g"} dif_url={'#'} foot_image={"dif1.png"} dif_name={""} dif={"0"} />
      <Difficulty simdif={"simdif__b"} dif_url={props.dif_bdp_url} foot_image={"dif2.png"} dif_name={"BDP"} dif={props.dif_bdp} />
      <Difficulty simdif={"simdif__d"} dif_url={props.dif_ddp_url} foot_image={"dif3.png"} dif_name={"DDP"} dif={props.dif_ddp} />
      <Difficulty simdif={"simdif__e"} dif_url={props.dif_edp_url} foot_image={"dif4.png"} dif_name={"EDP"} dif={props.dif_edp} />
      <Difficulty simdif={"simdif__c"} dif_url={props.dif_cdp_url} foot_image={"dif5.png"} dif_name={"CDP"} dif={props.dif_cdp} />
    </ul> :
    <React.Fragment></React.Fragment>;

  let aosStyle = "fade-up data-aos-once='true' ";

  if(!props.isLessThanMedium)
  {
    aosStyle = props.count%2 === 1 ? "fade-right" : "fade-left";
  }

  return(
    <div className="simfile" data-aos={aosStyle}>
        <dl className={props.count%2 === 1 ? "simboxright" : "simboxleft2"}>
          <dt>Title</dt>
          <dd className={props.is_small_title==='1' ? "minifont" : "normalfont"}>{props.title}</dd>
          <dt>Artist</dt>
          <dd className={props.is_small_artist==='1' ? "minifont" : "normalfont"}>{props.artist}</dd>
          <dt>BPM</dt>
          <dd>{props.bpm}</dd>
        </dl>
        <div className={props.count%2 === 1 ? "simboxleft" : "simboxright2"}>
          <View title={props.title} download_url={props.dl_url} bn_url={props.bn_url} bg_url={props.bg_url} bg_width={props.bg_width} bg_height={props.bg_height} has_video={props.has_video} video_url={props.video_url}/>
          {singleStepChart}
          {doubleStepChart}
          <dl className="simdet">
            <div>
              <dt>Time</dt>
              <dd>{props.time}</dd>
            </div>
            <div>
              <dt>Size</dt>
              <dd>{props.size}</dd>
            </div>
            <div>
              <dt>Update</dt>
              <dd>{props.update_at}</dd>
            </div>
          </dl>
        </div>
      </div>
  );

};

class Difficulty extends PureComponent
{
  render()
  {
    let tag = (this.props.dif_url==='#') ?
      <li className={this.props.simdif}>
        <img src={'/images/' + this.props.foot_image} align="top" border={0} />{this.props.dif_name + " " + ((this.props.dif !== '0') ? this.props.dif : '-')}
      </li>:
      <a className={this.props.simdif} href={'../images/' + this.props.dif_url} target="_blank" style={{textDecoration: 'none'}}>
        <li className={this.props.simdif}>
          <img src={'/images/' + this.props.foot_image} align="top" border={0} />{this.props.dif_name + " " + ((this.props.dif !== '0') ? this.props.dif : '-')}
        </li>
      </a>;

    return (
      <div className="difficulty-tags">
        {tag}
      </div>
    );
  }
};

export default SimfilesServer;

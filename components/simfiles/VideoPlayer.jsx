import React from 'react';
import {Player,BigPlayButton} from 'video-react';
//import firebase from 'firebase/app';
//import 'firebase/storage';
import 'video-react/dist/video-react.css';

const VideoPlayer = (props) =>
{
  return (
    <Player fluid={false} width={800} height={600} playInline={false}>
      <source src={props.video_url} />
      <BigPlayButton position="center" />
    </Player>
  );
}

// class VideoPlayer extends Component
// {
//   constructor(props)
//   {
//       super(props);

//       this.state = {
//         videoURL: ""
//       };
//   }

//   componentWillMount()
//   {
//     const storage = firebase.storage();
//     const pathReference = storage.ref().child('Video/'+ this.props.video_url);
//     pathReference.getDownloadURL().then((url) => {
//       this.setState({videoURL: url});
//     });
//   }

//   render()
//   {
//     let bgStyle = {
//       width: 800,
//       height: 600,
//       backgroundColor: "#000000",
//       objectFit: 'contain'
//     };
//     // const storage = firebase.storage();
//     // const pathReference = storage.ref().child('Video/'+ this.props.video_url);
//     // pathReference.getDownloadURL().then((url) => {
//     //   this.setState({videoURL: url});
//     // });
//     if(this.state.videoURL==="")
//       return <div style={bgStyle}></div>;

//     return (
//       <Player fluid={false} width={800} height={600} playInline={false}>
//         <source src={this.state.videoURL} />
//         <BigPlayButton position="center" />
//       </Player>
//     );
//   }
// }

export default VideoPlayer;

import React, { Component } from 'react';
import { connect } from "react-redux";
import Store from '../responsive/store';

import Scroll from 'react-scroll';
import HeadHelmet from '../components/common/HeadHelmet';
import FA from 'react-fontawesome';
import AOS from 'aos';
import NavDrawer from '../components/common/NavDrawer';
import request from 'superagent';

const JSON_URL = "/json/links.json";

const linkHash = {
  'stepmania': 'StepMania公式サイト',
  'foreign': '矢印系海外サイト',
  'manual': 'StepMania解説サイト',
  'theme': 'StepManiaテーマ配布サイト',
  'simfile1': 'StepMania関連1',
  'music': '音楽関連',
  'illust': 'イラスト関連',
  'ddredit': 'DDREDIT関連',
  'itg': 'ITG関連',
  'info': 'StepMania情報サイト',
  'simfile2': 'StepMania関連2',
  'updateStop': '更新停止,アンテナサイト',
  'other': 'その他'
};

class Links extends React.PureComponent
{
  constructor(props)
  {
    super(props);
    this.state = {
      data: [],
      isLoaded: false
    };
  }

  //LinksのJson取得
  loadSimfilesFromServer()
  {
    request
      .get(JSON_URL)
      .end((err, res) =>
      {
        if (err)
        {
          this.setState({data: []});
          console.error(this.props.url, status, err.toString());
        }
        else
        {
          const json = JSON.parse(res.text)
          this.setState({data: json});
          this.setState({isLoaded: true})
        }
      });
  }

  componentWillMount()
  {
    if(!this.state.isLoaded)
      this.loadSimfilesFromServer();
  }

  render()
  {
    return(
      <React.Fragment>
        <HeadHelmet text={'Links'} />
        <div id="container-inner" className="columns">
          <SideLinkBox />
          <MainLinkBox data={this.state.data} isLoaded={this.state.isLoaded}/>
        </div>
      </React.Fragment>
    )
  }
}

const Label = (props) =>(
  <h3>
    <div className="scroll-target">{props.text}</div>
  </h3>
)

const LinkCard = (props) => (
  <div class="card-wrap" data-aos="fade-up" data-aos-once="true" data-aos-anchor-placement="top-bottom">
    <a href={props.url} target="_blank">
    <div class="card">
      <img src={props.src} alt={props.title} class="card-img"/>
      <div>
        <h4><a href={props.url} target="_blank">{props.title}</a></h4>
      </div>
    </div>
    </a>
  </div>
)

const Linkbox_ba200 = (props) =>(
  <div className="linkbox-ba200" data-aos="fade-up" data-aos-once="true" data-aos-anchor-placement="top-bottom">
    <dl className="linkbox-ba200">
      <dt><a href={props.url} target="_blank">{props.title}</a></dt>
      <dd className="banner">
      <a href={props.url} target="_blank"><img src={props.src} width="200" height="40" alt={props.title} /></a>
      </dd>
    </dl>
  </div>
)

const LinkBoxNoBanner = (props) => (
  <div className="column" data-aos="fade-up" data-aos-once="true" data-aos-anchor-placement="top-bottom">
    <a className="button is-dark" href={props.url} target="_blank">{props.title}</a>
  </div>
)

const Linkbox_nobanner = (props) =>(
  <div className="linkbox-nobanner" data-aos="fade-up" data-aos-once="true" data-aos-anchor-placement="top-bottom">
    <dl className="linkbox-nobanner">
      <dt><a href={props.url} target="_blank">{props.title}</a></dt>
      <dd>
      <p></p>
      </dd>
    </dl>
  </div>
)

class CategoryLink extends React.PureComponent
{
  render()
  {
    let linkCards = [];
    this.props.links.forEach(link => {
      linkCards.push((link.src !== "") ?
        <LinkCard url={link.url} title={link.title} src={link.src} /> :
        <Linkbox_nobanner url={link.url} title={link.title} />
      );
    });

    return(
      <React.Fragment>
        <Scroll.Element name={this.props.category} className="element">
          <Label text={linkHash[this.props.category]} />
        </Scroll.Element>
        <div className="card-grid">
          {linkCards}
        </div>
        <div className="clear"></div>
      </React.Fragment>
    )
  }
}

class MainLinkBox extends React.PureComponent
{
  constructor(props)
  {
    super(props);
  }

  render()
  {
    const stepmaniaLinks = this.props.data.filter((el)=>{return el.category == "stepmania";});
    const foreignLinks = this.props.data.filter((el)=>{return el.category == "foreign";});
    const manualLinks = this.props.data.filter((el)=>{return el.category == "manual";});
    const themeLinks = this.props.data.filter((el)=>{return el.category == "theme";});
    const simfile1Links = this.props.data.filter((el)=>{return el.category == "simfile1";});
    const musicLinks = this.props.data.filter((el)=>{return el.category == "music";});
    const illustLinks = this.props.data.filter((el)=>{return el.category == "illust";});
    const ddreditLinks = this.props.data.filter((el)=>{return el.category == "ddredit";});
    const itgLinks = this.props.data.filter((el)=>{return el.category == "itg";});
    const infoLinks = this.props.data.filter((el)=>{return el.category == "info";});
    const simfile2Links = this.props.data.filter((el)=>{return el.category == "simfile2";});
    const updateStopLinks = this.props.data.filter((el)=>{return el.category == "updateStop";});
    const otherLinks = this.props.data.filter((el)=>{return el.category == "other";});

    return(
      <div id="main" className="column">
        <h2>Links</h2>
        {
          this.props.isLoaded === false &&
          <p>
            <i class="fas fa-spinner fa-spin"></i> Loading...
          </p>
        }
        { (this.props.isLoaded && this.props.data.length > 0) &&
          <React.Fragment>
            <CategoryLink category={'stepmania'} links={stepmaniaLinks}/>
            <CategoryLink category={'foreign'} links={foreignLinks}/>
            <CategoryLink category={'manual'} links={manualLinks}/>
            <CategoryLink category={'theme'} links={themeLinks}/>
            <CategoryLink category={'simfile1'} links={simfile1Links}/>
            <CategoryLink category={'music'} links={musicLinks}/>
            <CategoryLink category={'illust'} links={illustLinks}/>
            <CategoryLink category={'ddredit'} links={ddreditLinks}/>
            <CategoryLink category={'itg'} links={itgLinks}/>
            <CategoryLink category={'info'} links={infoLinks}/>
            <CategoryLink category={'simfile2'} links={simfile2Links}/>
            <CategoryLink category={'updateStop'} links={updateStopLinks}/>
            <CategoryLink category={'other'} links={otherLinks}/>
          </React.Fragment>
        }

      </div>
    )
  }

}


const browserSelector = ({browser}) =>{
  return {browser}
}

@connect(browserSelector)
class SideLinkBox extends React.PureComponent
{
  render()
  {
    const state = Store.getState();
    let isLessThanMedium = state.browser.lessThan.large;
    let content = <LinkMenu/>;

    let sideDom = isLessThanMedium ?
      <NavDrawer content={content} /> :
      <div id="side">
        <div className="column">
          <LinkMenu />
        </div>
      </div>;

    return( sideDom );
  }
}

const LinkMenu = () => {
  const state = Store.getState();
  let isSmall = state.browser.lessThan.small;
  let headerHeight = isSmall ? -56 : -135;
  return(
    <React.Fragment>
      <h3><FA name="link"/> Links Menu</h3>
      <ul id="sidemenu">
        <LinkLi to={"stepmania"} height={headerHeight} text={linkHash['stepmania']} />
        <LinkLi to={"foreign"} height={headerHeight} text={linkHash['foreign']} />
        <LinkLi to={"manual"} height={headerHeight} text={linkHash['manual']} />
        <LinkLi to={"theme"} height={headerHeight} text={linkHash['theme']} />
        <LinkLi to={"simfile1"} height={headerHeight} text={linkHash['simfile1']} />
        <LinkLi to={"music"} height={headerHeight} text={linkHash['music']} />
        <LinkLi to={"illust"} height={headerHeight} text={linkHash['illust']} />
        <LinkLi to={"ddredit"} height={headerHeight} text={linkHash['ddredit']} />
        <LinkLi to={"itg"} height={headerHeight} text={linkHash['itg']} />
        <LinkLi to={"info"} height={headerHeight} text={linkHash['info']} />
        <LinkLi to={"simfile2"} height={headerHeight} text={linkHash['simfile2']} />
        <LinkLi to={"updateStop"} height={headerHeight} text={linkHash['updateStop']} />
        <LinkLi to={"other"} height={headerHeight} text={linkHash['other']} />
      </ul>
    </React.Fragment>
  )
}

const LinkLi = ({to, height, text}) =>
(
  <Scroll.Link activeClass="active" to={to} spy={false} smooth={true} offset={height} duration={420} >
    <li className="btn_scroll">{text}</li>
  </Scroll.Link>
)

export default Links;

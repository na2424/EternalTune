import React, { Component } from 'react';
import {
  BrowserRouter as Router,
  Route,
  Link,
  Switch,
  Redirect,
  NavLink
} from "react-router-dom";

import Footer from '../components/Footer';

//Menu
import Home from '../components/Home';
import About from '../components/About';
import Simfiles from '../components/Simfiles';
import Others from '../components/Others';
import Links from '../components/Links';

//Other Children
import Creation from '../components/other/Creation'
import DDREdit from '../components/other/DDREdit'
import DDRPad from '../components/other/DDRPad'
import Memo from '../components/other/Memo'

//Memo Children
import Command from '../components/other/memo/Command'
import FileFormats from '../components/other/memo/FileFormats'
import CharactorCode from '../components/other/memo/CharactorCode'
import GroupFolder from '../components/other/memo/GroupFolder'
import Calculator from './other/memo/Calculator'

//enum
import MemoState from '../components/enum/MemoState';
import OtherState from '../components/enum/OtherState';

const Container = () =>
(
  <div id="container">
    <ContainerSwitch />
  </div>
)

const NoMatch = ({ location }) => (
  <div>
    <h3>No match for <code>{location.pathname}</code></h3>
  </div>
)

const ContainerSwitch = () =>
(
  <React.Fragment>
    <Switch>
      <Route exact path="/" component={Home} />
      <Route path="/about" component={About}/>
      <Route path="/simfiles" component={Simfiles}/>
      <Route path="/simfiles/:id" component={Simfiles}/>
      <Route exact path="/others" component={Others}/>
        <Route path="/others/creation" render={props => <Others contentType={OtherState.Creation} {...props} />}/>
        <Route path="/others/ddredit" render={props => <Others contentType={OtherState.DDREdit} {...props} />}/>
        <Route path="/others/ddrpad" render={props => <Others contentType={OtherState.DDRPad} {...props} />}/>
        <Route exact path="/others/memo" component={Memo}/>
          <Route path="/others/memo/command" render={props => <Memo contentType={MemoState.Command} {...props} />}/>
          <Route path="/others/memo/file_formats" render={props => <Memo contentType={MemoState.FileFormats} {...props} />}/>
          <Route path="/others/memo/charactor_code" render={props => <Memo contentType={MemoState.CharactorCode} {...props} />}/>
          <Route path="/others/memo/group_folder" render={props => <Memo contentType={MemoState.GroupFolder} {...props} />}/>
          <Route path="/others/memo/calcurate" render={props => <Memo contentType={MemoState.Calcurate} {...props} />}/>
      <Route path="/links" component={Links}/>
      <Route component={NoMatch}/>
    </Switch>
    <Footer/>
  </React.Fragment>
)

export default Container;

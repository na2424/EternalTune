import * as React from 'react';

const Footer : React.SFC = React.memo(() => {
  return <div id="footer">
    <div id="footer-inner">
      <CopyRight />
    </div>
  </div>;
})

const CopyRight : React.SFC = React.memo(() => (
  <div id="copy_r">
    <img src="/images/copyright.png" />
  </div>
))

export default Footer;

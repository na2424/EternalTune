import React, { Component } from "react";
import { Scrollbars } from 'react-custom-scrollbars';
import { connect } from "react-redux";
import { Helmet } from 'react-helmet';
import { Timeline } from 'react-twitter-widgets';
import ReactRotatingText from 'react-rotating-text';
import Store from '../responsive/store';
import Diary from '../components/Diary';
import NavDrawer from '../components/common/NavDrawer';

import FA from 'react-fontawesome'

//------------------
// Main Component.
//------------------
const MainHomeBox = () => (
  <div id="main" className="column">
      <h2>Index</h2>
      <TopInfo />
      <Diary/>
      <History/>
  </div>
)

const TopInfo = () =>(
  <React.Fragment>
    <p>
      最終更新 2019年02月22日<br/>
      ↓Dance Elenationページ↓<br/>
    </p>
    <a href="https://danceelenation.web.app/"><img src="images/pic/dance_elenation_logo.png" /></a>
    <br/> Dance Elenation (ver1.65)公開中
    <br/> <ReactRotatingText items={['Diaryを更新中', 'DDR EDITに譜面追加', "過去譜面を修正中"]}/>
  </React.Fragment>
)

const History = () =>(
  <React.Fragment>
    <h3>更新履歴</h3>
    <Scrollbars style={{ width: '100%', height: 100 }} id="content-ltn" className="content-sc2">
      <dl className="textbox">
        <dt>2016.6.30</dt>
        <dd>Simfiles(13)公開</dd>
        <dt>2016.3.6</dt>
        <dd>Simfiles(12)公開</dd>
        <dt>2015.12.17</dt>
        <dd>-Phase2-公開</dd>
        <dt>2015.11.13</dt>
        <dd>Simfiles(11)公開</dd>
        <dt>2015.3.31</dt>
        <dd>DWI(10)公開</dd>
        <dt>2015.1.1</dt>
        <dd>DWI(9)公開</dd>
        <dt>2014.11.6</dt>
        <dd>DWI(8)に「Decision full version」追加</dd>
        <dt>2014.8.6</dt>
        <dd>DWI(8)に「Invisible」追加</dd>
        <dt>2014.7.31</dt>
        <dd>DWI(8)に「Dew」追加</dd>
        <dt>2014.6.30</dt>
        <dd>DWI(8)に「Decision」追加</dd>
        <dt>2014.3.22</dt>
        <dd>DWI(7)に「End of the music」追加</dd>
        <dt>2014.3.1</dt>
        <dd>DWI(7)に「JOG HYBRID」,「夢幻華」追加</dd>
        <dt>2014.2.28</dt>
        <dd>DANCE ELENATION ver1.0公開</dd>
        <dt>2013.11.29</dt>
        <dd>DWI(6)公開</dd>
        <dt>2013.9.16</dt>
        <dd>DWI(5)に「YOKOHAMA EVOLVED」追加</dd>
        <dt>2013.8.13</dt>
        <dd>DWI(5)公開</dd>
        <dt>2013.7.18</dt>
        <dd>ProtComporationCSP追加版を公開しました</dd>
        <dt>2013.6.2</dt>
        <dd>DWI(4)公開</dd>
        <dt>2013.5.6</dt>
        <dd>DWI(3)公開</dd>
        <dt>2013.5.5</dt>
        <dd>相互リンク１件追加「<a href="http://whitia1.web.fc2.com/index.html">PARANOiA PRiSM</a>」</dd>
        <dt>2013.4.8</dt>
        <dd>DWI(1)(2)修正版を公開</dd>
        <dt>2013.1.28</dt>
        <dd>DWI(3)１file追加</dd>
        <dt>2013.1.28</dt>
        <dd>DWI製作者に60の質問更新</dd>
        <dt>2013.1.27</dt>
        <dd>DWI(2)公開</dd>
        <dt>2013.1.26</dt>
        <dd>相互リンク１件追加「<a href="http://writtenbypencil.web.fc2.com/index.html">Written</a>」</dd>
        <dt>2013.1.20</dt>
        <dd>ELEBEATver0.9公開</dd>
        <dt>2013.1.18</dt>
        <dd>相互リンク１件追加「<a href="http://7mai.blog71.fc2.com/index.html">VOCALO@DWI</a>」</dd>
        <dt>2013.1.12</dt>
        <dd>DDR EDIT追加、新サイトバナー追加、画像追加　</dd>
        <dt>2012.12.30</dt>
        <dd>相互リンク１件追加「<a href="http://aimfreestyle.blog46.fc2.com/index.html">時と星の軌跡</a>」</dd>
        <dt>2012.12.20</dt>
        <dd>相互リンク１件追加「<a href="http://www.isc.meiji.ac.jp/~yumie/research.html">健康医工学研究室</a>」</dd>
        <dt>2012.12.19</dt>
        <dd>相互リンク１件追加「<a href="http://maramuya.blog87.fc2.com/blog-entry-3.html">まらむやの暇つぶし</a>」</dd>
        <dt>2012.12.14</dt>
        <dd>相互リンク１件追加「<a href="http://com.nicovideo.jp/community/co1067385.html">StepManiaで足譜面</a>」</dd>
        <dt>2012.12.5</dt>
        <dd>相互リンク１件追加「<a href="http://ameblo.jp/elfen-knight/index.html">elfen-knight</a>」</dd>
        <dt>2012.11.20</dt>
        <dd>相互リンク１件追加「<a href="http://wantedoo.iza-yoi.net/index.html">をんてっ堂</a>」</dd>
        <dt>2012.11.18</dt>
        <dd><a href="http://www48.atwiki.jp/elebeat/index.html">ELEBEAT</a>特設サイト追加、リンク３件追加(後で整理)</dd>
        <dt>2012.11.7</dt>
        <dd>相互リンク追加「<a href="http://sms.ohuda.com/about.html">ステマニサーチ</a>」</dd>
        <dt>2012.10.31</dt>
        <dd>相互リンク２件追加「<a href="http://sm.tomyrou.com/index.html">Heimdall Stepmania</a>」「<a href="http://crsosdwi.blog127.fc2.com/index.html">-CR-SOS DWI置き場</a>」</dd>
        <dt>2012.10.31</dt>
        <dd>DWI(1)公開！</dd>
        <dt>2012.10.29</dt>
        <dd>Dolzarkさんが副管理人になりました。</dd>
        <dt>2012.10.20</dt>
        <dd>相互リンク追加「<a href="http://tenku1104.web.fc2.com/index.html">Esmelas Hearts</a>」</dd>
        <dt>2012.10.14</dt>
        <dd>DWI(1)Macbeth先行公開、相互リンク追加「<a href="http://kitunekotei.kitunebi.com/index.html">狐猫亭</a>」</dd>
        <dt>2012.10.7</dt>
        <dd>サイトリニューアル準備中</dd>
      </dl>
    </Scrollbars>
  </React.Fragment>
)

const twitterWindStyle = {
  width:'188px',
  height:'380px',
  border:'solid 1px',
  borderColor:'#E0E0E0'

}

const tweetStyle = {
  fontSize:'12px', // note the capital 'W' here
  textAlign:'right',
  width:'188px'
};

const ddrStyle = {
    textAlign: 'center'
}

const browserSelector = ({browser}) =>{
  return {browser}
}

@connect(browserSelector)
class SideHomeBox extends React.PureComponent
{
  render()
  {
    const state = Store.getState();
    let isLessThanMedium = state.browser.lessThan.large;
    let content = <SideMenu/>;

    let sideDom = isLessThanMedium ?
      <NavDrawer content={content} /> :
      <div id="side">
        <div className="column">
          <SideMenu />
        </div>
      </div>;

    return sideDom;
  }
}

const SideMenu = () => (
  <React.Fragment>
    <Profile />
    <Twitter />
    <DDR />
  </React.Fragment>
)

const Profile = () =>(
  <React.Fragment>
    <h3><FA name="fas fa-address-card"/> Profile</h3>
    <p>
      名前：NAOKI<br/>
      500譜面以上作っています。<br/>
      StepMania 2004-<br/>
      AC DDR 2012-<br/>
      <a href="http://www.flashflashrevolution.com/profile/NAOKI-24/index.html">FFR Account</a><br/>
    </p>
  </React.Fragment>
);

const Twitter = () =>(
  <React.Fragment>
    <h3><FA name="fas fa-infinity"/> Twitter Activities</h3>
    <div className="twitter_timeline">
      <Timeline
        dataSource={{
          sourceType: 'profile',
          screenName: 'NAOKI_StepMania'
        }}
        options={{
          username: 'NAOKI_StepMania',
          height: '420'
        }}
        data-chrome="nofooter transparent"
      />
    </div>
  </React.Fragment>
);

const DDR = () =>(
  <div style={ddrStyle}>
    <a href="https://p.eagate.573.jp/game/ddr/ddra/p/index.html"><img src="/images/banner_ddrgw.jpg" /></a>
  </div>
);

const Home = () =>(
  <React.Fragment>
    <Helmet
      title="Eternal∞Tune(StepMania用DWI配布サイト)"
      meta={[
        { name: 'twitter:card', content: 'summary' },
        { name: 'twitter:title', content: 'Eternal∞Tune(StepMania用DWI配布サイト)' },
        { name: 'twitter:description', content: 'StepManiaの足譜面(DWI)を公開しています。' },
        { name: 'twitter:image', content: '/images/eternal_tune_bn.png' },
        { name: 'twitter:creator', content: '@NAOKI_StepMania' },
        { property: 'og:title', content: 'Eternal∞Tune(StepMania用DWI配布サイト)' },
        { property: 'og:type', content: 'website' },
        { property: 'og:url', content: 'http://na24ddr.html.xdomain.jp/' },
        { property: 'og:image', content: '/images/eternal_tune_bn.png' },
        { property: 'og:description', content: 'StepManiaの足譜面(DWI)を公開しています。' },
      ]}
    />
    <div id="container-inner" className="columns">
      <SideHomeBox />
      <MainHomeBox />
    </div>
  </React.Fragment>
)

export default Home;


{/*
  -----------Memo--------------

  <!--<a href="http://agsystem2006.blog3.fc2.com/">(姉妹サイト)A.G.M -Anti Gravity Maxige-</a><br/>-->

    <strong>(合法ステマニサイト相互リンク)</strong><br/>
    <div>
        <ul id="sidelink">
            <li id="textlink">
                <a href="http://melnoirmeteor.xxxxxxxx.jp/index.html">meteo-r</a><br/>
                <a href="http://kitunekotei.kitunebi.com/index.html">狐猫亭</a><br/>
                <a href="http://tenku1104.web.fc2.com/index.html">Esmelas Hearts</a><br/>
                <a href="http://aimfreestyle.blog46.fc2.com/index.html">時と星の軌跡</a><br/>
                <a href="http://whitia1.web.fc2.com/index.html">PARANOiA PRiSM</a><br/>
            </li>
        </ul>
    </div>

  */}

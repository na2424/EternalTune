import React, { Component } from 'react';

import {
  BrowserRouter as Router,
  Route,
  Link,
  Switch,
  Redirect,
  NavLink
} from 'react-router-dom'

import HeadHelmet from '../components/common/HeadHelmet';
import LazyLoad from 'react-lazyload';

import SimfilesServer from '../components/simfiles/SimfilesServer';
import SideSimfileBox from '../components/simfiles/SideSimfileBox';

const Simfiles = ({ match }) =>(
  <React.Fragment>
    <HeadHelmet text={"Simfiles"} />
    <div id="container-inner" className="columns">
      <SideSimfileBox />
      <Switch>
        <Route exact path={match.url} component={MainBox}/>
        <Route path={`${match.url}/server`} component={SimfilesServer}/>
      </Switch>
    </div>
  </React.Fragment>
)

const MainBox = () =>(
    <div id="main" className="column">
        <h2>Simfiles</h2>
        <p>
            Simfileは3作から公開していきます。<br/>
            <br/>
            こちらのサイトで公開している譜面の配置は交互踏みの足譜面です。<br/>
            左右の足を交互に動かして踏む事を前提条件とした譜面になります。<br/>
            意図的なスライドは除き、交互踏みという公理から出発しています。<br/>
            (キーボードやPSコントローラー、PSP Revolution等の環境でもOKです。)<br/>
            スライドした場合に足の移動量が少なくなり、安定することがあります。<br/>
            <br/>
            <LazyLoad height={200}>
            <img src="/images/simfiles.png" /><br/>
            </LazyLoad>
            {/*
            <br/>
            <img src="/images/pic/motto.png" /><br/>
            <br/>
            */}
        </p>
        <h3>2枚抜きの譜面について</h3>
        <p>
            片足で踏める箇所が2つある前提で作られた譜面がPump It Upや一部のITG譜面に存在します。<br/>
            2枚抜きは踏み方の名前ですが、交互の譜面パターンを拡張するものです。<br/>
            <br/>
            <img src="/images/2mai.png" /><br/>
            <br/>
            左のサイドメニューからダウンロードできるページに入れます。<br/>
            もしダウンロードができないという場合はメールまたはツイッターで報告お願いします。<br/>
            na2424(∞)hotmail.co.jp [(∞)⇒@に変えてください]<br/>
            <br/>
            フォルダバナーはこちらをお使いください。<br/>
            <img src="/images/eternal_tune_bn_sample.png"/><br/>
        </p>
    </div>
)

export default Simfiles;

import React, { Component } from 'react';

import {
  BrowserRouter as Router,
  Route,
  Link,
  Switch,
  Redirect,
  NavLink
} from 'react-router-dom'

import { connect } from "react-redux";
import Store from '../responsive/store';

import Scroll from 'react-scroll';
import HeadHelmet from '../components/common/HeadHelmet';
import StackGrid from "react-stack-grid";

import Q60_2 from '../components/about/q60_2';
import SideBackToAbout from '../components/about/SideBackToAbout';
import ScrollToTopOnMount from '../components/common/ScrollToTopOnMount';
import NavDrawer from '../components/common/NavDrawer';
import FA from 'react-fontawesome';

const About = React.memo(() =>(
  <React.Fragment>
    <HeadHelmet text={'About'} />
    <Switch>
      <Route exact path="/about" component={AboutRoot}/>
      <Route path="/about/q60_2" component={About_Q60_2}/>
    </Switch>
  </React.Fragment>
))

const AboutRoot = () =>(
  <div id="container-inner" className="columns">
    <SideAboutBox />
    <MainAboutBox />
  </div>
)

const About_Q60_2 = () =>(
  <div id="container-inner" className="columns">
    <ScrollToTopOnMount />
    <SideBackToAbout />
    <Q60_2 />
  </div>
)

const Overview = () =>(
  <React.Fragment>
    <h3>{"概要"}</h3>
    <p>{"StepManiaという音楽ゲームで遊べる譜面(Simfile)を公開しています。"}<br/>
    <br />
    {"We are distributing the Simfile that you can play with the music game called StepMania."}<br/>
    <StackGrid
      columnWidth={150}
      monitorImagesLoaded={true}
      appearDelay={100}
    >
    <div>
      <img src={"/images/screen01651.jpg"} width={"160"} height={"120"}/>
    </div>
    <div>
      <img src={"/images/pic3.jpg"} width={"160"} height={"120"}/>
    </div>
    <div>
      <img src={"/images/pic4.png"} width={"160"} height={"120"}/>
    </div>
    <div>
      <img src={"/images/screen01685.jpg"} width={"160"} height={"120"}/>
    </div>
    </StackGrid>
    </p>
  </React.Fragment>
);


const AboutDWI = () => (
  <React.Fragment>
    <h3>{"Simfileについて"}</h3>
    <p>
      公開中のSimfileに含まれる音源（oggファイルなど）<br />
      は<font color="yellow">権利者より使用許諾を受けて</font>、 あるいは<font color="yellow">利用に関するガイドライン等に則って</font>使用しているものです。<br />
      このサイトで扱っているファイルは全て合法なダウンロードになります。<br />
      StepManiaのデフォルト設定であるカーソルキーまたはテンキー、PSコントローラー、PSPR等でのプレイもOKです。<br />
      一部のDP譜面は副管理人のDolzark氏に製作して頂いております。<br />
      <br />
      フォルダバナーはこちらをお使い下さい。<br/>
      <img src={"/images/eternal_tune_bn.png"} style={{width:"100%", maxWidth:"512px"}}/><br/>
      <br/>
      The music file included in the Simfile that is being distributed is one that is licensed by the right owner <br/>
      or is used in accordance with guidelines on use etc.<br/>
      All files handled on this site are legal downloads.<br/>
      You can also play with StepMania's default setting, Cursor Keys, Numeric Keypad, PS Controller, PSPR etc.<br/>
      Some of the DP Step chart is produced by Mr. Dolzark, Deputy Administrator.<br/>
    </p>
  </React.Fragment>
);

const SystemRequirements = () => (
  <React.Fragment>
    <h3>{"推奨閲覧環境"}</h3>
    <p>
      {"●ウェブブラウザ"}<br />
      Microsoft Edge(Spartan)、Google Chrome、 Firefox、Safari、Opera等<br />
      (Internet Explorerでの閲覧は推奨していません。)<br />
      {"●JavaScript"}<br />
      ブラウザのJavaScript設定を有効にしてください。<br />
      {"●モニタ解像度"}<br />
      1024×768px以上<br />
    </p>
  </React.Fragment>
);

const AboutLink = () => (
  <React.Fragment>
    <p>
      {"Eternal∞Tuneはリンクフリーです。"}<br/>
      {"相互リンクをご希望の方はメールまたはTwitterにてよろしくお願い致します。"}<br/>
      {"バナーは３種類あります。好きなものをお使いください。"}<br/>
      <br/>
      {"新バナー(2014)"}<br/>
      <img src="../images/pic/EternalTune_bn_2014.png" width="200" height="40" /><br/>
      <textarea cols="42" rows="2" defaultValue='<a href="http://na24ddr.html.xdomain.jp/"><img src="http://na24ddr.html.xdomain.jp/images/pic/EternalTune_bn_2014.png" alt="Eternal Tune" /></a>' />
      <br/>
      {"旧バナー(2012)"}<br/>
      <img src="../images/pic/EternalTune_bn_a.png" width="200" height="40" />
      <br/>
      <textarea cols="42" rows="2" defaultValue='<a href="http://na24ddr.html.xdomain.jp/"><img src="http://na24ddr.html.xdomain.jp/images/pic/EternalTune_bn_a.png" alt="Eternal Tune" /></a>' />
      <br/>
      {"旧バナー(2008)"}<br/>
      <img src="../images/pic/EternalTune_bn_b.jpg" width="200"  height="40" />
      <br/>
      <textarea cols="42" rows="2" defaultValue='<a href="http://na24ddr.html.xdomain.jp/"><img src="http://na24ddr.html.xdomain.jp/images/pic/EternalTune_bn_b.jpg" alt="Eternal Tune" /></a>' />
    </p>
  </React.Fragment>
);

const Profile_1 = () => (
  <table className="style_a">
    <tbody>
      <tr>
        <th>{"HN"}</th><td>Dolzark</td>
      </tr>
      <tr>
        <th>{"Birth"}</th><td>1987</td>
      </tr>
      <tr>
        <th>{"Hobby"}</th><td>DDR</td>
      </tr>
      <tr>
        <th>{"Favorite artist"}</th><td>TAG</td>
      </tr>
      <tr>
        <th>{"StepMania"}</th><td>いつごろ始めたか忘れたけど少なくとも2008より前</td>
      </tr>
      <tr>
        <th>{"DDR"}</th><td>初プレイは2nd、知人に借りてちょっとだけ<br/>DDRMAXの頃にPS2版を買ってやり込む<br/>SuperNOVA以降アーケードをやり始める<br/>現在は主にDPをプレイ</td>
      </tr>
      <tr>
        <th>{"DWI制作者に60の質問"}</th><td><NavLink to="/about/q60_2">こちら</NavLink></td>
      </tr>
      <tr>
        <th>{"マイリスト"}</th><td><a href="http://www.nicovideo.jp/mylist/25986615">mylist/25986615</a></td>
      </tr>
    </tbody>
  </table>
);

const Profile_2 = () => (
  <table className="style_a">
    <tbody>
      <tr>
        <th>{"HN"}</th><td>NAOKI</td>
      </tr>
      <tr>
        <th>{"Birth"}</th><td>1991</td>
      </tr>
      <tr>
        <th>{"Hobby"}</th><td>StepMania</td>
      </tr>
      <tr>
        <th>{"Favorite artist"}</th><td>前田尚紀</td>
      </tr>
      <tr>
        <th>{"StepMania"}</th><td>2004～</td>
      </tr>
      <tr>
        <th>{"DDR"}</th><td>1998～ DDRはCS1stから始めましたが、X3 VS 2ndMIXからACで踏み始めました。<br/>DDR-CODE(21135850)</td>
      </tr>
      <tr>
        <th>{"twitter"}</th><td>10/1/2012から活動用(?)として始めました。よろしくお願いします。</td>
      </tr>
      <tr>
        <th>{"ITG譜面シリーズ"}</th><td><a href={"https://www.nicovideo.jp/series/81038"}>series/81038</a></td>
      </tr>
    </tbody>
  </table>
)

const MainAboutBox = () =>
(
  <div id="main" className="column">
    <Scroll.Element name="link1" className="element">
      <h2><div className="scroll-point">{"サイトについて"}</div></h2>
    </Scroll.Element>
    <Overview />
    <AboutDWI />
    <SystemRequirements />

    <Scroll.Element name="link2" className="element">
      <h2><div className="scroll-point">{"リンクについて"}</div></h2>
    </Scroll.Element>
    <AboutLink />

    <Scroll.Element name="link3" className="element">
      <h2><div className="scroll-point">{"副管理人プロフィール"}</div></h2>
    </Scroll.Element>
    <Profile_1 />

    <Scroll.Element name="link4" className="element">
      <h2><div className="scroll-point">{"管理人プロフィール"}</div></h2>
    </Scroll.Element>
    <Profile_2 />

  </div>
)

const browserSelector = ({browser}) =>{
  return {browser}
}

@connect(browserSelector)
class SideAboutBox extends React.PureComponent
{
  render()
  {
    const state = Store.getState();
    let isLessThanMedium = state.browser.lessThan.large;
    let content = <SideMenu/>;

    let sideDom = isLessThanMedium ?
      <NavDrawer content={content} /> :
      <div id="side">
        <div className="column">
          <SideMenu />
        </div>
      </div>;

    return( sideDom );
  }
}

const SideMenu = React.memo(() => {
  const state = Store.getState();
  let isSmall = state.browser.lessThan.small;
  let headerHeight = isSmall ? -56 : -135;
  return (
    <React.Fragment>
      <h3><FA name="info-circle"/> About Menu</h3>
      <ul id="sidemenu">
        <SideLi to={"link1"} height={headerHeight} text={"サイトについて"} />
        <SideLi to={"link2"} height={headerHeight} text={"リンクについて"} />
        <SideLi to={"link3"} height={headerHeight} text={"副管理人プロフィール"} />
        <SideLi to={"link4"} height={headerHeight} text={"管理人プロフィール"} />
      </ul>
    </React.Fragment>
  )
})

const SideLi = ({to, height, text}) =>
(
  <Scroll.Link activeClass="active" to={to} spy={true} smooth={true} offset={height} duration={420} >
    <li className="btn_scroll">{text}</li>
  </Scroll.Link>
)

export default About;

import React, { Component } from "react";
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import {
  BrowserRouter as Router,
  Route,
  Link,
  Switch,
  Redirect,
  NavLink
} from 'react-router-dom'

import SideOtherBox from '../components/other/SideOtherBox';
import Creation from '../components/other/Creation';
import DDREdit from '../components/other/DDREdit';
import DDRPad from '../components/other/DDRPad';
import GroupFolder from '../components/other/memo/GroupFolder';
import OtherState from '../components/enum/OtherState';
import HeadHelmet from '../components/common/HeadHelmet';

const Others = ({contentType}) =>{

  let content = <React.Fragment></React.Fragment>;
  switch(contentType)
  {
    case OtherState.Creation:
      content = <Creation/>;
      break;
    case OtherState.DDREdit:
      content = <DDREdit/>;
      break;
    case OtherState.DDRPad:
      content = <DDRPad/>;
      break;
    default:
      content = <MainBox/>;
      break;
  }

  return(
    <div id="container-inner" className="columns">
      <SideOtherBox />
      {content}
    </div>
  )
}

const MainBox = () =>
{
  const imageFolder = '/images/other_title/';
  return(
    <React.Fragment>
      <HeadHelmet text={'Others'} />
      <div id="main" className="column">
        <h2>Other</h2>
        <p>DWI配布以外のページです。</p>
        <ContentOther url={"/others/creation"} img={imageFolder + "works.png"} pic={imageFolder + "works_pic.png"} text={"製作した画像(動画)やゲームを公開していきます。"} />
        <ContentOther url={"/others/ddredit"} img={imageFolder + "ddredit.png"} pic={imageFolder + "ddredit_pic.png"} text={"DDRのEDIT譜面と音源非同梱のSimfileを公開していきます。"} />
        <ContentOther url={"/others/ddrpad"} img={imageFolder + "ddrpad.png"} pic={imageFolder + "ddrpad_pic.png"} text={"StepManiaでﾊﾟｰﾉｩを踏みたい方に。"} />
        <ContentOther url={"/others/memo"} img={imageFolder + "memo.png"} pic={imageFolder + "memo_pic.png"} text={"覚書きメモなど"} />
      </div>
    </React.Fragment>
  );
}

const ContentOther = (props) => (
  <div className="contentother">
    <div><NavLink to ={props.url}><img src={props.img} /><img src={props.pic} /></NavLink></div>
    <div>{props.text}</div>
  </div>
)

ContentOther.propTypes = {
  url: PropTypes.string,
  img: PropTypes.string,
  pic: PropTypes.string
};

export default Others;

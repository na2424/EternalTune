import React, { Component } from 'react';
import InfiniteScroll from 'react-infinite-scroll-component';
import Diary_0 from '../components/diary/Diary_0';
import Diary_1 from '../components/diary/Diary_1';
import Diary_2 from '../components/diary/Diary_2';
import Diary_3 from '../components/diary/Diary_3';
import Diary_4 from '../components/diary/Diary_4';
import Diary_5 from '../components/diary/Diary_5';
import Diary_6 from '../components/diary/Diary_6';
import Diary_7 from '../components/diary/Diary_7';
import Diary_8 from '../components/diary/Diary_8';
import Diary_9 from '../components/diary/Diary_9';
import Diary_10 from '../components/diary/Diary_10';
import Diary_11 from '../components/diary/Diary_11';
import Diary_12 from '../components/diary/Diary_12';
import Diary_13 from '../components/diary/Diary_13';
import Diary_14 from '../components/diary/Diary_14';
import Diary_15 from '../components/diary/Diary_15';
import Diary_16 from '../components/diary/Diary_16';

const style = {
  display: 'flex',
  alignItems: 'center'
};

let index = 0;

const start =[
    <Diary_16 key={index++}/>,
    <Diary_15 key={index++}/>,
    <Diary_14 key={index++}/>
];

const diarys =[
    <Diary_16 key={index++}/>,
    <Diary_15 key={index++}/>,
    <Diary_14 key={index++}/>,
    <Diary_13 key={index++}/>,
    <Diary_12 key={index++}/>,
    <Diary_11 key={index++}/>,
    <Diary_10 key={index++}/>,
    <Diary_9 key={index++}/>,
    <Diary_8 key={index++}/>,
    <Diary_7 key={index++}/>,
    <Diary_6 key={index++}/>,
    <Diary_5 key={index++}/>,
    <Diary_4 key={index++}/>,
    <Diary_3 key={index++}/>,
    <Diary_2 key={index++}/>,
    <Diary_1 key={index++}/>,
    <Diary_0 key={index++}/>
];

const title = '更新状況など';
const WAIT_TIME = 500;

class Diary extends React.Component
{
  constructor () {
    super();
    this.state = {divs: start};
    this.generateDivs = this.generateDivs.bind(this);
    this.refresh = this.refresh.bind(this);
  }

  generateDivs () {
    if(diarys.length + start.length < this.state.divs.length) return;
    let moreDivs = [];
    let count = this.state.divs.length;

    moreDivs.push( diarys[count++] );

    setTimeout(() => {
      this.setState({divs: this.state.divs.concat(moreDivs)});
    }, WAIT_TIME);
  }

  refresh () {
    this.setState({divs: start});
  }

  render () {
    return (
      <div id="content1" className="content-sc">
        <h3>{title}</h3>
        <InfiniteScroll
          pullDownToRefresh
          pullDownToRefreshContent={<h3 style={{textAlign: 'center'}}>&#8595; Pull down to refresh</h3>}
          releaseToRefreshContent={<h3 style={{textAlign: 'center'}}>&#8593; Release to refresh</h3>}
          refreshFunction={this.refresh}
          next={this.generateDivs}
          hasMore={true}
          height={420}
          loader={<p><center><img src="../images/pic/ajax-loader.gif"/></center></p>}>
          {this.state.divs}
        </InfiniteScroll>
      </div>
    );
  }
}




export default Diary;

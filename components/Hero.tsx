import * as React from 'react';

export interface IHeroProps {
    pass: string | null;
}

const headImages = {
    'index': "header_index.png",
    'about': "header_about.png",
    'simfiles': "header_simfiles.png",
    'others': "header_other.png",
    'links': "header_link.png"
}

const Hero : React.SFC<IHeroProps> = (props) => (
    <section className="hero" >
        <div className="hero-body">
            <div className="container">
               <img src={"/images/header/" + headImages[props.pass]} />
            </div>
        </div>
    </section>
);

export default Hero;



import * as React from 'react';

const Diary_3 : React.SFC = () =>(
  <div className="item">
    <h6>3月31日（火）</h6>
    <p>
      先日Simfiles(10)-Phase1-公開しました。<br/>
      <img src="../../images/bn/ReplicantD-action-bn.png" alt="" width="512" height="160" /><br/>
      「Replicant D-action -Overhead T.T Remix-」 7/11/15/18/??(Phase2で公開予定)<br/>
      マラソン譜面。Replicant D-actionの譜面と音に合わせて追加した配置があります。<br/>
      BPMは波形の区間で計測していますが、最後は手動で合わせています。。<br/>
      Phase2でCSP追加予定です。<br/>
      <img src="../../images/bn/Negligence-bn.png" alt="" width="512" height="160" /><br/>
      「Negligence」 3/8/12/14/16<br/>
      局所難な16分と捻り8分がメインの譜面。<br/>
      PRANAと何かをイメージした配置にしています。<br/>
      BPMが速いので見た目より難しいかも。<br/>
      足の移動距離は短めで、体の向きが切り替わる時に注意。<br/>
      <img src="../../images/bn/MayBeStream-bn.png" alt="" width="512" height="160" /><br/>
      「Maybe Stream」 2/5/8/11/13<br/>
      正面交互譜面。<br/>
      次のノートがどの場所にくるか予測した通りになるような誘導配置で、<br/>
      8分と16分5連を軸にしています。<br/>
      2ヶ所32分が入っていますが地団駄配置なので無理なく踏めます。
    </p>
  </div>
)

export default Diary_3;

import * as React from 'react';

const Diary_12 : React.SFC = () =>(
  <div className="item">
    <h6>2017年9月2日 (土) </h6>
    <p>BelfegoreのCSPを変更しました。</p>
    <h6>2017年8月28日 (月) </h6>
    <p>Simfiles(14)にSystem↑OverTensionを追加しました。<br/>
      <a href="https://www.dropbox.com/s/ryn39sg28kebf7u/SystemOverTension.zip?dl=1"><img src ="images/bn/SystemOverTension-bn.png" alt="" width="512" height="160" /></a><br/>
      「System↑OverTension」4/7/11/14/16<br/>
      見た目がトリッキーな譜面です。<br/>
      微ズレ配置、ソフラン、踏み外しのFAなどの視覚効果を入れています。<br/>
      本家と違った感じの譜面を踏みたい方に是非。あと中盤の動画はAE入門して作っています。<br/>
      今日はこの辺で、ではでは!<br/>
    </p>
  </div>
)

export default Diary_12;

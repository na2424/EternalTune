import * as React from 'react';

const Diary_9 : React.SFC = () =>(
    <div className="item">
      <h6>2017年1月24日 (火) </h6>
      <p>
        リンクにOpenITGを追加しました。<br/>
        <img src="/images/pic/openitg-logo.png"/><br/>
      </p>
      <h6>2017年1月15日（日）</h6>
      <p>
        DDRマットでステマニの更新と<br/>
        Life Streamの画像の差し替えを行いました。<br/>
        <img src="/images/bn/Life-Stream-bn.png"/><br/>
        <img src="/images/diary/life-stream-mini-bg.png"/><br/>
        報告だけになってしまいましたが、今日はこの辺で。ではでは。<br/>
      </p>
    </div>
)

export default Diary_9;

import * as React from 'react';
const Diary_4 : React.SFC = () =>(
    <div className="item">
      <h6>10月26日（月）</h6>
      <p>
        最近制作したU.C.S.(User Custom Step)を動画化しました。<br/>
        (PIUの公式サイトで譜面エディタと曲のサンプルファイルが配布されています。)<br/>
        <iframe src="https://player.vimeo.com/video/144218347" width="320px" height="180px" ></iframe><br/> 
         {/* frameBorder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen */}
        <a href="https://vimeo.com/144218347">[PUMP IT UP] Move That Body! (FULL SONG) [UCS S17]</a><br/>
      </p>
    </div>
)

export default Diary_4;

import * as React from 'react';

const Diary_7 : React.SFC = () =>(
    <div className="item">
        <h6>2016年3月6日（日）</h6>
        <p>
          SimFiles(12)公開しました。<br/>
          <img src="../../images/bn/eternally-bn.png" height="160" width="512"/><br/>
          「eternally」 1/7/12/14/16(15?)<br/>
          総合譜面。前半は譜面通り交互に踏むと爆弾に当たらない配置にしています。<br/>
          難易度は15と16の間でしょうか。<br/>
          <br/>
          <img src="../../images/bn/GAIN-bn.png" height="160" width="512"/><br/>
          「GAIN」 1/5/8/11/13<br/>
          BPMが遅めな事もあり、短い16分乱打を入れています。<br/>
          <br/>
          <img src="../../images/bn/maximum_logic-bn.png" height="160" width="512"/><br/>
          「MAXIMUM LOGIC」 3/8/11/14/16<br/>
          変則リズムと捻りが合わさった譜面です。<br/>
          元々17の予定で作っていたのですが補正が掛かり、16になってしまいました。。<br/>
          もっと精進せねば。。<br/>
          <br/>
          今回は捻り配置の譜面が多くなりましたが、動向を見て作っていければと思います。<br/>
          今日はこの辺で、ではでは。<br/>
        </p>
      </div>
)

export default Diary_7;

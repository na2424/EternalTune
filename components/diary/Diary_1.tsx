import * as React from 'react';

const Diary_1 : React.SFC = () =>(
    <div className="item">
        <h6>1月1日（木）</h6>
        <p>
          明けましておめでとうございます。<br/>
          今年も新しく譜面を作っていきたいですね。<br/>
          ということで新規Simfiles(9)を公開しました。<br/>
          <img src="../../images/bn/BROOPER_STREAM-bn.png" alt="" width="512" height="160" /><br/>
          「BROOPER STREAM」 4/7/11/15/17<br/>
          正面交互譜面。中盤に長い乱打があります。<br/>
          体力消費の大きいジャンプを少なくしているのでこのBPM帯での交互が好きな方に是非。<br/>
          <img src="../../images/bn/if-bn.png" alt="" width="512" height="160" /><br/>
          「if」 2/7/12/15<br/>
          ピアノやエフェクト音に合わせた、所謂音合わせ傾向の強い譜面になっています。<br/>
          Sharpnel、Tachyon…などの譜面傾向もありますが…今回はこの難易度にしました。<br/>
          <img src="../../images/bn/glowing_clouds-bn.png" alt="" width="512" height="160" /><br/>
          「glowing cloud」 3/7/12/16<br/>
          ESPは序盤スイッチ、中盤変則リズム、終盤スライドと全体的に８分ジャンプ、低速が絡む総合譜面になっています。<br/>
          BPMが195と速いので見た目以上に体力が必要かもしれません。<br/>
          終盤のスライド誘導のFA3つ1セットになっているパターンですが、FAを無視すると2つ目まで正面交互で踏めます。<br/>
          また、去年収録できなかったカットイン等があったのでこちらで使用しました。<br/>
          <br/>
          今回一部修正した譜面<br/>
          ・Macbeth<br/>
          ・nation of love<br/>
          ・A transitory oath<br/>
          今年もEternal∞Tuneをよろしくお願いします。<br/>
        </p>
    </div>
)

export default Diary_1;

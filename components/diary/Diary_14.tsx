//import React, { Component } from 'react';
import * as React from 'react';

const Diary_14 : React.SFC = () => {
  return (
    <div className="item">
      <h6>2018年7月3日 (火) </h6>
      <p>
        ProtComporationにDP譜面、Worksの画像にGandharvaを追加しました。<br/>
        今年はEDITに譜面を追加したものの内、他パッケージで公開される予定の譜面も幾つか制作しています。<br/>
        昔作った譜面と今作っている譜面の傾向が異なるので、少しずつ置き換えていくつもりです。<br/>
        <br/>
        <h6>2017年11月14日 (火) </h6>
        新しいツールを色々と試していて、いずれもサイトのSimfilesに還元できたらと思っています。<br/>
        あと、某所でも踏んでます。<br/>
        <img src="/images/diary/itg.png"/><br/>
        置いてある譜面の傾向を見たり。forget meの乱打で踏みにくいところがあったので、画像共に修正しております。<br/>
        近況報告でした。ではでは！<br/>
      </p>
  </div>
  );
};


export default Diary_14;

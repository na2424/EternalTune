import * as React from 'react';

const Diary_8 : React.SFC= () =>(
    <div className="item">
      <h6>2017年1月3日（火）</h6>
      <p>
        <a href="/images/diary/2017.png" target="_blank"><img src="/images/diary/2017.png" height="500" width="500"/></a><br/>
        謹賀新年2017年になりました。<br/>
        昨年はDDRAが稼働してからDiaryの更新が止まっていました。久々の更新です。<br/>
        9999埋めの感覚を元にサイトの譜面を少しずつ変更しています。<br/>
        譜面を作り直して大きく変わったのはLifeStreamでしょうか。<br/>
        今作のDDRはEDITができませんが、16分乱打はPIUで解消してます。16分どころか32分も混ぜてくる←<br/>
        以前に公開したSimfiles(13)の紹介記事を書いていなかったので改めて書きます。<br/>
        <img src="/images/bn/arrogance-bn.png" height="160" width="512"/><br/>
        「arrogance」 2/8/10/13/17<br/>
        ITG用譜面。譜面のテストプレイはITGでやっています。<br/>
        中盤のソフランは見た目通りに踏めばタイミングが合うようになっています。<br/>
        (ITGパッケージで流行っているのかそういった傾向が見られます。)<br/>
        先日、難易度のバランスを考慮して24分地団駄の配置から捻りロールに一部譜面を変えました。<br/>
        2枚抜きや32分餡蜜(?)で対応されそうですが。<br/>
        <img src="/images/bn/ELECTRONIC_FUSION-bn.png" height="160" width="512"/><br/>
        「ELECTRONIC FUSION」 2/7/11/13/16<br/>
        所々に入る24分と正面向きの16分が特徴の譜面です。2回目の24分地団駄が少し長め。<br/>
        調整版は公開当初の譜面より少し捻らせています。<br/>
        <img src="/images/bn/turn_of_time-bn.png" height="160" width="512"/><br/>
        「Turn Of The Time」 2/7/10/13/15<br/>
        曲に合わせた変則リズムが終始続きます。こちらはDDR寄りの譜面です。<br/>
        今回は譜面を２つ作ってから譜面を組み合わせる作り方をしてみました。<br/>
        <br/>
        最近のPIUでS21埋めをいくつか。1枚BreakOnじゃないのが混じってるけどｷﾆｼﾅ‐ｲ。<br/>
        <img src="/images/diary/piu.png"/><br/>
        あとはRemix、Short Cutの曲がリザルト撮れてない…<br/>
        昨年から作りかけのものが譜面に限らず色々あるので早く公開できるよう頑張ります。<br/>
        <img src="/images/diary/omikuzi.png"/><br/>
        今年のおみくじは7,8年ぶりくらいに大吉が出ました。<br/>
        今日はこの辺で、ではでは。
      </p>
    </div>
)

export default Diary_8;

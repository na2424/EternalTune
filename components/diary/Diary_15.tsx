//import React, { Component } from 'react';
import * as React from 'react';

const Diary_15 : React.SFC = () => {
  return (
    <div className="item">
      <h6>2019年2月23日 (土) </h6>
      <p>
       「Rozen Trance Stamina」ITGパッケージを公開しました。<br/>
        <a href="https://www.dropbox.com/s/6xlc86iypmbrbog/Rozen%20Trance%20Stamina.zip?dl=1"><img src="/images/bn/rozen_trance_stamina-bn.png"/></a><br/>
        USBメモリのルートから[./In The Groove 2/Songs/]の中に展開したパッケージフォルダごと入れてください。<br/>
        In The Grooveはグループフォルダ分けされたファイルも読み込みます。<br/>
        (v1.3 1譜面追加)<br/>
        (v1.2 1譜面追加、黒薔薇にSHを追加しました。)<br/>
        (v1.1 1譜面追加、全体の音量を調整しました。)<br/>
        <img src="/images/diary/mini_banner/Alice_in_Leliuria-bn.png"/>
        <img src="/images/diary/mini_banner/DeepCrimson-bn.png"/>
        <img src="/images/diary/mini_banner/iroha.png"/>
        <img src="/images/diary/mini_banner/jade-bn.png"/>
        <img src="/images/diary/mini_banner/kurobara-bn.png"/>
        <img src="/images/diary/mini_banner/lapislazuli-bn.png"/>
        <img src="/images/diary/mini_banner/Little_Strawberry-bn.png"/>
        <img src="/images/diary/mini_banner/Mercury_Vapor_Lamp-bn.png"/>
        <img src="/images/diary/mini_banner/Pride-bn.png"/>
        <img src="/images/diary/mini_banner/Rose_Crystal-bn.png"/>
        <img src="/images/diary/mini_banner/Suigintoh-bn.png"/>
        <img src="/images/diary/mini_banner/Welcome_To_Blanche_Neige-bn.png"/><br/>
        パッケージ感を出すためテーマを統一しています。<br/>
        譜面は全て2000Note以上、内8譜面は2500Note以上になっています。<br/>
        足でクリア済みです。<br/>
        <br/>
        2018年に参加した企画について<br/>
        「TechnicalShowcase 3」<br/>
        In The Grooveの中難易度の譜面コンテストです。<br/>
        大会の譜面を書いている人がいたり、レベルが高かったです。<br/>
        自分の譜面の価値観が変わったイベントでもありました。<br/>
        Simfiles(15)に追加する予定の譜面だったのですが、見直すことにしました。<br/>
        <br/>
        「APPENDIX Vol.1 "Far East Stories."」<br/>
        <img src="/images/diary/app_poster.png"/><br/>
        国内合法パッケージです。前述のイベントから戻ったところで、譜面の方向性を変えていくつか収録しています。<br/>
        企画期間は1年くらいあったようですが、最後の1ヵ月で参加して30譜面近く制作、<br/>
        既存の譜面を修正したのは初めてです。全部は見れていません…。<br/>
        曲が良いだけに勿体ないなぁと思い、3譜面同時に修正したり、<br/>
        近場のDDRがモニタ故障中で黒画面の筐体でテストプレイしたりしてました。<br/>
        譜面で手(足)一杯で画像は時間的に作業できませんでした。<br/>
        <br/>
        地元のPIUについて<br/>
        DDRと半々でやっていましたがS23をクリアした後、撤去されてしまいました。<br/>
        <br/>
        フメンタインで参加した譜面について<br/>
        StepMania5の機能を使った譜面があまり出回っていない為、知識共有になればと思い作成しました。<br/>
        DDR,ITG譜面ではありません。StepMania5はPIU寄りです。<br/>
        寧ろPIU譜面はsscで作られてるんじゃないかと。(卵が先か、鶏が先か)<br/>
        使用したギミックは既存にあるものを組み合わせています。新しい事は特に入れていないのでまとも枠でしょう。<br/>
        Attackは判定エリアを動かしてしまうので議論の余地がありますが。<br/>
        配置は休憩を挟んで傾向を変えています。序盤と中盤はテクニカルチャートで終盤はストリーム傾向です。<br/>
        ギミックを含め各分野を2分半以内に詰め込みました。<br/>
        中盤以降は対称や体力配分まで調整したものの、スコア難になると思いつつ今回はこれで提出しました。<br/>
        <br/>
        2017年までは社会的なスキルを重視してきましたが、この年から例年より譜面を作ることが多くなりました。<br/>
        次は2年以内に作る目標の物があるのですが、学習コストが掛かりそうです。<br/>
        譜面は作るためにスコア上げとコミュニティの傾向の確認は続けていきます。<br/>
        今日はここまで。ではでは。<br/>
      </p>
  </div>
  );
};

export default Diary_15;

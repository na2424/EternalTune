import * as React from 'react';

const Diary_11 : React.SFC = () =>(
  <div className="item">
    <h6>2017年8月2日 (水) </h6>
    <p>夢幻華にChallenge譜面を追加しました。</p>
    <h6>2017年7月8日 (日) </h6>
    <p>
      Simfiles(14)から先に1ファイル公開します。<br/>
      <a href="https://www.dropbox.com/s/buycpaka20j09ha/our%20intention.zip?dl=1"><img src ="images/bn/our_intention-bn.png" alt="" width="512" height="160" /></a><br/>
      「our intention」6/8/11/13/17<br/>
      全譜面時間を取って作っています。単発から発狂まで好きなプレイスタイルで是非。<br/>
      また、画像を作る習慣が半年くらい消滅していましたが、時間を決めて少しずつ取り戻してきた感じです。<br/>
      <img src="/images/diary/ddrtan.png"/><br/>
      最近の激鬼9999埋めはこんな感じです。何枚かMFCを逃すリザルトが…。<br/>
      今日はこの辺で、ではでは!<br/>
    </p>
  </div>
)

export default Diary_11;

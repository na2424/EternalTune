import * as React from 'react';

const Diary_2 : React.SFC = () =>(
    <div className="item">
      <h6>3月7日（土）</h6>
      <p>
        今月Simfiles(10)公開予定。<br/>
        2曲各5譜面完成しています。最近の傾向としてジャンプが多くなりすぎないようにしています。<br/>
        話が変わりますがACのReplicantでMAX.が収録されましたね。<br/>
        当時はDDRマットでプレイしていて、足じゃ無理やろーと思っていましたが、<br/>
        解禁されれば青コン狙っていきたいですね。後半の倍速取りで600になるので等速かなぁ。。<br/>
        ではでは。
      </p>
    </div>
)

export default Diary_2;

import * as React from 'react';

const Diary_6 : React.SFC = () =>(
    <div className="item">
      <h6>2016年1月24日（日）</h6>
      <p>
        明けましておめでとうございます。<br/>
        今年から新しい開発環境を揃えたので、色々作って行きたいですね。<br/>
        過去のSimfileの画像と譜面も少しずつ差し替えていきます。<br/>
        Simfiles(12)は来月、もしくは3月初旬頃に公開予定です。(実際に踏んで微修正等々)<br/>
        今年もEternal∞Tuneをよろしくお願いします。では。<br/>
      </p>
    </div>
)

export default Diary_6;

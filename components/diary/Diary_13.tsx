import * as React from 'react';

const Diary_13 : React.SFC  = () =>(
  <div className="item">
    <h6>2017年9月6日 (水)</h6>
    <p>
      公開しているSimfilesのオフセット調整を行いました。<br/>
      調整時の環境を載せておきます。<br/>
      <s>使用バージョンStepMania3.9 Speedオプションx5~x8, 再生速度x0.3~0.5。</s><br/>
      (9/30 再調整しました。StepMania5 SimplyLove C1000 mini60% 再生速度x0.5。)<br/>
      ※2018年以降はArrowVortexを使用したオフセット調整をしています。
    </p>
  </div>
)

export default Diary_13;

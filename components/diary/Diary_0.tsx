import * as React from 'react';

const Diary_0 : React.SFC = () =>(
    <div className="item">
        <h6>2014年11月06日（木）</h6>
        <p>
            DWI(8)にてフル版DECISIONの譜面を追加しました。<br/>
        </p>
        <h6>10月01日（火)</h6>
        <p>
          DanceElenation ver1.39公開しました！
          ELEBEAT用の「月姫」を4Keys(足)譜面と共に同梱しています。<br/>
          各譜面の難易度は1/5/7/12/16となっています。<br/>
          <img src="/images/pic/moon_princess-bn.png" /><br/>
          ver1.39でAutoPlayとハンドクラップのバグを修正しました。<br/>
          そして参加パッケージが先月公開されたのでこちらでも紹介します。<br/>
          <a href="http://d3mix.a-zeroproduce.net/" target="_blank" title="D3NEX MAXIMUM"><img src="/images/diary/d3nex.png" /></a><br/>
          一部の譜面と画像を製作させて頂きました。<br/>
          今日はこの辺で、ではでは。<br/>
          <img src="/images/pic/energizer-jacket.png" /><br/>
        </p>
    </div>
)

export default Diary_0;

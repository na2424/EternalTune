﻿
import React, { Component } from 'react';
import ReactDOM from "react-dom";

import {
  BrowserRouter as Router,
  Route,
  Link,
  Switch,
  Redirect,
  NavLink
} from 'react-router-dom'

import { connect } from "react-redux";
import Store from '../responsive/store';
import FA from 'react-fontawesome';

import injectStyle from '../components/common/InjectStyle';
import { scrollToTop } from '../components/common/Helpers';
import AnimateHeight from 'react-animate-height';

const SIMEFILES_NUM = 14;
const HEIGHT_MOBILE = "56px";
const HEIGHT_PC = "135px";

const othersMap = new Map(
[
  ['Works', '/others/creation'],
  ['DDR EDIT', '/others/ddredit'],
  ['How to play Stepmania with DDRPad', '/others/ddrpad'],
  ['Memo', '/others/memo']
]);

const browserSelector = ({browser}) =>{
  return {browser}
}

@connect(browserSelector)
class HeaderBox extends React.PureComponent
{
  constructor()
  {
    super();
    this.state = { isOpen: false };
    this.onClickToggle = this.onClickToggle.bind(this);
    this.onSelected = this.onSelected.bind(this);
  }

  onClickToggle()
  {
    this.setState({ isOpen: !this.state.isOpen });
  }

  onSelected()
  {
    scrollToTop();
    if(this.state.isOpen)
      this.setState({ isOpen: false });
  }

  render()
  {
    const state = Store.getState();
    let isSmall = state.browser.lessThan.small;
    let isLessThanMedium = state.browser.lessThan.large;
    let simfilesDom = isSmall ? <NonSliderSimfileList onClickButton = {this.onSelected} />: <SliderSimfileList />;
    let otherDom = isSmall ? <NonSliderOtherList onClickButton = {this.onSelected} />: <SliderOtherList />;
    let headerDom = isSmall ? <MobileHeaderDom onSelected={this.onSelected} onClickToggle={this.onClickToggle} /> : <PCHeaderDom isShow={!isLessThanMedium}/>;

    document.body.style.paddingTop = isSmall ? HEIGHT_MOBILE : HEIGHT_PC;

    return (
      <div id="headerArea" className={this.state.isOpen ? "open":"close"}>
        <div id="header">
          <div id="header-inner">
            {headerDom}
          </div>
        </div>
        <nav id="topNav">
          <ul className={isSmall ? "none" : "space"}>
            <li onClick={this.onSelected}><NavLink to="/">Index</NavLink></li>
            <li onClick={this.onSelected}><NavLink to="/about">About</NavLink></li>
            {simfilesDom}
            {otherDom}
            <li onClick={this.onSelected}><NavLink to="/links">Links</NavLink></li>
            <li className="last"><a href={"/0.html"} title="Entrance">Entrance</a></li>
          </ul>
        </nav>
      </div>
    );
  }
}

const MobileHeaderDom = React.memo((props) => (
  <div id="mobile-head">
    <div id="mobile-logo" >
      <NavLink to="/" onClick = {props.onSelected}><img src={"/images/header/et-pro-logo.png"} /></NavLink>
    </div>
    <div id="nav-toggle" onClick={props.onClickToggle}>
      <div>
        <span></span>
        <span></span>
        <span></span>
      </div>
    </div>
  </div>
))

const PCHeaderDom = React.memo((props) => (
  <div id="pc-head">
    <h1>{"* ステマニ(StepMania)のSimfileを公開しています。*"}</h1>
    {props.isShow ? <ArrowIcon/> : <React.Fragment></React.Fragment>}
    <NavLink onClick={scrollToTop} to="/"><img src={"/images/header/eternal_tune_logo.png"} /></NavLink>
  </div>
))

class ArrowIcon extends React.PureComponent
{
  constructor(props)
  {
    super(props);
    const keyframesStyle = `
    @keyframes korokoro {
      0%   { transform: translate(0%, 0%); }
      5%   { transform: rotate(10deg); }
      25%  { transform: rotate(20deg); }
      30%  { transform: rotate(-10deg); }
      35%  { transform: rotate(-15deg); }
      45%  { transform: rotate(10deg); }
      50%  { transform: rotate(15deg); }
      60%  { transform: rotate(-5deg); }
      65%  { transform: rotate(-7deg); }
      75%  { transform: rotate(0deg); }
      100% { transform: rotate(0deg); }
    }`;
    injectStyle(keyframesStyle);

    this.state = {
      container: {
        animation: 'korokoro 2.5s linear infinite'
      }
    }
  }

  render()
  {
    const arrowImageURL = "/images/header/arrow.png";
    return(
      <div id="arrow_cg" style={this.state.container}>
        <img src={arrowImageURL} />
      </div>
    );
  }
}

class SliderSimfileList extends React.PureComponent
{
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false
    };
    this.onMouseEnter = this.onMouseEnter.bind(this);
    this.onMouseLeave = this.onMouseLeave.bind(this);
  }

  onMouseEnter()
  {
    this.setState({
      isOpen:true
    });
  }

  onMouseLeave()
  {
    this.setState({
      isOpen:false
    });
  }

  render()
  {
    return(
      <li ref="sims" onMouseEnter={this.onMouseEnter} onMouseLeave={this.onMouseLeave}>
        <NavLink to="/simfiles">Simfiles <FA name="chevron-down" fa-rotate-180/></NavLink>
        <NavSimfilesList simfilesNum={SIMEFILES_NUM} isOpen={this.state.isOpen}/>
      </li>
    );
  }
}

const NonSliderSimfileList = (props) => (
  <li onClick={props.onClickButton}><NavLink to="/simfiles">Simfiles</NavLink></li>
)

class SliderOtherList extends React.PureComponent
{
  constructor(props)
  {
    super(props);
    this.state = {
      isOpen: false
    };
    this.onMouseEnter = this.onMouseEnter.bind(this);
    this.onMouseLeave = this.onMouseLeave.bind(this);
  }

  onMouseEnter()
  {
    this.setState({
      isOpen:true
    });
  }

  onMouseLeave()
  {
    this.setState({
      isOpen:false
    });
    //const elem = ReactDOM.findDOMNode(this.refs.others).querySelector("ul");
    // $(elem).find("ul").stop().slideUp("fast");
  }

  render()
  {
    return(
      <li ref="others" onMouseEnter={this.onMouseEnter} onMouseLeave={this.onMouseLeave}>
      <NavLink to="/others">Others <FA name="chevron-down" fa-rotate-180/></NavLink>
        <NavOtherList isOpen={this.state.isOpen}/>
      </li>
    );
  }
}

const NonSliderOtherList = (props) => (
  <li onClick={props.onClickButton}><NavLink to="/others">Others</NavLink></li>
)

const NavSimfilesList = ({simfilesNum, isOpen}) =>
{
  let list = [];
  for (let i = 1; i <= simfilesNum; i++)
  {
      list.push(<li key={i} className= {(i === simfilesNum) ? "last" : "sim"}><NavLink to ={'/simfiles/server?simfiles='+i} title={"Simfiles(" + i + ")"}>{"Simfiles(" + i + ")"}</NavLink></li>);
  }
  const height = 31 * simfilesNum;

  return(
    <div>
      <ul id="simfilesNum" className='category'>
        <AnimateHeight
            duration={ 360 }
            height={ isOpen ? height : 0 }
        >
        {list}
        </AnimateHeight>
      </ul>
    </div>
  );
}

const NavOtherList = ({isOpen}) =>
{
  let list = [];
  const lastIndex = othersMap.size - 1;
  let keys = othersMap.keys();
  let index = 0;
  othersMap.forEach((value, key, map) => {
    list.push(<li key={key} className={(index===lastIndex) ? "last": "otherlist"}><NavLink to = {value} >{key}</NavLink></li>);
    index++;
  });

  const height = 166;

  return (
    <ul id="others" className="category">
      <AnimateHeight
            duration={ 360 }
            height={ isOpen ? height : 0 }
      >
      {list}
      </AnimateHeight>
    </ul>
  );
}

export default HeaderBox;

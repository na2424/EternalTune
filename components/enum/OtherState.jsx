
const OtherState = {
  None: Symbol(),
  Creation: Symbol(),
  DDREdit: Symbol(),
  DDRPad: Symbol(),
  Memo: Symbol()
};

export default OtherState;


const MemoState = {
  None: Symbol(),
  Command: Symbol(),
  FileFormats: Symbol(),
  CharactorCode: Symbol(),
  GroupFolder: Symbol(),
  Calcurate: Symbol()
};

export default MemoState;

import React from "react";
import HeadHelmet from "../../components/common/HeadHelmet";
import FA from "react-fontawesome";
import { connect } from "react-redux";
import request from "superagent";
import Store from "../../responsive/store";
import ReactTable from "react-table-v6";
import "react-table-v6/react-table.css";

const JSON_URL = "/json/ddredit.json";

const columns = [
  {
    Header: "Songs",
    accessor: "title", // String-based value accessors!
    maxWidth: 231
  },
  {
    Header: "譜面画像",
    accessor: "imageUrl", // String-based value accessors!
    Cell: props => (
      <a href={"/images/ddredit/" + props.value} target="_blank">
        <FA name="image" />
      </a>
    ),
    width: 70
  },
  {
    Header: "smfile",
    accessor: "zipUrl",
    Cell: props => (
      <a href={props.value !== "#" ? "/download/ddredit/" + props.value : "#"}>
        {props.value !== "#" ? <FA name="file" /> : <FA name="lock" />}
      </a>
    ), // Custom cell components!
    width: 70
  },
  {
    Header: "難易度",
    accessor: "diff", // Custom value accessors!
    sortMethod: (a, b) => {
      let aValue = 0.0;
      let bValue = 0.0;

      let aNum = a.replace(/[^0-9]/g, "");
      let bNum = b.replace(/[^0-9]/g, "");

      if (a.indexOf("+") !== -1) aValue += 0.1;
      if (a.indexOf("-") !== -1) aValue -= 0.1;

      if (b.indexOf("+") !== -1) bValue += 0.1;
      if (b.indexOf("-") !== -1) bValue -= 0.1;

      aValue += Number(aNum);
      bValue += Number(bNum);

      return aValue < bValue ? 1 : -1;
    },
    maxWidth: 70
  },
  {
    Header: "出典",
    accessor: "from",
    maxWidth: 100
  },
  {
    Header: "update",
    accessor: "update",
    maxWidth: 100
  }
];

const browserSelector = ({ browser }) => {
  return { browser };
};

@connect(browserSelector)
class DDREdit extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      isLoaded: false
    };
  }

  //DDREDITのJson取得
  loadSimfilesFromServer() {
    request.get(JSON_URL).end((err, res) => {
      if (err) {
        this.setState({ data: [] });
        console.error(this.props.url, status, err.toString());
      } else {
        const json = JSON.parse(res.text);
        this.setState({ data: json });
        this.setState({ isLoaded: true });
      }
    });
  }

  componentWillMount() {
    this.loadSimfilesFromServer();
  }

  render() {
    const state = Store.getState();
    let isSmall = state.browser.lessThan.medium;
    let table = isSmall ? (
      <EditMobileTable data={this.state.data} />
    ) : (
      <EditTable data={this.state.data} />
    );
    //let table = <EditTable />;
    return (
      <React.Fragment>
        <HeadHelmet text="Others/DDR EDIT" />
        <div id="main" className="column">
          <h2>DDR EDIT</h2>
          <div>
            <p>{"EDIT譜面とPackage参加譜面等を載せていきます。"}</p>
            {table}
          </div>
        </div>
      </React.Fragment>
    );
  }
}

const EditTable = props => {
  return (
    <ReactTable
      data={props.data}
      columns={columns}
      defaultPageSize={20}
      className="-striped -highlight"
      footerStyle={""}
    />
  );
};

const EditMobileTable = props => {
  let edits = props.data.map(i => (
    <tr>
      <td>{i.title}</td>
      <td>
        <a href={"/images/ddredit/" + i.imageUrl} target="_blank">
          <FA name="image" />
        </a>
      </td>
      <td>{i.diff}</td>
      <td>{i.update}</td>
    </tr>
  ));

  return (
    <React.Fragment>
      <table className="table is-black" >
        <tbody className="is">
          <tr>
            <th>{"Songs"}</th>
            <th>{"譜面画像"}</th>
            <th>{"難易度"}</th>
            <th>{"update"}</th>
          </tr>
          {edits}
        </tbody>
      </table>
    </React.Fragment>
  );
};

export default DDREdit;

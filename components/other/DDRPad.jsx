import React from 'react';
import HeadHelmet from '../../components/common/HeadHelmet'
import ScrollToTopOnMount from '../common/ScrollToTopOnMount';

const DDRPad = () =>(
  <React.Fragment>
    <HeadHelmet text="Others/DDR Pad" />
    <div id="main" className="column">
      <h2>DDRマットでステマニ</h2>

        <p>
          StepManiaのDDRマット使用方法を解説しているサイトが少なかったので<br/>
          このコーナーを作りました。<br/>
          <br/>
          ---------------必要なグッズ---------------<br/>
          ・ダンスダンスレボリューション専用コントローラー<br/>
          ・ゲームパッドコンバータ<br/>
          ------------------------------------------<br/>
          これらのものが無いとできないので注意...!<br/>
        </p>
        <h3>①確認</h3>
        <p>
          ダンスダンスレボリューション専用コントローラー(DDRマット)<br/>
          <img src="/images/ddrpad/ddrmat1.png" /><br/>
          <br/>
          ゲームパッドコンバータ<br/>
          <img src="/images/ddrpad/gp.png" /><br/>
          (今回使用するのはｴﾚｺﾑ社の「ゲームパッドコンバータ JC-PS101Uシリーズ」です。色違いもあります)<br/>
          <br/>
          他のコントローラーアダプターを買う場合、<br/>
          気を付けなければならない事があります。<br/>
          コントローラーアダプターはたくさんの種類があるのですが、<br/>
          DDRマットに対応しないものもあるのです。<br/>
          どういう事かと言うと、<br/>
          DDRマットの方向キーが軸入力になってしまい、←→、↓↑の同時押しが反応しません。<br/>
          このようなタイプのゲームパッドもあるので注意が必要です。<br/>
          <br/>
          今回使用するJC-PS101UシリーズはDDRマットへの対応を考慮してあるため、<br/>
          方向キーの軸入力がボタン入力になるダンスマットモードに変えることができます。<br/>
          こちらではこのコントローラーアダプターでの方法を書いていきます。<br/>
          <br/>
        </p>
        <h3>②接続＆設定</h3>
        <p>
          コントローラーアダプターを買った時に入っているCDをインストールして、
          <br/>
          コントローラーアダプターをPCとDDRマットに繋いだら<br/>
          接続はこれでOKです。<br/>
          <br/>
          あとは設定を変えます。<br/>
          [スタート]から[コントロールパネル]を開いて<br/>
          [デバイスとプリンターの表示]をクリック。<br/>
          <img src="/images/ddrpad/joypad01.png" /><br/>
          <br/>
          次に↓のアイコンを右クリックして「ゲーム コントローラーの設定」を選びます。<br/>
          <img src="/images/ddrpad/joypad02.png" /><br/>
          <br/>
        </p>
        <div className="box">
            <h4>CPLで早く開く方法</h4>
            <p>
              Windows + R キーを押して「ファイル名を指定して実行」を開きます。<br/>
              「joy.cpl」と入力してEnterを押します。<br/>
              <img src="/images/ddrpad/joy.png" /><br/>
            </p>
        </div>
        <p>
          するとこの様な画面が出るので<br/>
          <img src="/images/ddrpad/ddrpad6.png" /><br/>
          「ELECOM JC-PS101U series」<br/>
          を選択してプロパディを開きます。<br/>
          <br/>
          プロパディの画面はこんな感じです。<br/>
          <img src="/images/ddrpad/prop.PNG" /><br/>
          <br/>
          アナログモード時は方向キーを押すと「Hat Switch」が反応し、<br/>
          デジタルモード時は方向キーを押すと「Axis Test」が反応します。<br/>
          <br/>
          アナログモードになっている人はデジタルモードにします。<br/>
          アナログモード⇔デジタルモードの切り替えはPSコントローラーに差し替えて、<br/>
          STARTとSELECTの間にある「ANALOG」で出来ます。<br/>
          <img src="/images/ddrpad/modo2.png" />
          <br/>
          デジタルモードにしたら<br/>
          ここでコマンド入力です。<br/>
          START+SELECTを押したまま↑キーを押すと「ダンスマットモード」に変わります。<br/>
          この操作はPSコントローラーあるいはDDRPadのどちらでも構いません。<br/>
          <br/>
          ちなみに<br/>
          START+SELECTを押したまま↓キーを押すと「デジタルモード」に戻るようです。<br/>
          よくわかりませんが発見しましたｗ<br/>
          <br/>
          ダンスマットモードにすると、<br/>
          方向キーを押すと軸入力ではなく、ボタン入力として扱われるようになります。<br/>
          （←がボタン１、↓がボタン２、↑がボタン３、→がボタン４）<br/>
          <br/>
          OKを押してこれで設定は終わりです。<br/>
          <br/>
        </p>
        <h3>③StepManiaのキーコンフィグ</h3>
        <p>
          StepManiaを起動して<br/>
          オプションの「Config Key/Joy Mappings」を開きます。<br/>
          <br/>
          ↓の赤い四角の中のように設定してEscキーを押します。<br/>
          <img src="/images/ddrpad/config_key.png" />
          <br/>
          これでStepManiaでもDDRマットを使えます！<br/>
          あと、経験的ですがStartにあたるキーをSTARTボタンに、<br/>
          BackにあたるキーをSELECTボタンにすると良いでしょう。<br/>
          <br/>
          それでは解説はこの辺で。<br/>
          お疲れ様でした(´∀`*)<br/>
          <br/>
          <br/>
          解説：NAOKI
        </p>
    </div>
  </React.Fragment>
)

export default DDRPad;

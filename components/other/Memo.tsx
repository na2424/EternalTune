import * as React from 'react';
import HeadHelmet from '../../components/common/HeadHelmet'
import SideMemoBox from './memo/SideMemoBox';
import Command from './memo/Command';
import FileFormats from './memo/FileFormats';
import CharactorCode from './memo/CharactorCode';
import GroupFolder from './memo/GroupFolder';
import Calculator from './memo/Calculator';
import MemoState from '../enum/MemoState';
import { RefactorActionInfo } from 'typescript';

interface MemoProps
{
  contentType : symbol
}

const Memo : React.SFC<MemoProps> = React.memo((props : MemoProps) =>{

  let content = <React.Fragment></React.Fragment>;
  switch(props.contentType)
  {
    case MemoState.Command:
      content = <Command/>;
      break;
    case MemoState.FileFormats:
      content = <FileFormats/>;
      break;
    case MemoState.CharactorCode:
      content = <CharactorCode/>;
      break;
    case MemoState.GroupFolder:
      content = <GroupFolder/>;
      break;
    case MemoState.Calcurate:
      content = <Calculator/>;
      break;
    default:
      content = <MainBox/>;
      break;
  }

  return(
    <div id="container-inner" className="columns">
      <SideMemoBox />
      {content}
    </div>
  )
})

const MainBox : React.SFC = () => (
  <React.Fragment>
    <HeadHelmet text={"Others/Memo"} />
    <div id="main" className="column">
      <h2>Memo</h2>
      <p>
        覚え書きメモなど<br/>
      </p>
    </div>
  </React.Fragment>
)

export default Memo;

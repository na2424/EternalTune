import React from "react";
import {
  BrowserRouter as Router,
  Route,
  Link,
  Switch,
  Redirect,
  NavLink
} from 'react-router-dom'

import { connect } from "react-redux";
import Store from '../../responsive/store';
import FA from 'react-fontawesome';
import NavDrawer from '../common/NavDrawer';
import { scrollToTop } from '../common/Helpers';

const SideOtherBox = React.memo(() =>(
  <SideOtherMenu/>
))

const browserSelector = ({browser}) =>{
  return {browser}
}

@connect(browserSelector)
class SideOtherMenu extends React.PureComponent
{
  render()
  {
    const state = Store.getState();
    let isLessThanMedium = state.browser.lessThan.large;
    let content = <SideMenu/>;

    let sideDom = isLessThanMedium ?
      <NavDrawer content={content} /> :
      <div id="side">
        <div className="column">
          <SideMenu />
        </div>
      </div>;

    return( sideDom );
  }
}

const SideMenu = () => (
  <React.Fragment>
    <h3><FA name="list-alt"/> Others Menu</h3>
    <ul id="sidemenu">
        <li onClick={scrollToTop}><NavLink to="/others/creation">{"Works"}</NavLink></li>
        <li onClick={scrollToTop}><NavLink to="/others/ddredit">{"DDR EDIT"}</NavLink></li>
        <li onClick={scrollToTop}><NavLink to="/others/ddrpad">{"DDR Padでステマニ"}</NavLink></li>
        <li onClick={scrollToTop}><NavLink to="/others/memo">{"Memo"}</NavLink></li>
    </ul>
  </React.Fragment>
)

export default SideOtherBox;

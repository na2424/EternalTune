import React, { Component } from 'react';
import HeadHelmet from '../common/HeadHelmet';
import Lightbox from 'react-image-lightbox';
import StackGrid from "react-stack-grid";
import 'react-image-lightbox/style.css';

const cgImages = [
  // {
  //   title : 'Almagest / Galdeira',
  //   icon_image : "/images/cg/al.png",
  //   image : '/images/cg/Almagest-bg.jpg'
  // },
  // {
  //   title : 'Votum stellarum / iconoclasm',
  //   icon_image : "/images/cg/vo.png",
  //   image : '/images/cg/votum_stellarum2.png'
  // },
  // {
  //   title : "homéomorphisme",
  //   icon_image : "/images/cg/ho.jpg",
  //   image : '/images/cg/homeomorphic.png'
  // },
  // {
  //   title : "JUMP(Radio Edit) / BUS STOP   for Package [StepMania - Dance 2012 Mix -]",
  //   icon_image : "/images/cg/ju.png",
  //   image : '/images/cg/jump_bg.png'
  // },
  // {
  //   title : "Sakura Reflection / Ryu☆　[sm13432916]",
  //   icon_image : "/images/cg/sa.png",
  //   image : '/images/cg/sakura3.png'
  // },
  // {
  //   title : "Dazzling Destiny -advanced soul- / OSTER prpject [sm16338218]",
  //   icon_image : "/images/cg/DazzlingDestiny2-bg.jpg",
  //   image : '/images/cg/DazzlingDestiny2-bg.png'
  // },
  // {
  //   title : "deep forest (Mandelbox)",
  //   icon_image : "/images/df.jpg",
  //   image : '/images/cg/deep_forest.jpg'
  // },
  // {
  //   title : "Cold Breath / SHIKI    for Package [D3NEX 2013 for StepMania 5.0]",
  //   icon_image : "/images/cg/c.jpg",
  //   image : '/images/cg/cold_breath-bg.png'
  // },
  // {
  //   title : "If You Were Here (Quiqman Mix) / Jennifer   for Package [StepMania - Dance 2012 Mix -]",
  //   icon_image : "/images/cg/if.png",
  //   image : '/images/cg/bg.png'
  // },
  // {
  //   title : "Lightball-underground-",
  //   icon_image : "/images/cg/lightball-icon.png",
  //   image : 'http://www.geocities.jp/na24ddr/Picture/lightball_undergroundpng.png'
  // },
  // {
  //   title : "Chaos-une promenade mathématique-",
  //   icon_image : "/images/cg/chaos.jpg",
  //   image : 'http://www.geocities.jp/na24ddr/otherc.png'
  // },
  // {
  //   title : "MEPHISTO / LeaF",
  //   icon_image : "/images/cg/MEPHISTO-ja.jpg",
  //   image : '/images/cg/mephisto-bgbn.png'
  // },
  // {
  //   title : "Sakura Luminance (kors k mix) / Ryu☆ remix by kors k [sm20704527]",
  //   icon_image : "/images/cg/Sakura_l.png",
  //   image : '/images/cg/sakuraluminance.jpg'
  // },
  // {
  //   title : "Ignition∞Break / NAOKI",
  //   icon_image : "/images/cg/ib-icon.png",
  //   image : '/images/cg/ib.png'
  // },
  // {
  //   title : "Signal Prossesing of LOVE =inst ver= / SEKNO for Package [D3NEX 2013 for StepMania 5.0]",
  //   icon_image : "/images/cg/signal_prossesing_of_love-jacket.jpg",
  //   image : '/images/cg/signal_prossesing_of_love-bg.png'
  // },
  // {
  //   title : "logoicon",
  //   icon_image : "/images/cg/chintomo_b.png",
  //   image : '/images/cg/chintomo3.png'
  // },
  // {
  //   title : "Energizer for package [D3NEX MAXIMUM]",
  //   icon_image : "/images/cg/en.png",
  //   image : '/images/cg/energizer.png'
  // },
  // {
  //   title : "月姫 / Azulia Xectyer",
  //   icon_image : "/images/cg/mp-icon.png",
  //   image : '/images/cg/moon_princess.png'
  // },
  // {
  //   title : "Mercury Silver Lamp",
  //   icon_image : "/images/cg/mercury_lamp-icon.png",
  //   image : '/images/cg/mercury_lamp-bg.png'
  // },
  // {
  //   title : "Inception (マットペイント練習用)",
  //   icon_image : "/images/cg/inception-icon.png",
  //   image : 'http://www.geocities.jp/na24ddr/Picture/inception-matpaint.png'
  // },
  // {
  //   title : "Graphs",
  //   icon_image : "/images/cg/g.png",
  //   image : 'http://www.geocities.jp/na24ddr/Picture/Graphs.png'
  // },
  {
    title : "jacket icon",
    icon_image : "/images/cg/tomoyuki.png",
    image : 'https://firebasestorage.googleapis.com/v0/b/eternal-tune.appspot.com/o/images%2Fcg%2FCYYeMAQVAAANDdC.png?alt=media&token=b716dd17-8de0-4208-a317-9b67f54c7c2d'
  },
  {
    title : "DEBRIS",
    icon_image : "/images/cg/debris.png",
    image : 'https://firebasestorage.googleapis.com/v0/b/eternal-tune.appspot.com/o/images%2Fcg%2FDEBRIS-bg.png?alt=media&token=38907ee4-4f6e-475a-b4b5-d3453052b00b'
  },
  {
    title : "eternally",
    icon_image : "/images/cg/eternally.png",
    image : 'https://firebasestorage.googleapis.com/v0/b/eternal-tune.appspot.com/o/images%2Fcg%2Feternally-bg.png?alt=media&token=31ae7415-871e-4642-9cd9-9ee918c22cce'
  },
  {
    title : "ShaderLabでMandelBox (Unity2017)",
    icon_image : "/images/cg/sm.png",
    image : 'https://firebasestorage.googleapis.com/v0/b/eternal-tune.appspot.com/o/images%2Fcg%2Fshaderlab_de_mandelbox.png?alt=media&token=13a1bb06-5f97-4fa7-afbd-4edced9fe166'
  },
  {
    title : "Gandharva",
    icon_image : "/images/cg/gandharva_b.png",
    image : 'https://firebasestorage.googleapis.com/v0/b/eternal-tune.appspot.com/o/images%2Fcg%2Fgandharva_a.png?alt=media&token=afd95260-c12f-44d2-a660-b3f5c4d27897'
  },
  {
    title : "Our Intention",
    icon_image : "/images/cg/oi.png",
    image : 'https://firebasestorage.googleapis.com/v0/b/eternal-tune.appspot.com/o/images%2Fcg%2Four_intention-bg.png?alt=media&token=52934b89-eb74-4404-a97f-e30e91cbbdb6'
  },
  {
    title : "System↑OverTension",
    icon_image : "/images/cg/sot.png",
    image : 'https://firebasestorage.googleapis.com/v0/b/eternal-tune.appspot.com/o/images%2Fcg%2FSystemOverTension-bg.png?alt=media&token=e60b2918-3bed-43d2-8788-1924f28e642e'
  },
  {
    title : "Rate Cranky Hi-roller",
    icon_image : "/images/cg/cranky.png",
    image : 'https://firebasestorage.googleapis.com/v0/b/eternal-tune.appspot.com/o/images%2Fcg%2FRate_Cranky_Hi-roller-bg.png?alt=media&token=a71df17b-3e20-46d8-b9b2-cd6acccb81c1'
  }
]

const Creation = () =>
(
  <React.Fragment>
    <HeadHelmet text={"Others/Creation"} />
    <div id="main" className="column">
      <h2>{"Works"}</h2>
      <h3>{"画像"}</h3>
      <LightboxList />
      <div className="clear"></div>
      <h3>{"動画"}</h3>
      <p>
        マイペースに製作中。<br/>
      </p>
      <h3>{"ツール"}</h3>
      <p>
        ・<a href="/download/zip/ssc-file.zip">SSCFile(StepMania5 File Format syntax highlighting, autocomplete & snippets)</a><br/>
        ・VTube配信環境ソフト(制作中)<br/>
        <img className="pict" src="/images/pic/vtube300.png"  width="80%" height="80%"/><br/>
      </p>
      <h3>{"GAME"}</h3>
      {/*足でプレイするj◇beatをオマージュした音楽ゲームです。<br/>*/}
      <p>
        学部生の間に趣味で制作した音ゲーです。<br/>
        DANCE ELENATION<br/>
        新タイトルに変更しました<br/>
        <a href="https://danceelenation.web.app/"><img className="pict" src="/images/elebeat.png" /></a><br/>
      </p>
      <h3>{"Script"}</h3>
      <p>
        <a href="http://na24ddr.web.fc2.com/other/script/mandelblot.html" target="_blank">マンデルブロ集合</a><br/>
        <a href="http://na24ddr.web.fc2.com/other/script/note.html" target="_blank">譜面画像生成</a><br/>
        <a href="http://na24ddr.web.fc2.com/other/script/mandelbox.html" target="_blank">MandelBoxTrip(Windows版)</a><br/>
      </p>
      <h3>{"参加パッケージ"}</h3>
      <p>
        D3NEX<br/>
        etc...<br/>
      </p>
      <h3>{"過去製作物"}</h3> ※現在公開停止
      <p>
        StepManiaの曲を作るぜ！[DWI作成-職人編-]<br/>
        Dance Dance Revolution 1st-2nd MIX (NONSTOP MEGAMIX)<br/>
        <a href="http://www.nicovideo.jp/watch/sm11851964"><img className="pict" src="/images/cg/ddr1st2nd-bn.png" alt="DDR 1st-2nd MIX" /></a><br/>
      </p>
    </div>
  </React.Fragment>
)

const appear = rect => ({
  translateY: rect.top - 10,
  scale: 1.1,
  opacity: 0
});

const appeared = () => ({
  scale: 1,
  opacity: 1
});

class LightboxList extends Component
{
  constructor(props)
  {
      super(props);

      this.state = {
          photoIndex: 0,
          isOpen: false
      };

      this.pushMessage = this.pushMessage.bind(this);
  }

  pushMessage(imageIndex)
  {
      this.setState({ isOpen: true });
      this.setState({ photoIndex: imageIndex });
  }

  render()
  {
    const {
      photoIndex,
      isOpen
    } = this.state;

    let imageBoxList = [];

    let i=0;
    cgImages.forEach(element => {
      imageBoxList.push(<ImageBox key={i} num={i} src={element.icon_image} onClickImageEvent={this.pushMessage}/>);
      i++;
    });

    return (
      <div>
        <div style={{'height':'100%'}}>
          <StackGrid
            columnWidth={100}
            monitorImagesLoaded={true}
            appear={appear}
            appeared={appeared}
          >
            {imageBoxList}
          </StackGrid>
        </div>

        {isOpen &&
          <Lightbox
            mainSrc={cgImages[photoIndex].image}
            nextSrc={cgImages[(photoIndex + 1) % cgImages.length].image}
            prevSrc={cgImages[(photoIndex + cgImages.length - 1) % cgImages.length].image}
            imageTitle={cgImages[photoIndex].title}
            onCloseRequest={() => this.setState({ isOpen: false })}
            onMovePrevRequest={() => this.setState({
                photoIndex: (photoIndex + cgImages.length - 1) % cgImages.length
            })}
            onMoveNextRequest={() => this.setState({
                photoIndex: (photoIndex + 1) % cgImages.length
            })}
          />
        }
      </div>
    );
  }
}

class ImageBox extends Component
{
    constructor(props)
    {
      super(props);
      this.clickEvent = this.clickEvent.bind(this);
    }

    clickEvent(index)
    {
      this.props.onClickImageEvent(index);
      return;
    }

    render() {
      return (
        <div key={this.props.num} style={{'float':'left' , 'cursor': 'pointer'}} data-index={this.props.num} onClick={()=> this.clickEvent(this.props.num)} >
            <img className='pict' src={this.props.src} width={100} height={100}/>
        </div>
      );
    }
}

export default Creation;

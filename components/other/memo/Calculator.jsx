import React from 'react';
import { InlineMath, BlockMath } from 'react-katex';
import NumericInput from 'react-numeric-input';
import HeadHelmet from '../../common/HeadHelmet';
import 'katex/dist/katex.min.css';
import FA from 'react-fontawesome';

const MAX_N = 99;
const MIN_N = 2;

const Calculator = React.memo(() =>
  <React.Fragment>
    <HeadHelmet text="Others/Calcurate" />
    <div id="main" className="column">
      <h2>簡易計算ツール</h2>
      <p>
        譜面制作時に便利な機能をまとめていきます。<br/>
        ArrowVortexに拡張Pluginが無いので、こちらのページを開きながら使用する想定。<br/>
        (随時更新予定)
      </p>
      <ChangeBPMBox />
      {/* <Rader />
      <Difficulty /> */}
    </div>
  </React.Fragment>
)

const ChangeBPMBox = () =>
(
  <React.Fragment>
    <h3>元のオフセットに戻るソフランのBPMを求める</h3>
    <img src="/images/pic/changebpm_cal.png" />
    <p>
      求めたい<InlineMath math="BPM_n"/> の計算式は<br/>
    </p>
    <BlockMath math="f(n,\bm{BPM},\bm{p})= \frac{BaseBPM \times p_n}{\sum_{k=1}^n p_k - \sum_{k=1}^{n-1}(\frac{BaseBPM}{BPM_k}\times p_k)}"/>
    <p>
      BPMがマイナスになる時は足りない時間分ワープして戻ります。<br/>
      変更<InlineMath math="BPM_k"/>が<InlineMath math="BaseBPM"/>と同じ場合、<InlineMath math="BPM_n"/>は<InlineMath math="p_k"/>の値に対して不変です。
    </p>
    <ChangeBPM />
  </React.Fragment>
)

class ChangeBPM extends React.PureComponent
{

  constructor()
  {
    super();
    this.OnChangeN = this.OnChangeN.bind(this);
    this.OnChangeBBPM = this.OnChangeBBPM.bind(this);
    this.OnChangeCBPM = this.OnChangeCBPM.bind(this);
    this.OnChangeP = this.OnChangeP.bind(this);
    this.OnChangeStopTime = this.OnChangeStopTime.bind( this);
    this.OnClickCopy = this.OnClickCopy.bind(this);

    this.state = {
      m_baseBPM: 160,
      m_changeBPM: [320,160],
      m_p: [2,1,1],
      m_N: 3,
      m_stopTime: 0
    }
  }

  // 変化回数更新時
  OnChangeN(value)
  {
    if(value < MIN_N)
      value = MIN_N;

    if(MAX_N < value)
      value = MAX_N;

    let N = Math.floor(value);
    let changeBPM = [];
    let p = [];
    //ChangeBPM
    for(let i=0; i<N-1; i++)
    {
      if(i < this.state.m_changeBPM.length)
      {
        changeBPM.push(this.state.m_changeBPM[i]);
      }
      else
      {
        changeBPM.push(this.state.m_changeBPM[N-2]);
      }
    }

    //p
    for(let i=0; i<N; i++)
    {
      if(i < this.state.m_p.length)
      {
        p.push(this.state.m_p[i]);
      }
      else
      {
        p.push(this.state.m_p[N-1]);
      }
    }

    this.setState({m_N: N});
    this.setState({m_changeBPM: changeBPM});
    this.setState({m_p: p});
  }

  // 基礎BPM更新時
  OnChangeBBPM(value)
  {
    this.setState({m_baseBPM: value});
  }

  // BPM変化更新時
  OnChangeCBPM(value,num)
  {
    let changeBPM = [this.state.m_N-1];
    for(let i=0;i<this.state.m_N-1;i++)
    {
      if(i===num)
        changeBPM[i] = Number(value);
      else
        changeBPM[i] = this.state.m_changeBPM[i];
    }
    this.setState({m_changeBPM: changeBPM});
  }

  // 比更新時
  OnChangeP(value,num)
  {
    let p = [this.state.m_N];
    for(let i=0;i<this.state.m_N;i++)
    {
      if(i===num)
        p[i] = Number(value);
      else
        p[i] = this.state.m_p[i];
    }
    console.log(p);
    this.setState({m_p:p});
  }

  // 停止合計時間更新時
  OnChangeStopTime(value)
  {
    this.setState({m_stopTime: Number(value)});
  }

  OnClickCopy()
  {
    let copyTarget = document.getElementById("copyTarget");
    copyTarget.select();
    document.execCommand("Copy");
  }

  render()
  {
    let myFormat = (num) => {
      return Math.floor(num);
    }

    //ChangeBPM
    let changeBPM = [];

    for(let i=0;i<this.state.m_N-1; ++i)
    {
      let val = this.state.m_changeBPM[i];
      let cbpmText = "BPM_{" + (i+1) + "}";
      changeBPM.push(
      <div>
        <InlineMath math={cbpmText}/> = <NumericInput type="number" min={0} max={100000} value={val} onChange={(e) => this.OnChangeCBPM(e,i)} />
      </div>);
    }

    //P
    let p = [];

    for(let i=0; i<this.state.m_N; ++i)
    {
      let val = this.state.m_p[i];
      let pText = "p_{" + (i+1) + "}";
      p.push(
      <div>
        <InlineMath math={pText}/> = <NumericInput type="number" min={1} max={100000} value={val} onChange={(e) => this.OnChangeP(e,i)} />
      </div>);
    }

    let N = this.state.m_N;
    let totalP=0;
    for(let i=0;i<N;++i)
    {
      totalP += this.state.m_p[i];
    }

    // let totalPC=0;
    // for(let i=0;i<N-1;++i)
    // {
    //   totalPC += this.state.m_p[i] * (this.state.m_baseBPM/this.state.m_changeBPM[i]);
    // }
    // let answerBPM = (this.state.m_baseBPM * this.state.m_p[N-1]) / (totalP - totalPC);
    // console.log(totalP + " - "+ totalPC);

    let t =[];
    for(let i=0;i<N-1;i++)
    {
      t.push((this.state.m_baseBPM/this.state.m_changeBPM[i])*(this.state.m_p[i]/totalP));
    }

    let tTotal=0;
    for(let i=0;i<t.length;i++)
    {
      tTotal += t[i];
    }

    let answerBPM = (this.state.m_baseBPM * this.state.m_p[N-1]) / ((1.0 - tTotal)*totalP); //+ (this.state.m_baseBPM / ((this.state.m_stopTime * this.state.m_baseBPM / 60.0) + this.state.m_lastBeat));
    // 1 / ((0.069 *145/60) + 0.5) * 145
    //誤差を丸める.
    answerBPM = Math.round(100000.0 * answerBPM) / 100000.0;
    let bpmText = "BPM_{" + N + "}";
    let pSumText = "\\sum_{k=1}^{"+N+"} p_k";

    return(
      <React.Fragment>
        <p>
          (<InlineMath math="n"/> = <NumericInput type="number" min={2} max={99} value={this.state.m_N} onChange={this.OnChangeN} format={myFormat} />の時)<br/>
          <br/>
          <InlineMath math="BaseBPM"/>  =  <NumericInput type="number" min={0} max={100000} value={this.state.m_baseBPM} onChange={this.OnChangeBBPM} /><br/>
          {changeBPM}
          {p}
          (pはbeat数の比)<br/>
          {/* <InlineMath math="TotalStopTime"/>  =  <NumericInput type="number" min={0} max={100000} value={this.state.m_stopTime} onChange={this.OnChangeStopTime} /><br/> */}
          <br/>
          <InlineMath math="\bm{p}"/> の合計 {totalP}<br/>
        求めたい<InlineMath math={bpmText}/> = <input id="copyTarget" type="text" value={answerBPM} readonly></input><button class="button is-small" onClick={this.OnClickCopy} >Copy  <FA name="edit" style={{ color: 'rgba(0, 0, 0, 1)' }}/></button>
        </p>
      </React.Fragment>
    );
  }
}

class Rader extends React.PureComponent
{
  render()
  {
    return(
      <React.Fragment>
        <h3>独自レーダー作成</h3>
      <p>
        <code>
          Stream
          分間ノート数 = Floor( 60 * ノート数 / 曲の長さ[秒] )
          300以上：Stream値 = ( 分間ノート数 - 139 ) * 100 / 161
          300以下：Stream値 = 分間ノート数 / 3

          Voltage
          平均BPM = 60 * 曲の長さ[拍] / 曲の長さ[秒]
          最大密度ノート数 = 最も密度の高い4拍のノート数（SA含む）
          において、
          最大密度の分間ノート数 = Floor( 平均BPM * 最大密度ノート数 / 4 )
          最大密度の分間ノート数が
          600以上：Voltage値 = ( 最大密度の分間ノート数 + 330 ) * 10 / 93
          600以下：Voltage値 = 最大密度の分間ノート数 / 6

          Air
          分間ジャンプ数 = Floor( 60 * ( 同時押しノート数 + SA数 ) / 曲の長さ[秒] )
          分間ジャンプ数が
          55以上：Air値 = ( 分間ジャンプ数 + 5 ) * 5 / 3
          55以下：Air値 = 分間ジャンプ数 * 20 / 11

          Freeze
          （※X2と同じ）
          FA総延長 = 全てのFAの長さ[拍]の合計
          ※ただし、同時に始まるFAの場合、短い方（同時に終わるなら片側）を無視
          において、
          FA率 = Floor( 10000 * FA総延長 / 曲の長さ[拍] )
          FA率が
          3500以上：Freeze値 = ( FA率 + 2484 ) * 100 / 5984
          3500以下：Freeze値 = FA率 / 35

          Chaos
          （※X2と同じ）
          以下のサイトを参考にして作成しています。（外部）
          http://mh-sciz.jugem.jp/?eid=5

          ノートの変則値 = 矢印数 * 色補正 / 直前のノート（SA含む）との間隔[拍]
          ※矢印数：単押しなら1、同時押しなら2、SAは矢印の数（通常SPは4、DPは8））
          ※色補正：NOTE色分けによる色が、赤 = 0, 青 = 2, 黄 = 4, 緑 = 5
          　（FA始点やSAなど色分けされていないノートも、通常ノートと同じく色分けされているものとみなす）
          変則度基本値 = 全てのノート（SA含む）の変則値の合計

          合計BPM変化量 = BPM変化とストップにおける変化量の合計
          ※BPM変化の場合、変化前と変化後のBPM値の差。
          ※ストップの場合、ストップ直後のBPM値。
          ※BPM変化とストップが同時の（ストップ前後でBPMが変わる）場合、ストップによる値のみを考慮する。
          ※例としてPOSSESSIONの場合370-185-0-370-185-0-185-370なので、
          　合計BPM変化量は(370-185)+370+(370-185)+185+(370-185)=1110
          分間BPM変化量 = 60 * 合計BPM変化量 / 曲の長さ[秒]

          変則度 = 変則度基本値 * ( 1 + ( 分間BPM変化量 / 1500 ) )
          単位変則度 = 変則度 * 100 / 曲の長さ[秒]
          単位変則度が
          2000以上：Chaos値 = ( 単位変則度 + 21605 ) * 100 / 23605
          2000以下：Chaos値 = 単位変則度 / 20
          ※ただし2000以上のほうの式は暫定的なものです。（曲CHAOS SP鬼の単位変則度を25605として計算）

          参考URL:https://www21.atwiki.jp/asigami/pages/1037.html
        </code>
      </p>

      </React.Fragment>

    );
  }
}

class Difficulty extends React.PureComponent
{
  render()
  {
    return(
      <React.Fragment>
        <h3>難易度推定</h3>
      </React.Fragment>
    );
  }
}

export default Calculator;

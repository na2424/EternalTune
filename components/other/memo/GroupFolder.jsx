import React, { Component } from 'react';
import { Helmet } from 'react-helmet';
import SideMemoBox from '../../other/memo/SideMemoBox';
import HeadHelmet from '../../../components/common/HeadHelmet';

const GroupFolder = React.memo(() =>
{
  return (
    <React.Fragment>
      <HeadHelmet text="Others/GroupFolder" />
      <div id="main" className="column">
        <h2>グループフォルダ名を日本語にする</h2>
        <p>
          StepManiaの選曲画面で曲名やアーティスト名を日本語で表示させるには、<br/>
          smファイル（5.0ならsscファイル）に日本語で記述して、文字コード「UTF-8」で保存します。<br/>
          これは知っている人も多いと思います。<br/>
          <br/>
          では、グループフォルダ名は日本語で表記できないのか？<br/>
          <br/>
          実はできます。<br/>
          ただし、不完全です。<br/>
          <br/>
          以下、OSはWindows、StepMania本体バージョンは3.9または3.9＋を前提にします。<br/>
          <br/>
          Windowsではフォルダ名は内部的にShift-JISで記録されています。<br/>
          そこで、「UTF-8で解釈したら日本語になるコードを、Shift-JISで解釈したもの」をフォルダ名にすれば、<br/>
          選曲画面上ではUTF-8で解釈されて日本語になるはずです。<br/>
          <br/>
          例として、今回はフリーのテキストエディタ「TeraPad」を使います。<br/>
          保存時にUTF-8またはUTF-8Nを指定でき、<br/>
          読み込み時に解釈する文字コードを指定できるテキストエディタなら、<br/>
          他のものでも構いません。メモ帳やワードパッドは不可。<br/>
          <br/>
          フォルダ名にしたい日本語を打ち込んだら、<br/>
          ファイル→文字/改行コード指定保存で文字コード「UTF-8N」を指定して適当な場所に保存します<br/>
          (UTF-8でも構いませんが、ちょっと手間が増えます)。<br/>
          <br/>
          続いてファイル→文字コード指定再読み込みで「SHIFT-JIS」を指定して再読み込みします。<br/>
          すると日本語が化けて表示されるはずです。<br/>
          これをグループフォルダ名に指定すると、StepManiaの画面上では最初の日本語が表示されるはずです<br/>
          (UTF-8で保存した場合、頭の「・ｿ」は余分なので取り除きます)。<br/>
          <br/>
          例えば「アニソン」なら「繧｢繝九た繝ｳ」、「東方」なら「譚ｱ譁ｹ」、<br/>
          「Eternal∞Tune」なら「Eternal竏杁une」になります。<br/>
          (∞もそのままでは表示できないので日本語と同じ扱いをします)。<br/>
          <br/>
          ただし、化けた中に・（中点）が含まれているとうまく行きません。<br/>
          これは「Shift-JISでは解釈できない」ことを示すからです。<br/>
          その場合は諦めてください。<br/>
          例えば「初音ミク」は「蛻晞浹繝溘け」なので可能ですが、「重音テト」は「驥埼浹繝・ヨ」なので不可能です。<br/>
          <br/>
          なお、5.0ではテーマによっては日本語フォルダ名を表示するため、<br/>
          専用の設定ファイルを利用するものもあります（Theme waieiなど）。<br/>
          この場合は「重音テト」なども表示可能です。<br/>
          <br/>
          MacやLinuxではいろいろ変わってくると思いますが、詳しくは分かりません。<br/>
          Linuxはフリーなのでそのうち検証するかも知れません。<br/>
          <br/>
          解説：Dolzark<br/>
        </p>
      </div>
    </React.Fragment>
  )
})

export default GroupFolder;

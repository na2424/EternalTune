import * as React from 'react';
import { Helmet } from 'react-helmet';

const command_sm = [
    //StepMania3.9
    {
        "list" : [{
            "title" : "すべての画面で使えるコマンド",
            "command" : [
                {
                    "action" : "Tab",
                    "value" : "押している最中のみ２倍速で動作"
                },
                {
                    "action" : "Esc(Backに設定されているキー)",
                    "value" : "前の画面に戻る"
                },
                {
                    "action" : "Alt＋Enter",
                    "value" : "ウィンドウとフルスクリーンの切り替え"
                },
                {
                    "action" : "F2",
                    "value" : "テーマのリロード"
                },
                {
                    "action" : "Scroll Lock",
                    "value" : "オプション画面に移動"
                },
                {
                    "action" : "Print Screen",
                    "value" : "スクリーンショットをjpgでScreenshotsフォルダに保存。"
                }
            ]
        },
        {
            "title" : "プレイ中のコマンド",
            "command" : [
                {
                    "action" : "Esc(Backに設定されているキー)",
                    "value" : "選曲画面に戻る"
                },
                {
                    "action" : "F5",
                    "value" : "強制クリア"
                },
                {
                    "action" : "F7",
                    "value" : "ハンドクラップのON/OFF"
                },
                {
                    "action" : "F8",
                    "value" : "オートプレイのON/OFF"
                },
                {
                    "action" : "F11",
                    "value" : "譜面に対し曲の再生位置を遅くする"
                },
                {
                    "action" : "F12",
                    "value" : "譜面に対し曲の再生位置を早くする"
                }
            ]
        },
        {
            "title" : "選曲中のコマンド",
            "command" : [
                {
                    "action" : "↑↑",
                    "value" : "難易度を下げる"
                },
                {
                    "action" : "↓↓",
                    "value" : "難易度を上げる"
                },
                {
                    "action" : "↑↓↑↓",
                    "value" : "ソートメニューを開く"
                },
                {
                    "action" : "←＋→",
                    "value" : "次のソート"
                },
                {
                    "action" : "←→×４",
                    "value" : "オプションを全てキャンセル"
                }
            ]
        },
        {
            "title" : "SongsEDIT中のコマンド",
            "command" : [
                {
                    "action" : "1234(5678)",
                    "value" : "矢印(Note)を配置・削除"
                },
                {
                    "action" : "↑↓",
                    "value" : "カーソルの移動"
                },
                {
                    "action" : "→",
                    "value" : "カーソルの移動範囲を細かくする(4分,8分,12分,16分,24分...)"
                },
                {
                    "action" : "←",
                    "value" : "カーソルの移動範囲を広くする"
                },
                {
                    "action" : "Esc(Escape)",
                    "value" : "メインメニューを開く"
                },
                {
                    "action" : "スペースキー",
                    "value" : "エリアの開始点と終止点の選択"
                },
                {
                    "action" : "Enter,Return",
                    "value" : "エリアメニューを開く"
                },
                {
                    "action" : "F4",
                    "value" : "テストプレイ中にハンドクラップを付ける"
                },
                {
                    "action" : "F7",
                    "value" : "BPMを0.02下げる"
                },
                {
                    "action" : "F8",
                    "value" : "BPMを0.02上げる"
                },
                {
                    "action" : "F11",
                    "value" : "譜面に対し曲の再生位置を0.02秒遅くする"
                },
                {
                    "action" : "F12",
                    "value" : "譜面に対し曲の再生位置を0.02秒早くする"
                },
                {
                    "action" : "Ctrl＋↓",
                    "value" : "２倍ズーム"
                },
                {
                    "action" : "Ctrl＋↑",
                    "value" : "２倍縮小"
                },
                {
                    "action" : "F5",
                    "value" : "１つ下の難易度に移動"
                },
                {
                    "action" : "F6",
                    "value" : "１つ上の難易度に移動"
                },
                {
                    "action" : "Home",
                    "value" : "一番上にカーソルを移動"
                },
                {
                    "action" : "End",
                    "value" : "譜面の一番下にカーソルを移動"
                },
                {
                    "action" : "PageUp",
                    "value" : "１小節前にカーソルを移動"
                },
                {
                    "action" : "PageDown",
                    "value" : "１小節後にカーソルを移動"
                },
                {
                    "action" : "B",
                    "value" : "BGチェンジメニューを開く"
                },
                {
                    "action" : "M",
                    "value" : "プレビュー再生(選曲画面で流れる部分を再生)"
                },
                {
                    "action" : "｢",
                    "value" : "プレビュー再生の開始点を0.02秒早くする"
                },
                {
                    "action" : "｣",
                    "value" : "プレビュー再生の開始点を0.02秒遅らせる"
                },
                {
                    "action" : "Alt＋(数値変更操作)",
                    "value" : "通常より細かい数値変更操作になる"
                }
            ]

        }

    ]},
    //StepMania5
    {
    "list" : [{
            "title" : "すべての画面で使えるコマンド",
            "command" : [
                {
                    "action" : "Tab",
                    "value" : "押している最中のみ２倍速で動作"
                },
                {
                    "action" : "Esc(Backに設定されているキー)",
                    "value" : "前の画面に戻る"
                },
                {
                    "action" : "Alt＋Enter",
                    "value" : "ウィンドウとフルスクリーンの切り替え"
                },
                {
                    "action" : "F2",
                    "value" : "テーマのリロード"
                },
                {
                    "action" : "F3",
                    "value" : "デバックメニューを開く"
                },
                {
                    "action" : "Scroll Lock",
                    "value" : "オプション画面に移動"
                },
                {
                    "action" : "Print Screen",
                    "value" : "スクリーンショットをjpgでScreenshotsフォルダに保存。"
                }
            ]
        },
        {
            "title" : "プレイ中のコマンド",
            "command" : [
                {
                    "action" : "Esc(Backに設定されているキー)",
                    "value" : "選曲画面に戻る"
                },
                {
                    "action" : "F4",
                    "value" : "オフセットの変更を破棄"
                },
                {
                    "action" : "F6",
                    "value" : "自動オフセットのON/OFF"
                },
                {
                    "action" : "F7",
                    "value" : "ハンドクラップのON/OFF"
                },
                {
                    "action" : "F8",
                    "value" : "オートプレイのON/OFF"
                },
                {
                    "action" : "F11",
                    "value" : "譜面のオフセットを減らす"
                },
                {
                    "action" : "F12",
                    "value" : "譜面のオフセットを増やす"
                },
                {
                    "action" : "Shift + F11",
                    "value" : "筐体のオフセット(グローバルオフセット)を減らす"
                },
                {
                    "action" : "Shift + F12",
                    "value" : "筐体のオフセット(グローバルオフセット)を増やす"
                }
            ]
        },
        {
            "title" : "選曲中のコマンド",
            "command" : [
                {
                    "action" : "↑↑",
                    "value" : "難易度を下げる"
                },
                {
                    "action" : "↓↓",
                    "value" : "難易度を上げる"
                },
                {
                    "action" : "↑+↓",
                    "value" : "フォルダを閉じる(SimplyLove,CyberiaStyle)"
                },
                {
                    "action" : "←＋→",
                    "value" : "次のソート"
                }
            ]
        }
    ]}
]

interface IMainBoxProps {}

interface IMainBoxState
{
    verType?: number
}

class MainBox extends React.Component<IMainBoxProps, IMainBoxState>
{
    constructor(props: IMainBoxProps)
    {
        super(props);
        this.state = {
            verType: 0
        };
        this.changeStepManiaVersion = this.changeStepManiaVersion.bind(this);
        this.getVersionText = this.getVersionText.bind(this);
    }

  changeStepManiaVersion(event)
  {
    let type = Number(event.target.value) - 1;
    this.setState({verType: type});
  }

  getVersionText()
  {
    const texts = [
      "StepMania3.9",
      "StepMania5"
    ]
    return texts[this.state.verType];
  }

  render()
  {
    let commands = command_sm[this.state.verType].list.map((i)=>{return <CommandList title={i.title} command={i.command} /> });
    return(
      <React.Fragment>
        <Helmet>
          <title>Eternal∞Tune(StepMania用DWI配布サイト)/Others</title>
        </Helmet>
        <div id="main" className="column">
          <h2>Stepmaniaのコマンド紹介</h2>
          <p>
            <div className="selectVersionRadio" onChange={event => this.changeStepManiaVersion(event)}>
              <input type="radio" name="version" id="select1" value="1" checked = { this.state.verType === 0 } />
              <label htmlFor="select1">StepMania3.9</label>
              <input type="radio" name="version" id="select2" value="2" checked = { this.state.verType === 1 } />
              <label htmlFor="select2">StepMania5(WIP)</label>
            </div>
          </p>
          <p>
          {this.getVersionText()}でよく使いそうなコマンドをまとめました。<br/>
          {commands} <br/>
          </p>
        </div>
      </React.Fragment>
    );
  }
}

interface ICommandList
{
    command : any,
    title : string
}

const CommandList : React.SFC<ICommandList> = (props) =>
{
  let pairs = props.command.map((i)=> { return <CommandPair action={i.action} value={i.value} />});

  return(
    <div>
      <h3>{props.title}</h3>
      <table className="table">
        <tbody>
        <tr>
          <th>{"キー"}</th>
          <th>{"内容"}</th>
        </tr>
        {pairs}
        </tbody>
      </table>
    </div>
  );
}

const CommandPair =(props)=>(
  <tr>
    <td>{props.action}</td>
    <td>{props.value}</td>
  </tr>
)

export default MainBox;

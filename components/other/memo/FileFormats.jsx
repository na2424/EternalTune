﻿import React, { Component } from 'react';
import { Helmet } from 'react-helmet';
import SideMemoBox from '../../other/memo/SideMemoBox';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import "react-tabs/style/react-tabs.css";
import HeadHelmet from '../../../components/common/HeadHelmet';

class FileFormats extends Component {
  constructor() {
    super();
    this.state = { tabIndex: 0 };
  }

  render() {
    return (
      <React.Fragment>
        <HeadHelmet text = {"Others/FileFormats"} />
        <div id="main" className="column">
          <h2>譜面ファイルの書式</h2>
          <div className="note-format">
            <Tabs selectedIndex={this.state.tabIndex} onSelect={tabIndex => this.setState({ tabIndex })}>
              <TabList>
                <Tab>DWIファイル</Tab>
                <Tab>SMファイル</Tab>
                <Tab>SSCファイル</Tab>
              </TabList>
              <TabPanel><DWI/></TabPanel>
              <TabPanel><SM/></TabPanel>
              <TabPanel><SSC/></TabPanel>
            </Tabs>
            <h3 className="clear">参考文献</h3>
            <dt>Document</dt>
            <dd>
              <a href="https://github.com/stepmania/stepmania/wiki/File-Formats">StepMania公式ドキュメント File Formats</a><br/>
              <a href="http://ec2.stepmania.com/wiki/Category:Simfile_Formats">ec2.stepmania.com wiki</a><br/>
            </dd>
            <dt>Article</dt>
            <dd>
              <a href="http://www.kyleaward.com/faq/stepmania/">kyleaward.com</a><br/>
              <a href="https://www.reddit.com/r/rhythmgames/comments/460aqm/ddr_piu_or_itg_why/">DDR, PIU, or ITG. Why?</a><br/>
            </dd>
            <h2>LuaAPI,Xmlファイルの書式</h2>
            <p>
                OpenITG/NotITGのMods譜面を作るのに使用される。<br/>
                BG ChangeのファイルにXmlを指定してLuaAPIを使用することができます。<br/>
                <a href="https://sm.heysora.net/doc/">参考先</a>
            </p>
            </div>
        </div>
      </React.Fragment>
    );
  }
}

const DWI =() =>(
  <React.Fragment>
  <p>
    StepManiaが対応したDance With Intensityの譜面ファイル<br/>
    <br/>
    DWI形式の書式のメモ。<br/>
    :(コロン)と;(セミコロン)の間に文字が入る。<br/>
    <br/>
    <dl>
        <dt>#TITLE:;</dt>
        <dd>曲名を指定。</dd>
        <dt>#ARTIST:;</dt>
        <dd>アーティスト名を指定。</dd>
        <dt>#DISPLAYTITLE:;</dt>
        <dd>曲選択時に表示する曲名を指定。</dd>
        <dt>#DISPLAYARTIST:;</dt>
        <dd>曲選択時に表示するアーティスト名を指定。</dd>
        <dt>#BPM:;</dt>
        <dd>曲のBPMを指定。</dd>
        <dt>#DISPLAYBPM:;</dt>
        <dd>曲選択時に表示するBPMを指定。*を指定するとランダムに表示される。<br/>
        また、X..Y(X.Yは数字)を指定するとBPMがX〜Yと表示する。(SMと書式が異なる)。</dd>
        <dt>#CHANGBPM:;</dt>
        <dd>BPMを「どこで」「どの値に変更するか」を指定。</dd>
        <dt>#FREEZE:;</dt>
        <dd>曲を「どこで」「何秒停止させるか」を指定。</dd>
        <dt>#GAP:;</dt>
        <dd>譜面に対して曲がどれだけ遅れてスタートするかを指定。</dd>
        <dt>#SAMPLESTART:;</dt>
        <dd>曲選択時に曲のどの部分をサンプルとして流すかを指定。</dd>
        <dt>#SAMPLEELENGTH;</dt>
        <dd>曲選択時にサンプルを何秒流すかを指定。</dd>
        <dt>#FILE:;</dt>
        <dd>曲ファイルを指定。</dd>
        <dt>#CDTITLE:;</dt>
        <dd>CDタイトルを指定。</dd>
    </dl>
  </p>
  </React.Fragment>
)

const SM = () =>(
  <React.Fragment>
  <p>
    StepManiaで一般的に使用される譜面ファイル。<br/>
    <dl>
      <dt>#TITLE:;</dt>
      <dd>曲名を指定。</dd>
      <dt>#SUBTITLE:;</dt>
      <dd>曲名のサブタイトルを指定。</dd>
      <dt>#ARTIST:;</dt>
      <dd>アーティスト名を指定。</dd>
      <dt>#TITLETRANSLIT:;</dt>
      <dd>曲名の読み方を指定。文字化け対策とソートの基準になる。</dd>
      <dt>#ARTISTTRANSLIT:;</dt>
      <dd>アーティスト名の読み方を指定。文字化け対策とソートの基準になる。</dd>
      <dt>#CREDIT:;</dt>
      <dd>譜面製作者のコメントを記入。</dd>
      <dt>#BANNER:;</dt>
      <dd>曲のバナー画像を指定。ファイル名の語尾に-bnで自動指定。</dd>
      <dt>#BACKGROUND:;</dt>
      <dd>曲の背景画像を指定。ファイル名の語尾に-bgで自動指定。</dd>
      <dt>#LYRICSPATH:;</dt>
      <dd>拡張子が.lrcの歌詞ファイルを指定。</dd>
      <dt>#CDTITLE:;</dt>
      <dd>CDタイトルを指定。</dd>
      <dt>#MUSIC:;</dt>
      <dd>曲ファイルを指定。</dd>
      <dt>#MUSICLENGTH:;</dt>
      <dd>曲の長さを指定。</dd>
      <dt>#MUSICBYTES:;</dt>
      <dd></dd>
      <dt>#OFFSET:;</dt>
      <dd>譜面に対する曲の開始位置。</dd>
      <dt>#SAMPLESTART:;</dt>
      <dd>曲選択時にサンプルを何秒から流すかを指定。</dd>
      <dt>#SAMPLELENGTH:;</dt>
      <dd>曲選択時にサンプルを何秒流すかを指定。</dd>
      <dt>#SELECTABLE:YES/ROULETTE/NO;</dt>
      <dd>YES →曲選択時に選曲可能。<br/>
      ROULETTE→曲選択時にルーレットのみで選曲可能。<br/>
      NO  →曲選択時に選曲不可。<br/>
      ※StepManiaの設定に依存。</dd>
      <dt>#DISPLAYBPM:;</dt>
      <dd>曲選択時に表示するBPMを指定。*を指定するとランダムに表示される。<br/>
      また、X:Y(X,Yは数字)を指定するとBPMがX〜Yと表示される。(DWIと書式が異なる)</dd>
      <dt>#BPMS:;</dt>
      <dd>曲のBPMを指定。<br/>
      少なくとも初期位置でBPMを定義する必要があります。<br/>
      (例)<br/>
      0.000=191.000,72.000=382.000<br/>
      BPMを変更する場所の位置＝BPM<br/>
      ,(カンマ)で区切る。</dd>
      <dt>#STOPS:;</dt>
      <dd>曲を「どこで」「何秒停止させるか」を指定。<br/>
      <br/>
      (例)<br/>
      72.000=0.628,203.500=0.707<br/>
      停止を入れる場所の位置＝何秒停止させるか<br/>
      ,(カンマ)で区切る。</dd>
      <dt>BGCHANGES:;</dt>
      <dd>背景画像(動画)を「どこで」「どの画像に変更するか」を指定。<br/>
      <br/>
      (例)<br/>
      440.000=df_vdeo.avi=1.000=1=0=0,<br/>
      770.000=black.JPG=1.000=1=0=0,<br/>
      [背景を変更する場所の位置]＝[ファイル名]＝[再生速度]＝[フェードのON(1)/OFF(0)]＝[巻き戻し再生のON/OFF]＝[ループ再生のON/OFF]<br/>
      </dd>
      <h4>譜面書式</h4>
      <dt>#NOTES:</dt>
      <dd>[NotesType]:<br/>
      譜面タイプ (DDR,PIU 等)<br/>
      [Description]:<br/>
      譜面詳細 (製作者名、Shock Arrow Challenge 等)<br/>
      [DifficultyClass]:<br/>
      難易度<br/>
      [DifficultyMeter]:<br/>
      難易度値<br/>
      [RadarValues]:<br/>
      レーダー値(STR,VOL,AIR,FREEZE,CHAOS)<br/>
      [NoteData]<br/>
      譜面データ<br/>
      <br/>
      (例)<br/>
      dance-single:<br/>
      NAOKI:<br/>
      Challenge:<br/>
      17:<br/>
      0.988,1.000,0.432,0.432,0.458:<br/>
      </dd>
      <dt>NotesTypeについて</dt>
      <dd>
          dance-singleの場合4文字ごとに改行されている。左から←↓↑→に対応。<br/>
          また、NotesTypeによって1行当たりの文字(Note)数が異なる。<br/>
          dance-single = 4 notes/row (Left,Down,Up,Right)<br/>
          dance-double = 8 notes/row<br/>
          dance-couple = 8 notes/row<br/>
          dance-solo = 6 notes/row<br/>
          pump-single = 5 notes/row<br/>
          pump-double = 10 notes/row<br/>
          pump-couple = 10 notes/row<br/>
          ez2-single = 5 notes/row<br/>
          ez2-double = 10 notes/row<br/>
          ez2-real = 7 notes/row<br/>
          para-single = 5 notes/row<br/>
          <br/>
      </dd>
      <dt>Noteの値について</dt>
      <dd>0 - 無し 'No Note'<br/>
      1 - 通常ノート 'Normal note'<br/>
      2 - FA始点 'Hold　head'<br/>
      3 - FA/Roll終点 'Hold/Roll tail'<br/>
      4 - ロール始点 'Roll head'　(3.9は非対応)<br/>
      M - 地雷 'Mine'<br/>
      StepMania5用の値(旧バージョンのStepManiaに未対応の値)<br/>
      K - 'Automatic keysound'<br/>
      L - リフト 'Lift note'<br/>
      F - フェイク 'Fake note'<br/>
      (ワープギミック間でノートを置くと判定が残るので、<br/>
      フェイクは見た目を爆弾ではなく通常ノートでワープさせたい時に使えます。)<br/>
      </dd>
      <dt>譜面間隔について</dt>
      <dd>
        小節ごとに,(カンマ)で区切る。<br/>
        小節単位で「１行あたりの譜面間隔=1小節/行数」と分割される。<br/>
        <br/>
        <img src="../../../images/memo/sakura.png" height="427" width="266" /><br/>
        (例)
        <br/>
        0001<br/>
        1000<br/>
        0100<br/>
        0010<br/>
        0100<br/>
        0001<br/>
        0100<br/>
        1000<br/>
        ,<br/>
        1000<br/>
        0000<br/>
        0000<br/>
        0000<br/>
        0001<br/>
        0000<br/>
        0000<br/>
        0000<br/>
        0100<br/>
        0000<br/>
        0000<br/>
        0000<br/>
        0010<br/>
        0000<br/>
        0000<br/>
        0000<br/>
        0200<br/>
        0000<br/>
        0000<br/>
        0000<br/>
        0000<br/>
        0000<br/>
        0000<br/>
        0000<br/>
        0000<br/>
        1000<br/>
        0000<br/>
        0000<br/>
        0000<br/>
        0000<br/>
        0000<br/>
        0000<br/>
        ,<br/>
        0301<br/>
        ...<br/>
      </dd>
    </dl>
  </p>
  </React.Fragment>
)

const SSC =()=>(
  <React.Fragment>

  <p>
    StepMania5から一般的に使用される譜面ファイル。<br/>
    (3.0final、3.9、3.95、4.0など以前のバージョンでは使用できない)<br/>
    各譜面毎に速度変化などのギミックオプションを変更できるようになった。(SplitBPM)<br/>
    <br/>
    <dl>
        <dt>#VERSION:0.73;</dt>
        <dd>各SSCファイルには、ステップファイルが作成されたバージョンを示すバージョンタグがあり、<br/>
        互換性を維持しながら変更を行えるように用意されている。</dd>
        <dt>#TITLE:Springtime;</dt>
        <dd>曲名を指定。</dd>
        <dt>#SUBTITLE:;</dt>
        <dd>曲名のサブタイトルを指定。</dd>
        <dt>#ARTIST:Kommisar;</dt>
        <dd>アーティスト名を指定。</dd>
        <dt>#TITLETRANSLIT:;</dt>
        <dd>曲名の読み方を指定。文字化け対策とソートの基準になる。</dd>
        <dt>#SUBTITLETRANSLIT:;</dt>
        <dd>曲名のサブタイトルの読み方を指定。文字化け対策とソートの基準になる。</dd>
        <dt>#ARTISTTRANSLIT:;</dt>
        <dd>アーティスト名の読み方を指定。文字化け対策とソートの基準になる。</dd>
        <dt>#GENRE:;</dt>
        <dd>ジャンルを指定。</dd>
        <dt>#ORIGIN:;</dt>
        <dd>出展を指定。</dd>
        <dt>#CREDIT:;</dt>
        <dd>譜面製作者のコメントを記入。</dd>
        <dt>#BANNER:springbn.png;</dt>
        <dd>曲のバナー画像を指定。ファイル名の語尾に-bnで自動指定。</dd>
        <dt>#BACKGROUND:spring.png;</dt>
        <dd>曲の背景画像を指定。ファイル名の語尾に-bgで自動指定。</dd>
        <dt>#LYRICSPATH:;</dt>
        <dd>拡張子が.lrcの歌詞ファイルを指定。</dd>
        <dt>#CDTITLE:;</dt>
        <dd>CDタイトルを指定。</dd>
        <dt>#MUSIC:Kommisar - Springtime.mp3;</dt>
        <dd>曲ファイルを指定。</dd>
        <dt>#OFFSET:-0.125000;</dt>
        <dd>譜面に対する曲の開始位置。</dd>
        <dt>#SAMPLESTART:105.760086;</dt>
        <dd>曲選択時にサンプルを何秒から流すかを指定。</dd>
        <dt>#SAMPLELENGTH:9.360000;</dt>
        <dd>曲選択時にサンプルを何秒流すかを指定。</dd>
        <dt>#SELECTABLE:YES/ROULETTE/NO;</dt>
        <dd>YES →曲選択時に選曲可能。<br/>
        ROULETTE→曲選択時にルーレットのみで選曲可能。<br/>
        NO  →曲選択時に選曲不可。<br/>
        ※StepManiaの設定に依存。</dd>
        <dt>#DISPLAYBPM:181.699997;</dt>
        <dd>曲選択時に表示するBPMを指定。*を指定するとランダムに表示される。<br/>
            また、X:Y(X,Yは数字)を指定するとBPMがX〜Yと表示される。</dd>
        <dt>#BPMS:0.000000=181.699997</dt>
        <dd>曲のBPMを指定。<br/>
            少なくとも初期位置でBPMを定義する必要があります。<br/>
            BPMを変更する場所の位置＝BPM<br/>
            ,(カンマ)で区切る。</dd>
        <dt>#STOPS:;</dt>
        <dd>譜面データ書式と同様</dd>
        <dt>#DELAYS:;</dt>
        <dd>譜面データ書式と同様</dd>
        <dt>#WARPS:;</dt>
        <dd>譜面データ書式と同様</dd>
        <dt>#TIMESIGNATURES:0.000000=4=4;</dt>
        <dd>譜面データ書式と同様</dd>
        <dt>#TICKCOUNTS:0.000000=2;</dt>
        <dd>Hold時の1拍当たりの判定数。</dd>
        <dt>#COMBOS:0.000000=1;</dt>
        <dd>1Note当たりのコンボの増分を表す。</dd>
        <dt>#SPEEDS:0.000000=1.000000=0.000000=0;</dt>
        <dd>譜面データ書式と同様</dd>
        <dt>#SCROLLS:0.000000=1.000000;</dt>
        <dd>譜面データ書式と同様</dd>
        <dt>#LABELS:0.000000=Song Start;</dt>
        <dd>譜面データ書式と同様</dd>
        <dt>#BGCHANGES:;</dt>
        <dd>背景画像(動画)を「どこで」「どの画像に変更するか」を指定。</dd>
        <dt>#KEYSOUNDS:;</dt>
        <dd></dd>
        <dt>#ATTACKS:;</dt>
        <dd>譜面データ書式と同様</dd>
    </dl>
    <h4>ここから譜面データ</h4>
    <dl>
        <dt>#NOTEDATA:;</dt>
        <dd></dd>
        <dt>#STEPSTYPE:dance-single;</dt>
        <dd>譜面タイプ(smファイルの書式参照)</dd>
        <dt>#DESCRIPTION:Blank;</dt>
        <dd>譜面詳細 (製作者名、Shock Arrow Challenge 等)</dd>
        <dt>#CHARTSTYLE:;</dt>
        <dd></dd>
        <dt>#DIFFICULTY:Challenge;</dt>
        <dd>難易度</dd>
        <dt>#METER:12;</dt>
        <dd>難易度値</dd>
        <dt>#RADARVALUES:;</dt>
        <dd>レーダー値(STR,VOL,AIR,FREEZE,CHAOS)</dd>
        <dt>#CREDIT:Blank;</dt>
        <dd>譜面製作者のコメントを記入。</dd>
        <dt>#OFFSET:-0.125000;</dt>
        <dd>譜面に対する曲の開始位置。</dd>
        <dt>#BPMS:0.000000=181.699997</dt>
        <dd>[beat number]=[BPM],[beat number]=[BPM],etc.<br/>
        ( [位置(開始からの拍数)] = [BPM]、[次の位置] = [BPM] ...繰り返し... )<br/>
        <br/>
        BPMセグメントを表します。譜面スクロールはBPMの値に基づいて速度を変更します。<br/>
        少なくとも初期位置でBPMを定義する必要があります。</dd>
        <dt>#STOPS:;</dt>
        <dd>[beat number]=[length of stop in seconds],etc.<br/>
        ( [位置] = [ストップ秒の長さ] )<br/>
        <br/>
        停止セグメントを表します。<br/>
        このセグメントでは、その行で譜面の判定を行ってから一定時間停止します。<br/>
        これはDance Dance Revolutionが停止処理する方法と似ています。</dd>
        <dt>#DELAYS:;</dt>
        <dd>[beat number]=[length of delay in seconds],etc.<br/>
        ( [位置] = [遅延の長さ(秒)] )<br/>
        <br/>
        遅延セグメントを表します。<br/>
        このセグメントでは、その行で譜面の判定を行う前に一定時間停止します。<br/>
        これはPump It Upが停止処理する方法と似ています。</dd>
        <dt>#WARPS:;</dt>
        <dd>[beat number]=[beats to warp past],etc.<br/>
        ( [位置] = [飛ばす拍数] )<br/>
        <br/>
        ワープセグメントを表しており、ノートの指定拍数分スキップすることができます。</dd>
        <dt>#TIMESIGNATURES:0.000000=4=4;</dt>
        <dd>[beat number]=[numerator]=[denominator] etc.<br/>
        ( [位置] = [分子] = [分母] )<br/>
        <br/>
        1小節に4分音符が何個入るかを表します。例えば「5.000=3=4」なら6ビート目から3/4拍子という意味になります。<br/>
        この設定を変更するとEDITの小節線の区切りも変更されます。<br/>
        このタグを使用して、Burning Heatの3/4拍子、GO! Mahalo Mixの5/4拍子を表すことができます。<br/>
        少なくとも初期位置で拍子を定義する必要があります。</dd>
        <dt>#TICKCOUNTS:0.000000=2;</dt>
        <dd>[beat number]=[ticks per beat (int, quantized)],etc.<br/>
        ( [位置]=[1拍内の判定数 (整数で指定)] )<br/>
        <br/>
        ロングノートがPump It Upのような動作する際に、ホールド中に判定がどれほど頻繁に発生するかを指定します。<br/>
        Theme等の設定でTICKCOUNTSが許可されていない場合、このタグは無視されます。<br/>
        少なくとも初期位置でTICKCOUNTSを定義する必要があります。</dd>
        <dt>#COMBOS:0.000000=1;</dt>
        <dd>[beat number]=[combo gained on hit][=[combo "gained" on miss]],etc.<br/>
        ( [位置] = [ヒット時に得るコンボ数] [= [ミスで得るコンボ数]] )<br/>
        <br/>
        1Note当たりのコンボの増分を表します。<br/>
        このタグで、Pump It Upのように特定のノートのコンボ数を変更できます。<br/>
        ミス部分の定義はオプションで記述がない場合はヒットと同じ値になります。<br/>
        少なくとも初期位置でコンボを定義する必要があります。</dd>
        <dt>#SPEEDS:0.000000=1.000000=0.000000=0;</dt>
        <dd>[beat number]=[field multiplier][=[duration until fully activated][=[(0 if beats, 1 if seconds)]]],etc.<br/>
        ( [位置] = [譜面のスクロール速度(割合)] [= [最終値になるまでの時間] [= [拍数で指定する場合は0、秒の場合は1]]] )<br/>
        <br/>
        速度セグメントは、スクロール速度を動的に変化させることができます。(おそらく線形補完)<br/>
        Pump譜面のようなNoteが伸びるギミックはこちらを使用します。<br/>
        <img src="../../../images/memo/piu_gimic.gif"/><br/>
        durationとbooleanフラグはオプションで、デフォルトは共に0です。<br/>
        少なくとも初期位置でSPEEDSを定義する必要があります。
        </dd>
        <dt>#SCROLLS:0.000000=1.000000;</dt>
        <dd>[beat number]=[field multiplier],etc.<br/>
        ( [位置] = [譜面のスクロール単位] )<br/>
        <br/>
        譜面をスクロールさせる単位(1拍分の譜面間隔の割合)を指定します。<br/>
        少なくとも初期位置でSCROLLSを定義する必要があります。</dd>
        <dt>#FAKES:;</dt>
        <dd>[beat number]=[beats to not judge],etc.<br/>
        ( [位置] = [判定を行わない拍数] )<br/>
        <br/>
        FAKESは、どのような方法でも判定やスコアを行わせないようにします。<br/>
        ワープセグメントとは異なり、FAKESは正常にスクロールします。<br/>
        </dd>
        <dt>#LABELS:0.000000=Song Start;</dt>
        <dd>[beat number]=[String of text],etc.<br/>
        ( [位置] = [文字列] )<br/>
        用途不明。<br/>
        少なくとも初期位置でLABELSを定義する必要があります。</dd>
        <dt>#ATTACKS:;</dt>
        <dd>[TIME=(値)]:[LEN=(値)]:[MODS=(オプション名),...]:(繰り返し);<br/>
        おじゃまオプションを指定した時間に追加できます。<br/>
        StepMania5の譜面編集画面では範囲選択してエリアメニュー(Aキー)で「選択範囲をAttackに」を選択して追加できます。<br/>
        (例)<br/>
        #ATTACKS:<br/>
        TIME=-2.000000:LEN=14.127094:MODS=1x ,10% Stealth, Dark, Overhead:<br/>
        TIME=12.127094:LEN=0.335196:MODS=Dizzy, Confusion,*8 25% Flip,Stealth,*32 Dark, Overhead:<br/>
        ... ;<br/>
        (参考元:DDR Mega-IntegratE Mix 2nd Stageの「Gravity -CHiNTOMO remix-」)</dd>
        <dt>#NOTES:</dt>
        <dd>smファイルと同様<br/>
        0000<br/>
        0000<br/>
        0000<br/>
        0000<br/>
        ,<br/>
        ...<br/>
        </dd>
    </dl>
    </p>
  </React.Fragment>
)

export default FileFormats;

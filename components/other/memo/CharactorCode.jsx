import React from 'react';
import HeadHelmet from '../../common/HeadHelmet';

const CharactorCode = () =>(
  <React.Fragment>
    <HeadHelmet text="Others/CharactorCode" />
    <div id="main" className="column">
      <h2>文字コード表</h2>
      <h3>ASCII</h3>
      <p>
        文字集合の符号化のための７ビット符号の構造を規定したもの。<br/>
      </p>
      <h3>ANSI</h3>
      <p>
        メモ帳で文字コードをANSIに選んだ場合に日本のWindowsでは次のShift_JIS(CP932)で保存される。<br/>
      </p>
      <h3>Shift_JIS(Shift-JIS)</h3>
      <p>
        日本語用のJISコードを全角文字の部分からずらして作った文字コード。<br/>
        半角の１バイトコードと全角の２バイトコードからなる。<br/>
        Windowsにおける日本語標準コードとしてMicrosoft等のベンダーが独自に拡張したCP932、<br/>
        MS932、Windows-31JはShift_JISからの派生であり、Shift_JISの未定義領域を独自拡張した結果、<br/>
        データ交換の際に文字化けを引き起こす環境依存文字と呼ばれる文字が存在する。<br/>
        <a href="http://na24ddr.web.fc2.com/other/memo/shift_jis_1byte_code.html" target="_blank" onclick="window.open('shift_jis_1byte_code.html', '_blank', 'width=484,height=484','menubar=no,toolbar=no');return false;">１バイトコード(半角文字)</a>
        <br/>
        <a href="http://na24ddr.web.fc2.com/other/memo/shift_jis_2byte_code.html" target="_blank" onclick="window.open('shift_jis_2byte_code.html', '_blank', 'width=444,height=444','menubar=no,toolbar=no');return false;">２バイトコード(全角文字)</a>
        <br/>
      </p>
      <h3>UTF-8</h3>
      <p>
        UCS-2やUCS-4(Unicode)で定義される文字集合を用いて記述された文字列を<br/>
        バイト列(数値の列)に変換する方式の一つ。<br/>
        StepManiaで一般的に使用されている文字コード。日本語の表示、文字化け対策になる。<br/>
        <img src="../../../images/memo/bom.png" /><br/>
        UTF-8を示すBOM付きの場合、先頭に３バイトのバイナリデータ(0xEF 0xBB 0xBF)が付加される。<br/>
        UTF-8では1文字を1〜6バイトの可変長の数値(バイト列)に変換するようになっているが、<br/>
        現在定義されているUnicode文字をUTF-8で表現した場合、最長で4バイトのバイト列に変換される。<br/>
        また、UTF-8にはutf-8(NFC)とutf-8-mac(NFD)があり、macでは濁点を2文字として表現する。例「タ」「゛」<br/>
      </p>
      <h3>サロゲートペア文字列</h3>
      <p>
        Unicode番号が16進数で10000以上の文字を、UTF-8(およびUTF-16)では表現できないため、<br/>
        Unicode番号D800〜DBFF(Unicode上位サロゲート・コード単位)と<br/>
        DC00〜DFFF(Unicode下位サロゲート・コード単位)の組み合わせで表現した仕組み。(例：&#x1f914;)<br/>
      </p>
      <h3>マルチバイト文字とワイド文字</h3>
      <p>
        C言語の規格においてすべての文字を等しいサイズのデータで表したワイド文字(列)に対して、<br/>
        1文字あたり1バイト以上の可変長のバイト列として表したものをマルチバイト文字(列)と呼ぶ。<br/>
      </p>
      <h3>wstring,string,wchar,char間の変換(C++)</h3>
      <p>
        string,wstringからchar,wcharに変換<br/>
        stringクラスのメンバ関数c_str()を使うとchar型の文字列が取得できます。<br/>
        string.c_str()<br/>
        <br/>
        char,wcharからstring,wstringに変換<br/>
        stringクラスにchar型を代入できます。<br/>
        vectorコンテナに追加する時はemplace_back<br/>
        <br/>
        マルチバイト文字列(char[SIZE])からワイド文字列(wchar[SIZE])に変換<br/>
        mbstowcs(wchar_t *wcstr,const char *mbstr, size_t count);<br/>
        セキュリティ強化版<br/>
        mbstowcs_s(...)<br/>
        <br/>
        ワイド文字列(wchar[SIZE])からマルチバイト文字列(char[SIZE])に変換<br/>
        wcsrtombs(char *mbstr, const wchar_t **wcstr, sizeof count, mbstate_t *mbstate);<br/>
        セキュリティ強化版<br/>
        wcsrtombs_s(...)<br/>
      </p>
    </div>
  </React.Fragment>
)

export default CharactorCode;

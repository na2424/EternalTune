import React from 'react';
import {
    BrowserRouter as Router,
    Route,
    Link,
    Switch,
    Redirect,
    NavLink
} from 'react-router-dom'
import { Helmet } from 'react-helmet';

import FA from 'react-fontawesome';

import { connect } from "react-redux";
import Store from '../../../responsive/store';

import NavDrawer from '../../common/NavDrawer';
import { scrollToTop } from '../../common/Helpers';

const SideMemo = React.memo(() =>
(
  <React.Fragment>
    <Helmet>
      <title>{"Eternal∞Tune(StepMania用DWI配布サイト)/Others/Memo"}</title>
    </Helmet>
    <SideMemoMenu />
  </React.Fragment>
))

const browserSelector = ({browser}) =>
{
  return {browser}
}

@connect(browserSelector)
class SideMemoMenu extends React.PureComponent
{
  render()
  {
    const state = Store.getState();
    let isLessThanMedium = state.browser.lessThan.large;
    let content = <SideMenu/>;

    let sideDom = isLessThanMedium ?
      <NavDrawer content={content} /> :
      <div id="side">
        <div className="column">
          <SideMenu />
        </div>
      </div>;

    return( sideDom );
  }
}

const SideMenu = React.memo(() =>(
  <React.Fragment>
    <h3><FA name="fas fa-edit"/> MEMO MENU</h3>
    <ul id="sidemenu">
      <li onClick={scrollToTop}><NavLink to="/others/memo/command" title="Command">{"Stepmaniaのコマンド紹介"}</NavLink></li>
      <li onClick={scrollToTop}><NavLink to="/others/memo/file_formats" title="File">{"DWI・SM・SSCファイルの書式"}</NavLink></li>
      <li onClick={scrollToTop}><NavLink to="/others/memo/charactor_code" title="Code">{"文字コード表"}</NavLink></li>
      <li onClick={scrollToTop}><NavLink to="/others/memo/group_folder" title="FolderName">{"グループフォルダ名を日本語にする"}</NavLink></li>
      <li onClick={scrollToTop}><NavLink to="/others/memo/calcurate" title="Calculate">{"簡易計算ツール"}</NavLink></li>
    </ul>
  </React.Fragment>
))

export default SideMemo;

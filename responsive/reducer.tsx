import {combineReducers} from 'redux'
import {createResponsiveStateReducer} from 'redux-responsive'

export default combineReducers({
  browser: createResponsiveStateReducer({
    extraSmall: 500,
    small: 700,
    medium: 900,
    large: 1280,
    extraLarge: 1400
  })
})

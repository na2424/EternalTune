import {createStore} from 'redux'
import {responsiveStoreEnhancer} from 'redux-responsive'
import reducer from '../responsive/reducer'

const store = createStore(reducer, responsiveStoreEnhancer);

export default store;

# Eternal∞Tune WebSite
React + ReactRourer + ES6 syntax applications. This website includes the using tools and frameworks:

* [React](https://facebook.github.io/react/)
* [ReactRouter](https://reacttraining.com/react-router/)
* [Redux](http://redux.js.org/) *TODO
* [Bulma](https://bulma.io/)
* [webpack](https://webpack.github.io/)
* [Babel](https://babeljs.io/)
* [ESLint](http://eslint.org/)
* [Sass](https://sass-lang.com/)
* [Gulp](https://gulpjs.com/)
* [Yarn](https://yarnpkg.com/ja/)
* [TypeScript](https://www.typescriptlang.org/)

## Requirement

* [NodeJS](https://nodejs.org/ja/)
* [Ruby](https://rubyinstaller.org/)
* [Compass](http://compass-style.org/)

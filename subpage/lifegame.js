﻿//===================
// ライフゲーム
//===================

/*グローバル変数*/
//キャンバスの幅
var SCREEN_SIZE = 500;
//一辺のセル数
var SIDE_CELLS = 168;
//セルの幅
var CELL_SIZE = SCREEN_SIZE/SIDE_CELLS;
//FPS
var FPS = 10;
//キャンバス
var canvas;
//コンテキスト
var context;
//ポーズフラグ
var isPause=false;
//フィールド情報
var field = new Array(SIDE_CELLS*SIDE_CELLS);
//フィールド情報の一時記憶
var tempField = new Array(SIDE_CELLS*SIDE_CELLS);

window.onload = function()
{
    Start();
}

function Pause()
{
    if(isPause)
    {
        isPause=false;
        document.handle.pausef.value = '一時停止';
        Update(field,tempField);
    }
    else
    {
        isPause=true;
        document.handle.pausef.value = '　再生　';
    }
}

/*ゲームの初期化処理と開始処理*/
function Start()
{
    //ランダムに生死を格納
    for(var i=0; i<field.length;i++)
    {
        field[i] = Math.floor(Math.random()*2);
    }
    //キャンバス要素を取得
    canvas = document.getElementById('world');
    //キャンバスのサイズを設定
    canvas.width = canvas.height = SCREEN_SIZE;
    //キャンバス引き伸ばし率を取得
    var scaleRate = Math.min(window.innerWidth/SCREEN_SIZE,window.innerHeight/SCREEN_SIZE);
    //キャンバスを引き伸ばし
    canvas.style.width = canvas.style.height = SCREEN_SIZE*scaleRate+'px';
    //コンテキストを取得
    context = canvas.getContext('2d');
    //プロパティの色
    context.fillStyle = 'rgb(255, 20, 147)'
    //ゲームループ開始
    Update(field,tempField);
}

/*世代の更新処理とキャンバスの描画処理*/
function Update(field,tempField)
{
    if(isPause) return;
    //自身のまわりにある「生」の数
    var n=0;
    //複製
    tempField = field.slice();
    for(var i=0;i<tempField.length;i++)
    {
        n=0;
        //自身の周りを調べる
        for(var s=-1; s<2; s++)
        {
            for(var t=-1; t<2; t++)
            {
                //自身はカウントしない
                if(s==0 && t==0) continue;
                var check = i+s*SIDE_CELLS+t;
                //配列からはみ出していないか
                if( 0<=check && check<tempField.length)
                {
                    //左右の壁判定
                    if((i<check && !(check%SIDE_CELLS==0 && i%SIDE_CELLS==SIDE_CELLS-1))||(check<i && !(check%SIDE_CELLS==SIDE_CELLS-1 && i%SIDE_CELLS==0)))
                    {
                        //生ならカウント
                        if(tempField[check]) n++;
                    }
                }
            }
        }
        //自身が「生」でカウントが2か3の時
        if(tempField[i] && (n==2||n==3))
        {
            field[i] = 1;
        }
        //自身が「死」でカウントが3の時
        else if(!tempField[i] && n==3)
        {
            field[i] = 1;
        }
        //その他
        else
        {
            field[i] = 0;
        }
    }
    Draw(field);
    //ミリ秒後に再帰処理をしてループ.
    setTimeout(Update,1000/FPS,field,tempField);
}
/*キャンバスの描画処理*/
function Draw(field)
{
    //描画をクリア
    context.clearRect(0,0,SCREEN_SIZE,SCREEN_SIZE);
    for(var i=0;i<field.length;i++)
    {
        var x = (i%SIDE_CELLS) * CELL_SIZE;
        var y = Math.floor(i/SIDE_CELLS) * CELL_SIZE;
        if(field[i]) context.fillRect(x,y,CELL_SIZE,CELL_SIZE);
    }
}
/*フィールド情報をクリアにする*/
function ClearArray()
{
  for (var i=0; i<SIDE_CELLS*SIDE_CELLS; i++)
  {
      field[i]=0;
  }
}
/*グライダーガンを生成*/
function GliderGun() {
    ClearArray();
    var N=SIDE_CELLS;
    field[3 * N + 27] = 1;
    field[4 * N + 25] = 1; field[4 * N + 27] = 1;
    field[5 * N + 15] = 1; field[5 * N + 16] = 1; field[5 * N + 23] = 1;
    field[5 * N + 24] = 1; field[5 * N + 37] = 1; field[5 * N + 38] = 1;
    field[6 * N + 14] = 1; field[6 * N + 18] = 1; field[6 * N + 23] = 1;
    field[6 * N + 24] = 1; field[6 * N + 37] = 1; field[6 * N + 38] = 1;
    field[7 * N + 3] = 1;  field[7 * N + 4] = 1;  field[7 * N + 13] = 1;
    field[7 * N + 19] = 1; field[7 * N + 23] = 1; field[7 * N + 24] = 1;
    field[8 * N + 3] = 1;  field[8 * N + 4] = 1;   field[8 * N + 13] = 1;
    field[8 * N + 17] = 1; field[8 * N + 19] = 1; field[8 * N + 20] = 1;
    field[8 * N + 25] = 1; field[8 * N + 27] = 1;
    field[9 * N + 13] = 1; field[9 * N + 19] = 1; field[9 * N + 27] = 1;
    field[10 * N + 14] = 1;field[10 * N + 18] = 1;
    field[11 * N + 15] = 1; field[11 * N + 16] = 1;
}
/*十文字を生成*/
function LineN() {
    ClearArray();
    var N=SIDE_CELLS;
    for(var i=13;i<SIDE_CELLS-14;i++)
    {
        field[(N/2-1) * N + i] =1;
        field[N * i + N/2-1] =1;
    }
}

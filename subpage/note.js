﻿//======================================
// 譜面画像生成
// (Release:2015/04/17)
// (Update:2016/12/17)
// テキストを入力→譜面表示→画像保存
//======================================
/// <reference path="jquery/index.d.ts" />
/// <reference path="file-saver/index.d.ts" />
///TODO:jsのジェネリックシングルトン
//常に単一のインスタンスしか存在しないことを保証する
// class Singleton<T>
// {
//     private static _instance = null;
//     constructor()
//     {
//         if (Singleton._instance)
//             throw new Error("must use the getInstance.(getInstanceからアクセス可)");
//         Singleton._instance = this;
//     }
//     public static getInstance()
//     {
//         if (Singleton._instance === null)
//             Singleton._instance = new Singleton();
//         return Singleton._instance;
//     }
// }
//文字列操作クラス(シングルトン)
var StringController = (function () {
    function StringController() {
        if (StringController._instance)
            throw new Error("must use the getInstance.(getInstanceからアクセス可)");
        StringController._instance = this;
    }
    StringController.getInstance = function () {
        if (StringController._instance === null)
            StringController._instance = new StringController();
        return StringController._instance;
    };
    //文字列A中に含まれる文字列Bの数を返す
    StringController.prototype.CountStr = function (In_str /*A*/, Find_str /*B*/) {
        var rtn = 0;
        var icount = In_str.indexOf(Find_str, 0);
        while (icount >= 0) {
            icount = In_str.indexOf(Find_str, ++icount);
            rtn++;
        }
        return rtn;
    };
    //数字であるか判定
    StringController.prototype.IsNumber = function (c) {
        if (typeof (c) != 'number' && typeof (c) != 'string')
            return false;
        if (c == '\n')
            return false;
        if (c == '')
            return false;
        if (c == ' ')
            return false;
        if (c == '　')
            return false;
        // if(isNaN(c))
        //     return false;
        return true;
    };
    //コメントを取り除く
    StringController.prototype.RemoveComment = function (str) {
        var buf = [], pos = 0, s = str = String(str);
        R: while (s) {
            var c = void 0, nc = void 0, n = void 0, q = false, sq = false, re = false, rec = false, esc = false;
            for (var i = 0, l = s.length; i < l; i++) {
                c = s[i];
                if (esc) {
                    esc = false;
                }
                else if (q) {
                    if (c == '"')
                        q = false;
                    else if (c == '\\')
                        esc = true;
                }
                else if (sq) {
                    if (c == "'")
                        sq = false;
                    else if (c == '\\')
                        esc = true;
                }
                else if (re) {
                    if (rec) {
                        if (c == ']')
                            rec = false;
                        else if (c == '\\')
                            esc = true;
                    }
                    else {
                        if (c == "/")
                            re = false;
                        else if (c == '\\')
                            esc = true;
                        else if (c == '[')
                            rec = true;
                    }
                }
                else {
                    if (c == '"') {
                        q = true;
                    }
                    else if (c == "'") {
                        sq = true;
                    }
                    else if (c == '/') {
                        nc = s[i + 1];
                        if (nc == '/') {
                            var l_1 = s.length;
                            buf.push(s.slice(0, i));
                            s = s.slice(i + 2);
                            s = s.replace(/[\s\S]*?(\r?\n)/, '$1');
                            pos += (l_1 - s.length);
                            continue R;
                        }
                        else if (nc == '*') {
                            var l_2 = s.length;
                            buf.push(s.slice(0, i));
                            n = s.indexOf('*/', i + 2);
                            s = s.slice(n + 2);
                            pos += (l_2 - s.length);
                            continue R;
                        }
                        else {
                            var cur = pos + i + 1;
                            var regcheck = str.slice(0, cur) + '^' + str.slice(cur);
                            try {
                                Function('(' + regcheck + ')');
                                re = true;
                            }
                            catch (e) { }
                        }
                    }
                }
            }
            buf.push(s);
            break;
        }
        return buf.join('');
    };
    //文字列の最大列数を取得
    StringController.prototype.GetMaxRowText = function (text) {
        var str = "";
        var maxNum = 0;
        var num = 0;
        for (var i = 0; i < text.length; i++) {
            var c = text.charAt(i);
            if (c === '\n') {
                str = "";
                if (num > maxNum)
                    maxNum = num;
                num = 0;
            }
            else {
                str += "" + c;
                num++;
            }
        }
        return maxNum;
    };
    //ゼロパディング
    StringController.prototype.ComPadZero = function (value, length) {
        return new Array(length - ('' + value).length + 1).join('0') + value;
    };
    StringController._instance = null;
    return StringController;
}());

//日付関連クラス
var DateTime = (function () {
    function DateTime() {
        if (DateTime._instance)
            throw new Error("must use the getInstance.(getInstanceからアクセス可)");
        DateTime._instance = this;
    }
    DateTime.getInstance = function () {
        if (DateTime._instance === null)
            DateTime._instance = new DateTime();
        return DateTime._instance;
    };
    //現在時刻を文字列で取得
    DateTime.prototype.GetNowTimeString = function () {
        var now = new Date();
        var format = 'yyyy/MM/dd HH:mm:ss';
        return this.ComDateFormat(now, format);
    };
    //書式を指定して日付オブジェクトから文字列に変換
    DateTime.prototype.ComDateFormat = function (date, format) {
        var result = format;
        var f;
        var rep;
        var yobi = new Array('日', '月', '火', '水', '木', '金', '土');
        f = 'yyyy';
        if (result.indexOf(f) > -1) {
            rep = date.getFullYear();
            result = result.replace(/yyyy/, rep);
        }
        f = 'MM';
        if (result.indexOf(f) > -1) {
            rep = StringController.getInstance().ComPadZero(date.getMonth() + 1, 2);
            result = result.replace(/MM/, rep);
        }
        f = 'ddd';
        if (result.indexOf(f) > -1) {
            rep = yobi[date.getDay()];
            result = result.replace(/ddd/, rep);
        }
        f = 'dd';
        if (result.indexOf(f) > -1) {
            rep = StringController.getInstance().ComPadZero(date.getDate(), 2);
            result = result.replace(/dd/, rep);
        }
        f = 'HH';
        if (result.indexOf(f) > -1) {
            rep = StringController.getInstance().ComPadZero(date.getHours(), 2);
            result = result.replace(/HH/, rep);
        }
        f = 'mm';
        if (result.indexOf(f) > -1) {
            rep = StringController.getInstance().ComPadZero(date.getMinutes(), 2);
            result = result.replace(/mm/, rep);
        }
        f = 'ss';
        if (result.indexOf(f) > -1) {
            rep = StringController.getInstance().ComPadZero(date.getSeconds(), 2);
            result = result.replace(/ss/, rep);
        }
        f = 'fff';
        if (result.indexOf(f) > -1) {
            rep = StringController.getInstance().ComPadZero(date.getMilliseconds(), 3);
            result = result.replace(/fff/, rep);
        }
        return result;
    };
    //シングルトン
    DateTime._instance = null;
    return DateTime;
}());

//DOM管理クラス(シングルトン)
var DOMManager = (function () {
    function DOMManager() {
        if (DOMManager._instance)
            throw new Error("must use the getInstance.(getInstanceからアクセス可)");
        DOMManager._instance = this;
    }
    DOMManager.getInstance = function () {
        if (DOMManager._instance === null)
            DOMManager._instance = new DOMManager();
        return DOMManager._instance;
    };
    DOMManager.prototype.Initialize = function () {
        this.canvas = document.getElementById("world");
        this.context = this.canvas.getContext('2d');
        this.textarea = document.getElementById("textarea");
        this.message = document.getElementById("message");
    };
    //----------------------
    // Form操作
    //----------------------
    //テキストエリアの文字列を取得
    DOMManager.prototype.GetText = function () {
        return this.textarea.value;
    };
    DOMManager.prototype.WriteMessage = function (messageText) {
        this.message.innerHTML = messageText;
    };
    //ノートテキスト編集
    DOMManager.prototype.EditNote = function (num, isSingle) {
        var text = this.textarea.value;
        //this.barCount = strS.CountStr(text, ',') + strS.CountStr(text, ';');
        var lines = text.split('\n');
        var maxRow = lines.length;
        this.textarea.value = '';
        for (var i = 0; i < maxRow - 1; ++i) {
            this.textarea.value += lines[i] + '\n';
        }
        var setStr = '';
        var nowLineText = lines[maxRow - 1];
        var maxNum = isSingle ? 4 : 8;
        for (var i = 0; i < maxNum; i++) {
            var c = nowLineText.charAt(num);
            if (i == num) {
                if (c == '0' || c == '')
                    setStr += '1';
                else
                    setStr += '0';
            }
            else {
                if (c == '')
                    setStr += 0;
                else
                    setStr += nowLineText.charAt(i);
            }
        }
        this.textarea.value += setStr;
    };
    //行テキスト追加
    DOMManager.prototype.AddNextLine = function (isSingle) {
        var text = this.textarea.value;
        var lines = text.split('\n');
        var maxRow = lines.length;
        var str = '';
        var lastLineStr = lines[maxRow - 1];
        if (lastLineStr != '') {
            str = '\n';
        }
        this.textarea.value += str + (isSingle ? '0000' : '00000000');
    };
    //小節テキスト追加
    DOMManager.prototype.AddNextBar = function (isSingle) {
        var text = this.textarea.value;
        var lines = text.split('\n');
        var maxRow = lines.length;
        var str = '';
        if (lines[maxRow - 1] != '') {
            str = "\n,\n";
        }
        //書き込み
        this.textarea.value += str + (isSingle ? '0000' : '00000000');
    };
    //---------------------
    // Canvas操作
    //---------------------
    //キャンバス状態の再設定
    DOMManager.prototype.ResetCanvas = function (maxRow, barCount, speed) {
        this.canvas.width = maxRow * ONE_BEAT_PIXEL;
        this.canvas.height = Math.floor((barCount * MEASURE * speed + 1) * ONE_BEAT_PIXEL);
        this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
    };
    //キャンバスに描画
    DOMManager.prototype.DrawCanvas = function (image, offsetX, offsetY, width, height) {
        this.context.drawImage(image, offsetX, offsetY, width, height);
    };
    DOMManager.prototype.DrawRectCanvas = function (image, sx, sy, sw, sh, dx, dy, dw, dh) {
        this.context.drawImage(image, sx, sy, sw, sh, dx, dy, dw, dh);
    };
    //背景と拍線を描画
    DOMManager.prototype.DrawBackGround = function (barCount, speed) {
        this.context.fillStyle = "rgb(255, 255, 255)";
        //context.fillRect(0,(ONE_BEAT_PIXEL/2),canvas.width,canvas.height-(ONE_BEAT_PIXEL/2));
        this.context.fillRect(0, 0, this.canvas.width, this.canvas.height);
        this.context.fillStyle = "rgb(188, 188, 188)";
        for (var i = 0; i < barCount * 2; i++) {
            this.context.fillRect(0, Math.floor((i * 2 + 1) * ONE_BEAT_PIXEL * speed + (ONE_BEAT_PIXEL / 2)), this.canvas.width, Math.floor(ONE_BEAT_PIXEL * speed));
        }
        this.context.fillStyle = "rgb(68, 68, 68)";
        //ボーダーを描画
        for (var i = 0; i < barCount * 4; i++) {
            if (i % 4 == 0)
                this.context.fillRect(0, Math.floor((i) * ONE_BEAT_PIXEL * speed + (ONE_BEAT_PIXEL / 2)) - 1, this.canvas.width, 2);
            else
                this.context.fillRect(0, Math.floor((i) * ONE_BEAT_PIXEL * speed + (ONE_BEAT_PIXEL / 2)), this.canvas.width, 1);
        }
    };
    //画像保存処理
    DOMManager.prototype.CanvasSaveImage = function () {
        this.canvas.toBlob(function (blob) {
            saveAs(blob, "Notes-" + DateTime.getInstance().GetNowTimeString() + ".png");
        });
    };
    DOMManager._instance = null;
    return DOMManager;
}());
//Note管理クラス
var Notes = (function () {
    function Notes() {
        this.images = [];
        this.barCount = 0;
        this.noteElement = document.getElementById("note");
        this.selectSpeed = document.getElementById("speed");
        this.SelectSpeed = (1.0).toString();
    }
    Object.defineProperty(Notes.prototype, "SelectSpeed", {
        get: function () {
            return this.selectSpeed.value;
        },
        set: function (speed) {
            this.selectSpeed.value = speed;
        },
        enumerable: true,
        configurable: true
    });
    //画像読み込み
    Notes.prototype.LoadImage = function () {
        //画像パスを設定.
        this.srcs = [
            //4分
            [
                'img/itgnote00.png',
                'img/itgnote01.png',
                'img/itgnote02.png',
                'img/itgnote03.png',
            ],
            //8分
            [
                'img/itgnote10.png',
                'img/itgnote11.png',
                'img/itgnote12.png',
                'img/itgnote13.png',
            ],
            //12分
            [
                'img/itgnote20.png',
                'img/itgnote21.png',
                'img/itgnote22.png',
                'img/itgnote23.png',
            ],
            //16分
            [
                'img/itgnote30.png',
                'img/itgnote31.png',
                'img/itgnote32.png',
                'img/itgnote33.png',
            ],
            //その他
            [
                'img/itgnote40.png',
                'img/itgnote41.png',
                'img/itgnote42.png',
                'img/itgnote43.png',
            ],
            //Mine
            [
                'img/mine.png',
            ],
            //Hold
            [
                'img/HoldBody.png',
                'img/HoldBottom.png',
            ],
            //Roll
            [
                'img/RollBody.png',
                'img/RollBottom.png',
            ]
        ];
        //画像読み込み.
        for (var i = 0; i < this.srcs.length; i++) {
            this.images[i] = [];
            for (var j = 0; j < this.srcs[i].length; j++) {
                this.images[i][j] = new Image();
                this.images[i][j].src = this.srcs[i][j];
            }
        }
    };
    //speedを取得
    Notes.prototype.GetSpeedFromSelect = function () {
        var speedValue = $('[name=speed] option:selected').val();
        console.log("speed = " + speedValue);
        return speedValue;
    };
    //小節があるか確認
    Notes.prototype.HasBar = function () {
        if (this.barCount == 0)
            DOMManager.getInstance().WriteMessage('<p>読み込みエラー:\n小節区切り文字(","または";")がありません。</p>');
        return this.barCount != 0;
    };
    //------------------
    // 描画関連
    //------------------
    //通常Noteを描画
    //(描画の縦位置は ((小節数*小節拍数*1拍分のピクセル)+(分割したn個目/拍分割数)*小節拍数*1拍分のピクセル)*倍速
    Notes.prototype.DrawOneNote = function (note, row, pos, speed) {
        DOMManager.getInstance().DrawCanvas(this.images[note][row % 4], /*描画するノート画像*/ row * ONE_BEAT_PIXEL, /*描画x座標*/ pos * ONE_BEAT_PIXEL * speed, /*描画y座標*/ ONE_BEAT_PIXEL, /*描画x範囲*/ ONE_BEAT_PIXEL); /*描画y範囲*/
    };
    //地雷Noteを描画
    Notes.prototype.DrawMineNote = function (row, pos, speed) {
        DOMManager.getInstance().DrawCanvas(this.images[NoteColorImage.MineNote][0], row * ONE_BEAT_PIXEL, /*描画x座標*/ pos * ONE_BEAT_PIXEL * speed, /*描画y座標*/ ONE_BEAT_PIXEL, /*描画x範囲*/ ONE_BEAT_PIXEL /*描画y範囲*/);
    };
    //Long(Hold/Roll)Noteを描画 /*A<B*/
    Notes.prototype.DrawLongBody = function (note, row, notePosA, notePosB, speed) {
        //画像の高さ
        var IMAGE_HEIGHT = 256;
        //FAの全体の長さを保持 (FAの全体の長さ=分割FAの長さ*商+余り)
        var bodyLength = Math.abs((notePosB - notePosA) * ONE_BEAT_PIXEL * speed);
        //商
        var bodyCount = Math.floor(bodyLength / (ONE_BEAT_PIXEL * 4));
        //余り
        var remainLength = bodyLength % (ONE_BEAT_PIXEL * 4);
        //描画
        for (var f = bodyCount; f > 0; --f) {
            DOMManager.getInstance().DrawCanvas(this.images[note][0], row * ONE_BEAT_PIXEL, ((notePosB * ONE_BEAT_PIXEL) * speed - (f * ONE_BEAT_PIXEL * 4)) + ONE_BEAT_PIXEL / 2, ONE_BEAT_PIXEL, (ONE_BEAT_PIXEL * 4));
        }
        DOMManager.getInstance().DrawRectCanvas(this.images[note][0], 0, IMAGE_HEIGHT - remainLength * 2, 64, remainLength * 2, row * ONE_BEAT_PIXEL, (notePosA * ONE_BEAT_PIXEL * speed + ONE_BEAT_PIXEL / 2), ONE_BEAT_PIXEL, remainLength);
    };
    //ロングノート終端を描画
    Notes.prototype.DrawLongBottom = function (note, row, pos, speed) {
        DOMManager.getInstance().DrawCanvas(this.images[note][1], row * ONE_BEAT_PIXEL, (pos * ONE_BEAT_PIXEL * speed + ONE_BEAT_PIXEL / 2), ONE_BEAT_PIXEL, ONE_BEAT_PIXEL / 2);
    };
    return Notes;
}());
var Modes = (function () {
    function Modes() {
        this.isSingle = true;
    }
    Object.defineProperty(Modes.prototype, "IsSingle", {
        get: function () {
            return this.isSingle;
        },
        enumerable: true,
        configurable: true
    });
    Modes.prototype.ChangePlayMode = function () {
        var mode = $('#setnoteform [name=mode]:checked').val();
        console.log("mode = " + mode);
        if (mode == 'Double') {
            $('#disp').css('display', 'inline');
            this.isSingle = false;
        }
        else {
            $('#disp').css('display', 'none');
            this.isSingle = true;
        }
    };
    return Modes;
}());
//===============================================================================================
var notes = new Notes();
var modes = new Modes();
//シングルトン参照
var strS = StringController.getInstance();
var NoteColorImage;
(function (NoteColorImage) {
    NoteColorImage[NoteColorImage["Note4th"] = 0] = "Note4th";
    NoteColorImage[NoteColorImage["Note8th"] = 1] = "Note8th";
    NoteColorImage[NoteColorImage["Note12th"] = 2] = "Note12th";
    NoteColorImage[NoteColorImage["Note16th"] = 3] = "Note16th";
    NoteColorImage[NoteColorImage["NoteOthers"] = 4] = "NoteOthers";
    NoteColorImage[NoteColorImage["MineNote"] = 5] = "MineNote";
    NoteColorImage[NoteColorImage["HoldNote"] = 6] = "HoldNote";
    NoteColorImage[NoteColorImage["RollNote"] = 7] = "RollNote";
})(NoteColorImage || (NoteColorImage = {}));
var ONE_BEAT_PIXEL = 32;
var MEASURE = 4.0;
//------------------------
// イベントコールバック
//------------------------
//DOMロード後に呼ばれる処理
$(window).load(function () {
    DOMManager.getInstance().Initialize();
    //画像読み込み
    notes.LoadImage();
});
//プレイスタイル変更
function OnChangeModeButton() {
    modes.ChangePlayMode();
}
//現在カーソル(最終行)のノート編集
function OnClickAddNoteButton(num) {
    DOMManager.getInstance().EditNote(num, modes.IsSingle);
}
//次の行
function OnClickNextLineButton() {
    DOMManager.getInstance().AddNextLine(modes.IsSingle);
}
//次の小節
function OnClickNextBarButton() {
    DOMManager.getInstance().AddNextBar(modes.IsSingle);
}
//ボタンクリック後にテキストエリアの値から譜面をCanvasに描画する
function OnClickDisplayButton() {
    var speed = notes.GetSpeedFromSelect();
    DOMManager.getInstance().WriteMessage('');
    var n = 0;
    var p = 0;
    var row = 0;
    var bar = -1;
    var text = DOMManager.getInstance().GetText();
    text = strS.RemoveComment(text);
    var maxRow = strS.GetMaxRowText(text);
    notes.barCount = strS.CountStr(text, ',') + strS.CountStr(text, ';');
    DOMManager.getInstance().ResetCanvas(maxRow, notes.barCount, speed);
    //小節がなければエラーメッセージを表示
    if (!notes.HasBar())
        return;
    var through = true;
    var isOneNum = false;
    var holdFlag = false;
    var note = [];
    var notePos = [];
    for (var i = 0; i < maxRow; i++) {
        note[i] = [];
        notePos[i] = [];
    }
    //小節単位で文字列を確保
    var str = '';
    //1文字ずつ取り出す
    for (var i_1 = 0; i_1 < text.length; i_1++) {
        var c = text.charAt(i_1);
        //Note定義文字が来たところから読み込み再スタート
        if (isOneNum || StringController.getInstance().IsNumber(c) || c == 'M')
            isOneNum = true;
        else
            continue;
        //１小節の文字列を保持
        if (c === ',' || c === ';') {
            ++bar;
            through = false;
        }
        else {
            if (c === '\n')
                ++p;
            if (StringController.getInstance().IsNumber(c) || c === '\n' || c === 'M')
                str += c;
        }
        //---小節文字列ができていれば通す---
        if (through)
            continue;
        //----------------------------
        through = true;
        isOneNum = false;
        n = 0;
        row = 0;
        //Noteの種類とポジションを保持
        for (var j = 0; j < str.length; j++) {
            var c_1 = str.charAt(j);
            switch (c_1) {
                case '1':
                case '2':
                case '3':
                case '4':
                case 'M':
                    note[row].push(c_1);
                    notePos[row].push(bar * MEASURE + (n / p) * MEASURE);
                    break;
                case '\n':
                    row = -1;
                    ++n;
                    break;
            }
            ++row;
        }
        p = 0;
        str = '';
    }
    /*---ここから描画処理---*/
    //背景
    DOMManager.getInstance().DrawBackGround(notes.barCount, speed);
    //Noteを描画
    for (var row_1 = 0; row_1 < maxRow; row_1++) {
        for (var i_2 = note[row_1].length - 1; i_2 >= 0; --i_2) {
            switch (note[row_1][i_2]) {
                case '1':
                case '2':
                case '4':
                    var position = notePos[row_1][i_2];
                    //4分
                    if (position * MEASURE % MEASURE == 0)
                        notes.DrawOneNote(NoteColorImage.Note4th, row_1, position, speed);
                    else if (position * MEASURE * 2 % MEASURE * 2 == 0)
                        notes.DrawOneNote(NoteColorImage.Note8th, row_1, position, speed);
                    else if (position * MEASURE * 3 % MEASURE * 3 == 0)
                        notes.DrawOneNote(NoteColorImage.Note12th, row_1, position, speed);
                    else if (position * MEASURE * 4 % MEASURE * 4 == 0)
                        notes.DrawOneNote(NoteColorImage.Note16th, row_1, position, speed);
                    else
                        notes.DrawOneNote(NoteColorImage.NoteOthers, row_1, position, speed);
                    break;
                case '3':
                    holdFlag = true;
                    break;
                case 'M':
                    var pos = notePos[row_1][i_2];
                    notes.DrawMineNote(row_1, pos, speed);
                    break;
            }
            if (holdFlag) {
                holdFlag = false;
                if (i_2 > 0 && note[row_1][i_2] == '3' && (note[row_1][i_2 - 1] == '2' || note[row_1][i_2 - 1] == '4')) {
                    if (note[row_1][i_2 - 1] == '2') {
                        notes.DrawLongBody(NoteColorImage.HoldNote, row_1, notePos[row_1][i_2 - 1], notePos[row_1][i_2], speed);
                        notes.DrawLongBottom(NoteColorImage.HoldNote, row_1, notePos[row_1][i_2], speed);
                    }
                    else {
                        notes.DrawLongBody(NoteColorImage.RollNote, row_1, notePos[row_1][i_2 - 1], notePos[row_1][i_2], speed);
                        notes.DrawLongBottom(NoteColorImage.RollNote, row_1, notePos[row_1][i_2], speed);
                    }
                }
                else {
                    DOMManager.getInstance().WriteMessage('<p>読み込みエラー:\n書式が違っています。\nロングノートの終止点("3")の前に、\n開始点("2"→FA または "4"→ロール)がありません。</p>');
                    return;
                }
            }
        }
    }
}
//画像保存ボタンクリック時
function OnClickSaveButton() {
    if (notes.barCount <= 0) {
        DOMManager.getInstance().WriteMessage('譜面が表示されていません');
        return;
    }
    DOMManager.getInstance().CanvasSaveImage();
}

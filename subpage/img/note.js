﻿//================================
// 譜面画像生成(Beta版)
// テキストを入力→譜面表示→画像保存
//================================
var canvas = document.getElementById("world");
var context = canvas.getContext('2d');
var textarea = document.getElementById("textarea");
var ONE_BEAT_PIXEL = 32;

var NoteColorImage = {
    Note4th: 0,
    Note8th: 1,
    Note12th: 2,
    Note16th: 3,
    NoteOthers: 4,
    MineNote: 5,
    HoldBottom: 6,
}

//Note画像のパス(画像そのものを回転させて描画するAPIが無かったので複数用意した)
var srcs = [
    //4分
    [
        'img/itgnote00.png',
        'img/itgnote01.png',
        'img/itgnote02.png',
        'img/itgnote03.png',
    ],
    //8分
    [
        'img/itgnote10.png',
        'img/itgnote11.png',
        'img/itgnote12.png',
        'img/itgnote13.png',
    ],
    //12分
    [
        'img/itgnote20.png',
        'img/itgnote21.png',
        'img/itgnote22.png',
        'img/itgnote23.png',
    ],
    //16分
    [
        'img/itgnote30.png',
        'img/itgnote31.png',
        'img/itgnote32.png',
        'img/itgnote33.png',
    ],
    //その他
    [
        'img/itgnote40.png',
        'img/itgnote41.png',
        'img/itgnote42.png',
        'img/itgnote43.png',
    ],
    //Mine
    [
        'img/mine.png',
        'img/mine.png',
        'img/mine.png',
        'img/mine.png',
    ],
    //HoldBottom
    [
        'img/HoldBottom.png',
        'img/HoldBottom.png',
        'img/HoldBottom.png',
        'img/HoldBottom.png',
    ],

];
var images = [];

window.onload = function()
{
    //初期Speed
    document.note.speed.value = 1.0;
    //画像読み込み
    for (var i=0; i<srcs.length;i++)
    {
        images[i] = [];
        for(var j=0;j<srcs[i].length;j++)
        {
            images[i][j] = new Image();
            images[i][j].src = srcs[i][j];
        }
    }
};

//ボタンクリック後にテキストエリアの値から譜面をCanvasに描画する
function OnClickDisplayButton()
{
    var speed=1;
    for(var i = 0 ; i < document.note.speed.options.length ; i++)
    {
        if(document.note.speed.options[i].selected)
            speed = document.note.speed.options[i].value;
    }
    //小節単位で文字列を確保
    var str = '';
    
    var n=0;
    var p=0;
    var text = textarea.value;
    var maxRow = GetMaxRowText(text); 
    var barCount = CountStr(text,',');
    canvas.width = maxRow*ONE_BEAT_PIXEL;
    canvas.height = (barCount*4.0 + 1) * ONE_BEAT_PIXEL*speed;
    context.clearRect(0, 0, canvas.width, canvas.height*speed);
    DrawBackGround(barCount,speed);

    var row=0;
    var bar=barCount;
    var through=true;
    var oneNum=false;

    //1文字ずつ取り出す
    //(後ろのノートから描画していく)
    for(var i=text.length-1; i>=0; --i)
    {   
        var c = text.charAt(i);

        //数字が来たところから読み込み再スタート
        if(oneNum || IsNumber(c))
            oneNum = true;
        else
            continue;

        //１小節毎に文字列を保持(strに１小節分の文字列を入れていく)
        if(c === ',')
        {
            --bar;
            through = false;
        }
        else
        {
            if( c==='\n' )
                p++;
            if(IsNumber(c) || c==='\n' || c==='M')
                str += c;
        
            //,が無い文字列の場合1小節内に収める
            if(i==0)
            {
                --bar;
                p++;
                through = false;
                if(p<4) 
                    p=4;
            }
        }

        //-----------------------
        //小節文字列ができていれば通す
        if(through) continue;
        //以下は小節毎に描画
        //-----------------------
    
        through = true;
        oneNum = false;
        n=p-1;
        row=maxRow-1;

        //Noteを描画
        for(var j=0;j<str.length;j++)
        {
            switch(str.charAt(j))
            {
            case '1':
            case '2':
                var pos = (n/p)*4.0;
                    //4分
                    if((pos*p) % p == 0)
                        DrawOneNote(NoteColorImage.Note4th,row,bar,pos,speed);
                    //8分
                    else if((pos*p) % p  == p/2)
                        DrawOneNote(NoteColorImage.Note8th,row,bar,pos,speed);
                    //16分
                    else if((pos*p) % p == p/4 || (pos*p) % p == 3*p/4)
                        DrawOneNote(NoteColorImage.Note16th,row,bar,pos,speed);
                    //12分
                    else if((pos*p) % p == p/3 || (pos*p) % p == 2*p/3)
                        DrawOneNote(NoteColorImage.Note12th,row,bar,pos,speed);
                    //その他
                    else
                        DrawOneNote(NoteColorImage.NoteOthers,row,bar,pos,speed);
                break;
            case '3':
                var pos = (n/p)*4.0;
                DrawOneNote(NoteColorImage.HoldBottom,row,bar,pos,speed);
                break;
            case 'M':
                var pos = (n/p)*4.0;
                DrawOneNote(NoteColorImage.MineNote,row,bar,pos,speed);
                break;    
            case '\n':
                row = maxRow;
                --n;
                break;
            }
            --row;
        }
        p=0;
        str='';
    }
    console.log(text);
}

function DrawBackGround(barCount,speed)
{
    //背景を描画
    context.fillStyle = "rgb(188, 188, 188)";
    for(var i=0;i<barCount*2;i++)
    {
        context.fillRect(0, ((i)*2*ONE_BEAT_PIXEL*speed+(ONE_BEAT_PIXEL/2)), canvas.width, ONE_BEAT_PIXEL*speed);
    }
    context.fillStyle = "rgb(68, 68, 68)";
    //ボーダーを描画
    for(var i=0;i<barCount*4;i++)
    {
        if(i%4 ==0)
            context.fillRect(0, (i)*ONE_BEAT_PIXEL*speed+(ONE_BEAT_PIXEL/2), canvas.width, 2);
        else
            context.fillRect(0, (i)*ONE_BEAT_PIXEL*speed+(ONE_BEAT_PIXEL/2), canvas.width, 1);
    }
}

//Noteを描画
//(描画の縦位置は (小節数*小節拍数*1拍分のピクセル)+((分割したn個目/拍分割数)*小節拍数*1拍分のピクセル)*倍速
function DrawOneNote(note,row,bar,pos,speed)
{
    //描画 
    context.drawImage(
        images[note][row%4], 
        row*ONE_BEAT_PIXEL, 
        ((bar*4.0*ONE_BEAT_PIXEL)+(pos*ONE_BEAT_PIXEL))*speed, 
        ONE_BEAT_PIXEL, 
        ONE_BEAT_PIXEL);
}

//文字列A中に含まれる文字列Bの数を返す
function CountStr(In_str /*A*/, Find_str /*B*/)
{
    var icount;
    rtn = 0;
    icount = In_str.indexOf(Find_str,0)
    while ( icount >= 0 ) {
        icount = In_str.indexOf(Find_str,++icount);
        rtn++;
    }
    return rtn;
}

//数字であるか判定
function IsNumber(c)
{
    if(typeof(c) != 'number' && typeof(c) != 'string')
        return false;
    if(c == '\n')
        return false;
    if(c == '')
        return false;
    if(isNaN(c))
        return false;
    return true;
}

//文字列の最大列数を取得
function GetMaxRowText(text)
{
    var str = "";
    var maxNum = 0;
    var num = 0;
    for(var i=0;i<text.length;i++)
    {
        var c = text.charAt(i);
        if(c === '\n')
        {
            str = "";
            if(num > maxNum)
                maxNum=num;
            num = 0;
        }
        else
        {
            str += "" + c;
            num++;
        }
    }
    return maxNum;
}

// 保存処理
function CanvasSaveImage()
{
    canvas.toBlob(function ( blob ) {
            saveAs( blob, "inazumatv.png" );
        }
    );
}
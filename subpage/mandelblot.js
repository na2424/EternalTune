﻿//========================================
// マンデルブロ集合
// 以下の複素数列が発散しないCの集まり
// Z(0)=0
// Z(k+1)=Z(k)^2+C (k=0,1,2,...)
// (Z,C ComplexNumber)
//========================================
/*グローバル変数*/
//キャンバス
var canvas = document.getElementById("world");
//コンテキスト
var context = canvas.getContext('2d');
//計算開始位置(複素数列の「C」にあたる部分の左上座標)
var mandel_x = -1.5;
var mandel_y = -1.0;
//計算範囲(-1.5<x<0.5,-1.0<y<1.0　[C=x+yi])
var range_x = 2.0;
var range_y = 2.0;
//描画画面サイズ
var SCREEN_SIZE = 500;
//1ピクセル刻み幅
var step_x = range_x/SCREEN_SIZE;
var step_y = range_y/SCREEN_SIZE;
//最大計算回数
var COUNT_MAX = 500;
//色設定
var mandelColor = new Object();


window.onload = function()
{
    //キャンバスサイズ
    canvas.width = canvas.height = SCREEN_SIZE;
    MandelBlotSet();
}

canvas.onmousedown = function(e) {
    //左クリック時
    if (e.button == 0)
    {
        var mouseX = e.clientX - canvas.offsetLeft;
        var mouseY = e.clientY - canvas.offsetTop;
        var x = mouseX / SCREEN_SIZE * range_x + mandel_x;
        var y = mouseY / SCREEN_SIZE * range_y + mandel_y;
        //クリックした座標を中心に２倍ズーム
        ResetRange(x - range_x/4.0, y - range_y/4.0, range_x / 2, range_y / 2);
        //マンデルブロ集合とその周辺を描く
        MandelBlotSet();
    }
};
//描画初期化処理
function ResetRange(x0,y0,x1,y1)
{
    mandel_x = x0;
    mandel_y = y0;
    range_x = x1;
    range_y = y1;
    step_x = range_x/SCREEN_SIZE;
    step_y = range_y/SCREEN_SIZE;
    for(var i=0;i<SCREEN_SIZE;i++)
    {
        for(var j=0;j<SCREEN_SIZE;j++)
        {
            Draw(i,j,COUNT_MAX);
        }
    }
}

//リセットボタン
function OnClickResetButton()
{
    ResetRange(-1.5,-1.0,2.0,2.0);
    MandelBlotSet();
}

//マンデルブロ集合を描く
function MandelBlotSet()
{
    var n;
    var tr,ti,vr,vi,pr,pi;
    var index;
    for(var i=0;i<SCREEN_SIZE;i++)
    {
        for(var j=0;j<SCREEN_SIZE;j++)
        {
            //複素数列の初項( Z(0)=0 )
            vr = 0;
            vi = 0;
            //複素平面上のピクセル位置(実部と虚部)
            pr = mandel_x + (step_x*j);
            pi = mandel_y + (step_y*i);

            //ピクセル位置における発散判定までに掛かる回数を計算
            for(n=0;n<COUNT_MAX;n++)
            {
                //漸化式を実部と虚部に分けて計算
                tr = (vr*vr)-(vi*vi)+pr;
                ti = 2.0*vr*vi+pi;

                vr = tr;
                vi = ti;
                //複素数の絶対値の2乗が4を超えると発散する事が知られている(|Z|>2⇒|Z(n+1)|>|Z(n)|)
                if((vr*vr)+(vi*vi)>4) break;
            }
            //発散の速さ(カウント数)によってピクセルの色を指定
            Draw(i,j,n);
        }
    }
}
//指定ピクセルに色を塗る
function Draw(i,j,count)
{
    var col = color(count);
    var r = (col >> 24) & 255;
    var g = (col >> 16) & 255;
    var b = (col >>  8) & 255;
    var a =  col        & 255;
    var str = "rgba(" + String(r) + "," + String(g) + "," + String(b) + "," + String(a) + ")";
    context.fillStyle = str;
    context.fillRect(j, i, 1, 1);
}
//カウント数から色を設定
function color(count)
{
    if(count==COUNT_MAX)
    {
        return 0x000000ff;
    }
    else if (count >= 0)
    {
        return rgba(count % 256, (count % 128) * 2 , (count % 64) * 4, 255);
    }
    else
    {
        return 0x000000ff;
    }
}
//RGBAカラービットを作成
function rgba(r, g, b, a)
{
    return ((((( (r & 255)) << 8) | (g & 255)) << 8) | (b & 255)) << 8 | (a & 255);
}

import React, { Component, lazy, Suspense } from "react";
import ReactDOM from "react-dom";
import {
  BrowserRouter as Router,
  Route,
  Link,
  Switch,
  Redirect,
  NavLink
} from "react-router-dom";

import { Provider } from "react-redux";
import { calculateResponsiveState } from "redux-responsive";

import Store from "../responsive/store";

//AOS
import AOS from "aos";

//Common
import BackTop from "../components/common/BackTop";
import ScrollToTop from "react-scroll-up";

//HeapAnalytics(https://heapanalytics.com/)
import HeapAnalytics from "../components/common/HeapAnalytics";

import { scrollToTop } from "../components/common/Helpers";

//import firebase from 'firebase/app';
//import 'firebase/firestore';
//import config from '../config/firebase-config';

import ParticleCanvas from "../components/common/ParticleCanvas";

import HeaderBox from "../components/Header";
import Hero from "../components/Hero";

// import { VFXProvider } from 'react-vfx';

import 'aos/dist/aos.css';

const Container = React.lazy(() => import("../components/Container"));

class App extends Component {
  constructor() {
    super();
    this.state = {
      backgroundName: "index",
      pass: "index",
      loading: true
    };

    this.onChangeLink = this.onChangeLink.bind(this);
  }

  componentDidMount() {
    HeapAnalytics.Init();
    AOS.init();
    // firebase.initializeApp(config);
    // firebase.firestore().settings({timestampsInSnapshots:true});
    this.setState({ loading: false });
  }

  componentDidUpdate() {
    scrollToTop();
  }

  onChangeLink(id) {
    let index = id.indexOf("/");

    let rootPass = id.slice(0, index);

    if (id === this.state.pass) return;

    this.setState({ pass: id });
  }

  render() {
    const changePass = (
      <Switch>
        <Route
          exact
          path="/"
          component={() => {
            this.onChangeLink("index");
            return null;
          }}
        />
        <Route
          path="/:id"
          component={({ match }) => {
            this.onChangeLink(match.params.id);
            return null;
          }}
        />
      </Switch>
    );

    const fallbackStyle = {
      textAlign:'center',
      marginRight: 'auto',
      marginLeft: 'auto'
    };

    return (
      <React.Fragment>
        <ParticleCanvas />
        <Router history={history}>
          <div>
            {changePass}
            <HeaderBox />
            <Hero pass={this.state.pass} />
            {/* <div className={"pageTitleImage-" + this.state.backgroundName}></div> */}
            <Suspense
              fallback={
                <React.Fragment>
                  <div style={fallbackStyle}>
                    <div>Please wait for a while. m(_ _)m</div>
                    <div>
                      <img src="/images/pic/ajax-loader.gif" />
                    </div>
                  </div>
                </React.Fragment>
              }
            >
              <Container />
            </Suspense>
          </div>
        </Router>
        <BackTopScroll />
      </React.Fragment>
    );
  }
}

const BackTopScroll = React.memo(() => (
  <ScrollToTop showUnder={160}>
    <BackTop />
  </ScrollToTop>
));

ReactDOM.render(
  // <VFXProvider>
  <Provider store={Store}>
    <App />
  </Provider>,
  // </VFXProvider>
  document.getElementById("root")
);

// calculate the initial state
Store.dispatch(calculateResponsiveState(window));

<?php
header('Access-Control-Allow-Origin: *');
try
{
    $dbHost = 'mysql:host=mysql1.php.xdomain.ne.jp;dbname=na24ddr_simfiles;charset=utf8';
    $dbName = 'na24ddr_simfile';
    $dbPass = 'na2424ddrA';
    $options = array(PDO::ATTR_ERRMODE=>PDO::ERRMODE_EXCEPTION, PDO::ATTR_DEFAULT_FETCH_MODE=>PDO::FETCH_ASSOC);

    $pdo = new PDO($dbHost, $dbName, $dbPass, $options);
    $sql = '';
    $parameter = (!empty($_SERVER['QUERY_STRING'])) ? $_SERVER['QUERY_STRING'] : '';

    if($parameter != '')
    {
        parse_str($parameter, $array);

        $addStr = '';
        $keys = array_keys($array);

        if($keys[0] == 'simfiles')
        {
            $addStr = 'WHERE simfiles = '.$array['simfiles'];
        }
        else if($keys[0] == 'title')
        {
            $str = '';
            
            //文字チェック.
            if(preg_match('/^[a-z]-[a-z]$/u', $array['title']))
                $str = $array['title'];
            else
                $str = 'others';
            
            //検索条件付け.
            if($str != 'others')
                $addStr = "WHERE title REGEXP" ."'^[" .$str ."]'" ."ORDER BY title ASC";
            else
                $addStr = "WHERE title NOT REGEXP" ."'^[a-z]'" ."ORDER BY title ASC";

            //TODO:例外判定.
        }

        $sql = 'select * from Simifiles '.$addStr;
    }
    else
    {
        $sql = 'select * from Simifiles';
    }

    $stmt = $pdo->query($sql);
    $simfilesData = array();

    //$rows = $pdo->query('SELECT * FROM users')->fetchAll(PDO::FETCH_ASSOC);
    while($result = $stmt->fetch(PDO::FETCH_ASSOC))
    {
        $date = new DateTime($result['update_at']);

        $simfilesData[] = array(
            'id'=>$result['id'],
            'simfiles'=>$result['simfiles'],
            'title'=>$result['title'],
            'subtitle'=>$result['subtitle'],
            'artist'=>$result['artist'],
            'genre'=>$result['genre'],
            'bpm'=>$result['bpm'],
            'update_at'=>$date->format('Y-m-d'),
            'time'=>$result['time'],
            'size'=>$result['size'],
            'dl_url'=>$result['dl_url'],
            'bn_url'=>$result['bn_url'],
            'bg_url'=>$result['bg_url'],
            'dif_gsp'=>$result['dif_gsp'],
            'dif_bsp'=>$result['dif_bsp'],
            'dif_dsp'=>$result['dif_dsp'],
            'dif_esp'=>$result['dif_esp'],
            'dif_csp'=>$result['dif_csp'],
            'dif_gsp_url'=>$result['dif_gsp_url'],
            'dif_bsp_url'=>$result['dif_bsp_url'],
            'dif_dsp_url'=>$result['dif_dsp_url'],
            'dif_esp_url'=>$result['dif_esp_url'],
            'dif_csp_url'=>$result['dif_csp_url'],
            'has_dp'=>$result['has_dp'],
            'dif_bdp'=>$result['dif_bdp'],
            'dif_ddp'=>$result['dif_ddp'],
            'dif_edp'=>$result['dif_edp'],
            'dif_cdp'=>$result['dif_cdp'],
            'dif_bdp_url'=>$result['dif_bdp_url'],
            'dif_ddp_url'=>$result['dif_ddp_url'],
            'dif_edp_url'=>$result['dif_edp_url'],
            'dif_cdp_url'=>$result['dif_cdp_url'],
            'is_small_title'=>$result['is_small_title'],
            'is_small_artist'=>$result['is_small_artist'],
            'bg_width'=>$result['bg_width'],
            'bg_height'=>$result['bg_height'],
            'has_video'=>$result['has_video'],
            'video_url'=>$result['video_url']
        );
    }
    //jsonとして出力
    header('Content-type: application/javascript');
    echo json_encode($simfilesData, JSON_UNESCAPED_UNICODE);
}
catch (PDOException $e) 
{ 
    die($e->getMessage()); 
}
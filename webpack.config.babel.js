import webpack from 'webpack';
const path = require('path');
import HtmlWebpackPlugin from 'html-webpack-plugin';
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const CompressionPlugin = require('compression-webpack-plugin');
const { CheckerPlugin } = require('awesome-typescript-loader');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
const PRODUCTION = process.env.NODE_ENV === 'production';
const fs = require('fs');

const nowDateTime = new Date();
let cssPathName = '/css/' + 'style-'+
  nowDateTime.getFullYear() + '-' +
  (nowDateTime.getMonth()+1) + '-' +
  nowDateTime.getUTCDate() + '.min.css';

let fileTargets = ['/css/style.min.css', '/css/style.min.css.gz'];
console.log('cssファイルのパス : ' + __dirname + fileTargets[0]);

const cacheFilePath = __dirname + "/cssfilename.txt";

if(fs.existsSync(__dirname + fileTargets[0]))
{
  fs.renameSync(__dirname + fileTargets[0], __dirname + cssPathName );
  fs.renameSync(__dirname + fileTargets[1], __dirname + cssPathName + '.gz');
  console.log("cssのファイル名を" + cssPathName + 'に変更しました');
  try {
    fs.writeFileSync(cacheFilePath, cssPathName);
  } catch(e){
    console.log(e);
  }
}
else
{
  cssPathName = fs.readFileSync(cacheFilePath, 'utf8');
  console.log("Name:"+ cssPathName);
};

export default {
  entry:"./src/index.jsx",

  plugins: [
    new webpack.LoaderOptionsPlugin({
      options: {
        eslint: {
          configFile: '.eslintrc'
        }
      }
    }),
    // new webpack.optimize.UglifyJsPlugin(
    //   {sourceMap: true},
    //   {comments: false}
    // ),
    new CleanWebpackPlugin(),
    new HtmlWebpackPlugin({
      title: 'Eternal∞Tune(StepMania用DWI配布サイト)',
      cssName: cssPathName,
      template: './src/index.ejs'
    }),
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify('production')
      }
    }),
    new CompressionPlugin({
      filename: '[path].gz[query]',
      algorithm: 'gzip',
      test: /\.js$|\.css$|\.html$|\.eot?.+$|\.ttf?.+$|\.woff?.+$|\.svg?.+$/,
      threshold: 10240,
      minRatio: 0.78
    }),
    new BundleAnalyzerPlugin()
    // new CompressionPlugin({
    //   asset: "[path].gz[query]",
    //   algorithm: "gzip",
    //   test: /\.js$|\.html$/,
    //   threshold: 10240,
    //   minRatio: 0.8
    // })
    // new CheckerPlugin()
  ],
  output: {
    path: __dirname + '/static',
    filename: '[name]-[hash].bundle.js',
    publicPath: '/'
  },
  devServer: {
    contentBase: __dirname,
    compress: true,
    port: 8080,
    open: true,
  },

  module: {
    rules: [
      {
        test: /\.html$/,
        loader: "file?name=[name].[ext]"
      },
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
        options: {
          plugins: ['transform-decorators-legacy', "syntax-dynamic-import" ],
          presets: ['es2015', 'react']
        }
      },
      {
        test: /\.(ts|tsx)?$/,
        loader: "ts-loader"
      },
      {
        test: /\.css$/,
        loader: [ 'style-loader', 'css-loader' ]
      },
      {
        test: /\.(ttf|woff|woff2?)$/,
        loader: 'file-loader',
        options: {
          name: '[name].[ext]',
          outputPath: 'fonts/'
        }
      }
      // {
      //   enforce: "pre",
      //   test: /\.(jsx|tsx)?$/,
      //   exclude: /node_modules/,
      //   use: {
      //     loader: 'babel-loader',

      //   },
      //   query: {
      //     plugins: [
      //       'transform-flow-strip-types',
      //       'transform-decorators-legacy'
      //     ]
      //   }
      // }
    ]
  },
  resolve: {
    extensions: ['*', '.js', '.jsx', ".ts", ".tsx"]
  }
};

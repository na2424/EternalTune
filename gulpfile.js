const gulp = require('gulp');
const sass = require('gulp-sass');
const compass = require('gulp-compass');

const cssmin = require('gulp-cssmin');
const rename = require('gulp-rename');
const gzip = require("gulp-gzip");
const runSequence = require('run-sequence');

const webserver = require('gulp-webserver');

//var revReplace = require('gulp-rev-replace');

//--------------------
// タスクを定義
//--------------------
// webserver
gulp.task('webserver', function () {
  gulp.src('.')
    .pipe(webserver({
      host: 'localhost',
      port: 8080,
      livereload: true
    }));
});

//sass to css
gulp.task('compass', () => {
  return gulp.src('sass/**/*.scss')
    .pipe(compass({
    config_file: 'config.rb',
    comments: false,
    css: 'css_expanded/',
    sass: 'sass/'
  }));
});

//css to min.css
gulp.task('cssmin', () => {
  return gulp.src('css_expanded/*.css')
    .pipe(cssmin())
    .pipe(rename({suffix: '.min'}))
    .pipe(gulp.dest('css'));
});

//css to gzip(.gz)
gulp.task('gzip', () => {
  return gulp.src('css_expanded/*.css')
    .pipe(cssmin())
    .pipe(rename({suffix: '.min'}))
    .pipe(gzip())
    .pipe(gulp.dest('css'));
});

//-------------------
// ターミナルコマンド
//-------------------
/**
 * watch
 * watchでcompassを自動で書きだす
 */
gulp.task('watch', () => {
  gulp.watch('sass/**/*.scss', (event) => {
    gulp.run('compass');
  });

  gulp.watch('css_expanded/**/*.css', (event) => {
    gulp.run('cssmin');
  });

  gulp.watch('css_expanded/**/*.css', (event) => {
    gulp.run('gzip');
  });
});

//[実行方法] gulp build
gulp.task('build', function(done) {
  runSequence('compass', 'cssmin', 'gzip', ()=>
  {
    console.log('Run something else');
    done();
  });
});

gulp.task('default', () => {
  gulp.run('build');
});
